class Constants {

  // API
  static const String BASE_URL = "http://beta.devsandbox.me/";

  static const String API_HOME_PAGE = BASE_URL + "api/home";
  static const String API_HOME_PAGE_NEW = BASE_URL + "api/home_new";
  static const String API_BRAND_PAGE = BASE_URL + "api/brand/";
  static const String API_ADD_CART = BASE_URL + "api/cart/store";
  static const String API_GET_CART = BASE_URL + "api/cart/get";
  static const String API_ADD_FAVE = BASE_URL + "api/favorite/store";
  static const String API_GET_FAVE = BASE_URL + "api/favorite/get";
  static const String API_REMOVE_FAVE = BASE_URL + "api/favorite/destroy/";
  static const String API_GET_CHECKOUT = BASE_URL + "api/checkout/get";
  static const String API_COMMIT_CHECKOUT = BASE_URL + "api/checkout/commit";
  static const String API_UPDATE_ADDRESS_CHECKOUT = BASE_URL + "api/checkout/update/address";
  static const String API_VOUCHER_CHECKOUT = BASE_URL + "api/checkout/discount/update";
  static const String API_SHIPMENT_GET = BASE_URL + "api/checkout/shipping/get";
  static const String API_SHIPMENT_UPDATE = BASE_URL + "api/checkout/shipping/update";

  static const String API_UPLOAD_TRANSFER = BASE_URL + "api/checkout/upload_transver";

  static const String API_NOTIFICATION_BADGE = BASE_URL + "api/badge";
  static const String API_GOOGLE_LOGIN = BASE_URL + "api/social/gg/login";
  static const String API_FACEBOOK_LOGIN = BASE_URL + "api/social/fb/login";

  static const String API_GET_ADDRESS = BASE_URL + "api/address/get";
  static const String API_GET_PROVINCE = BASE_URL + "api/address/province";
  static const String API_GET_CITY = BASE_URL + "api/address/city";
  static const String API_GET_SUBDISTRICT = BASE_URL + "api/address/subdistrict";
  static const String API_ADD_ADDRESS = BASE_URL + "api/address/store";
  static const String API_REMOVE_ADDRESS = BASE_URL + "api/address/destroy";
  static const String API_EDIT_ADDRESS = BASE_URL + "api/address/update";

  static const String API_GET_ORDERS = BASE_URL + "api/orders";
  static const String API_SEARCH = BASE_URL + "api/search";
  static const String API_LANG = BASE_URL + "api/lang/";

  static const String ROUTE_HOME_PAGE = "/";
  static const String ROUTE_LOGIN_PAGE = "/login";
  static const String ROUTE_PRODUCT_LIST = "/productList";
  static const String ROUTE_PRODUCT_DETAIL = "/productDetail";
  static const String ROUTE_CART_LIST = "/cartList";
  static const String ROUTE_PAYMENT_PAGE = "/payment";

  static const String ROUTE_ADDRESS_LIST = "/addressList";
  static const String ROUTE_ADD_ADDRESS = "/addAddress";
  static const String ROUTE_ADD_PROVINCE = "/addProvince";
  static const String ROUTE_ADD_CITY = "/addCity";
  static const String ROUTE_ADD_SUBDISTRICT = "/addSubdistrict";
  static const String ROUTE_ORDERS = "/orders";

  // Google API Key for g+ profile pic
  static const String GOOGLE_API_KEY = "AIzaSyDrh5Fhykz50tlI0HiwvzVUQ7FTTQ7wdcM";
  static const String SAVED_USER_MEMBERSHIP = "userMembership";

  // cart timeout
  static const String TIMESTAMP_ADD_CART = "timestampAddCart";
  static const String BOOL_TIMER_ACTIVE = "boolTimerActive";

  // Google Shared Prefs
  static const String IS_GOOGLE_LOGIN = "isGoogleLoggedIn";
  static const String SAVED_GOOGLE_ID = "googleId";
  static const String SAVED_GOOGLE_NAME = "googleName";
  static const String SAVED_GOOGLE_EMAIL = "googleEmail";
  static const String SAVED_GOOGLE_TOKEN = "googleToken";
  static const String GOOGLE_PIC_URL = "googlePic";

  // Google Shared Prefs
  static const String IS_FACEBOOK_LOGIN = "isFacebookLoggedIn";
  static const String SAVED_FACEBOOK_ID = "facebookId";
  static const String SAVED_FACEBOOK_NAME = "facebookName";
  static const String SAVED_FACEBOOK_EMAIL = "facebookEmail";
  static const String SAVED_FACEBOOK_TOKEN = "facebookToken";
  static const String FACEBOOK_PIC_URL = "facebookPic";

  // label menu
  static const String PRODUCT_DETAIL_EDIT = "UBAH";
  static const String PRODUCT_DETAIL_SIZE = "Size: ";
  static const String PRODUCT_DETAIL_COLOR = "Color: ";
  static const String PRODUCT_DETAIL_ITEM_QTY = "Jumlah barang: ";
  static const String PRODUCT_DETAIL_CUT_QTY = "Jumlah yg dipotong: ";
  static const String PRODUCT_DETAIL_CUT_NOTE = "Catatan potong: ";
  static const String PRODUCT_DETAIL_SOLD_OUT = "SOLD OUT";
  static const String PRODUCT_DETAIL_HINT = "Contoh:\nPotong 10cm\ndari bawah\n\nAtau potong \nmengikuti tinggi\nbadan saya 165cm";

}

class Globals {

  static int brandID;
  static bool alreadyLogin;

  static String provinceID = "";
  static String cityID = "";
  static String subdistrictID = "";

  static String provinceName = "";
  static String cityName = "";
  static String subdistrictName = "";
  static String postalCode = "";
  static String village = "";

  static int langSelected = 1;
  static int imageIndex;

  static var timeAddCart;
  static var timeOut;
  static var offset = 60;
  static bool timerIsInactive;
  static bool timerPopupIsAlreadyShowing;
}