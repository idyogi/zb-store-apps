import 'package:flutter/material.dart';
import 'package:zb_store/pages/product_detail_page.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/pages/product_list_page.dart';
import 'package:zb_store/pages/home_page.dart';
import 'package:zb_store/pages/home_login_page.dart';
import 'package:zb_store/pages/address_add_page.dart';
import 'package:zb_store/pages/cart_list_page.dart';
import 'package:zb_store/pages/payment_page.dart';
import 'package:zb_store/pages/address_list_page.dart';
import 'package:zb_store/pages/address_province_page.dart';
import 'package:zb_store/pages/address_city_page.dart';
import 'package:zb_store/pages/address_subdistrict_page.dart';
import 'package:zb_store/pages/order_page.dart';

class Routes {
  static final routes = <String, WidgetBuilder>{
    // core
    Constants.ROUTE_HOME_PAGE: (BuildContext context) => HomePage(),
    Constants.ROUTE_LOGIN_PAGE: (BuildContext context) => LoginPage(),
    Constants.ROUTE_PRODUCT_LIST: (BuildContext context) => ProductsListPage(),
    Constants.ROUTE_PRODUCT_DETAIL: (BuildContext context) => ProductDetailPage(),
    Constants.ROUTE_CART_LIST: (BuildContext context) => CartListPage(),
    Constants.ROUTE_PAYMENT_PAGE: (BuildContext context) => PaymentPage(),
    Constants.ROUTE_ORDERS: (BuildContext context) => OrderPage(),
    // address
    Constants.ROUTE_ADDRESS_LIST: (BuildContext context) => AddressListPage(),
    Constants.ROUTE_ADD_ADDRESS: (BuildContext context) => AddressAddPage(),
    Constants.ROUTE_ADD_PROVINCE: (BuildContext context) => AddressProvincePage(),
    Constants.ROUTE_ADD_CITY: (BuildContext context) => AddressCityPage(),
    Constants.ROUTE_ADD_SUBDISTRICT: (BuildContext context) => AddressSubdistrictPage(),
  };
}
