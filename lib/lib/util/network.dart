import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zb_store/util/globals.dart';

class Network {
  String token = "";

  Future getSavedToken() async {
    final prefs = await SharedPreferences.getInstance();
    final alreadyLogin = prefs.getBool(Constants.IS_GOOGLE_LOGIN) ?? false;
    if (alreadyLogin) {
      token = prefs.getString(Constants.SAVED_GOOGLE_TOKEN);
    } else {
      token = prefs.getString(Constants.SAVED_FACEBOOK_TOKEN);
    }
  }

  Future<int> getNotificationBadge() async {
    int itemCount = 0;
    // get token from shared prefs
    await getSavedToken();
    // get cart
    var response = await http.get(
      Constants.API_NOTIFICATION_BADGE,
      headers: {
        "X-Authorization":token,
      },
    ).catchError(
          (error) {
        return false;
      },
    );

    if (json.decode(response.body)['status'] == "success") {
      itemCount = json.decode(response.body)['item_count'];
    }

    return itemCount;
  }

}