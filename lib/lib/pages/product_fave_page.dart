import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:zb_store/scoped_model/product_scoped_model.dart';
import 'package:zb_store/util/languages.dart';
import 'package:zb_store/widgets/products_list_item.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/widgets/home_drawer.dart';

class ProductFavePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
     ProductsScopedModel productScopedModel = ProductsScopedModel();
     productScopedModel.parseFavoriteFromResponse(Constants.API_GET_FAVE, 1);
    // TODO: implement build
    return new ScopedModel<ProductsScopedModel>(
        model: productScopedModel,
        child: Scaffold(
          //key: _scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            elevation: 1,
            backgroundColor: Colors.white,
            title: new Text(
                AppText.HOME_FAVE[Globals.langSelected], style: TextStyle(color: Colors.black87)),
            iconTheme: new IconThemeData(color: Colors.black54),
          ),
          body: ProductsListPageBody(),
          //drawer: HomeDrawer(),
          //bottomNavigationBar: _buildBottomNavigationBar(context),
        )
    );
  }
}

class ProductsListPageBody extends StatelessWidget {
  //final String url;
  //const ProductsListPageBody({Key key, @required this.url}) : super(key: key);
  int pageIndex = 1;

  @override
  Widget build(BuildContext context) {


    return ScopedModelDescendant<ProductsScopedModel>(
      builder: (context, child, model) {
        //print("---------------------------------------------------------------->TOP");
        return model.isLoading
            ? _buildSplashScreen()
            : _buildListView(context, model);
      },
    );
  }

  _buildSplashScreen() {

    return Stack(
      children: <Widget>[
        Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
        ),

        Center(
          child: GlowingProgressIndicator(
              duration: Duration(milliseconds: 400),
              child: Container(
                height: 70.0,
                width: 70.0,
                child: Image.asset('assets/images/logo_zb_10.png', color: Colors.black54)
              )
          ),
        ),

/*
          Container(
            height: double.infinity,
            width: double.infinity,
          ),
*/
      ],
    );
  }

  _buildCircularProgressIndicator() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  _buildListView(BuildContext context, ProductsScopedModel model) {
    Size screenSize = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
      child: model.getProductsCount() == 0
          ? Center(child: Text("No products available."))
          : ListView.builder(
        itemCount: model.getProductsCount() + 2,
        itemBuilder: (context, index) {
          if (index == model.getProductsCount() && !model.produkHabis) {
            if (model.hasMoreProducts) {
              pageIndex++;
              model.parseFavoriteFromResponse(Constants.API_GET_FAVE, pageIndex);
              return Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Container(),
                //child: Center(child: CircularProgressIndicator()),
              );

            }
            return Container();
          } /*else if (index == 0) {
            //0th index would contain filter icons
            return _buildFilterWidgets(screenSize);
          }*/ else if (index % 2 == 0) {
            //2nd, 4th, 6th.. index would contain nothing since this would
            //be handled by the odd indexes where the row contains 2 items
            return Container();
          } else {
            // jika jumlah data ganjil tampilkan single produk
            if (index % 2 == 1 && model.getProductsCount() == index) {
              return ProductsListItem(
                product1: model.productsList[index - 1],
                product2: null,
              );
            }

            //1st, 3rd, 5th.. index would contain a row containing 2 products
            if (index > model.getProductsCount() - 1) {
              return Container();
            }
            return ProductsListItem(
              product1: model.productsList[index - 1],
              product2: model.productsList[index],
            );
          }
        },
      ),
    );
  }

  _buildFilterWidgets(Size screenSize) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      width: screenSize.width,
      child: Card(
        elevation: 4.0,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              _buildFilterButton("SORT"),
              Container(
                color: Colors.black,
                width: 2.0,
                height: 24.0,
              ),
              _buildFilterButton("REFINE"),
            ],
          ),
        ),
      ),
    );
  }

  _buildFilterButton(String title) {
    return InkWell(
      onTap: () {
        print(title);
      },
      child: Row(
        children: <Widget>[
          Icon(
            Icons.arrow_drop_down,
            color: Colors.black,
          ),
          SizedBox(
            width: 2.0,
          ),
          Text(title),
        ],
      ),
    );
  }
}
