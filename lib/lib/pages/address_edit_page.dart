import 'package:flutter/material.dart';
import 'package:zb_store/model/address.dart';
import 'package:zb_store/scoped_model/address_scoped_model.dart';
import 'address_province_page.dart';
import 'package:zb_store/util/globals.dart';

class AddressEditPage extends StatefulWidget {
  final Address address;

  AddressEditPage({
    @required this.address,
  });

  @override
  AddressEditPageState createState() => new AddressEditPageState(address: address);
}

class AddressEditPageState extends State<AddressEditPage> {
  final Address address;

  AddressEditPageState({
    @required this.address,
  });


  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final AddressScopedModel addressModel = AddressScopedModel();

  final _nama = TextEditingController();
  final _alamat = TextEditingController();
  final _telpon = TextEditingController();
  final _desa = TextEditingController();
  final _kodepos = TextEditingController();
  bool _validateNama = false;
  bool _validateAlamat = false;
  bool _validateTelpon = false;
  bool _isDefault;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    (address.isDefault == 1) ? _isDefault = true : _isDefault = false;
    _nama.text = address.name;
    _alamat.text = address.address;
    _telpon.text = address.phone;
    _desa.text = address.village;
    _kodepos.text = address.postalCode;

  }

  void _onSwitchChanged(bool value) {
    setState(() {
      _isDefault = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    //(address.isDefault == 1) ? _isDefault = true : _isDefault = false;

    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 1,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: new Text(
            "EDIT ALAMAT", style: TextStyle(color: Colors.black87)),
        iconTheme: new IconThemeData(color: Colors.black54),
      ),
      body: _buildAddressAddPageBody(address),
      bottomNavigationBar: _buildBottomNavigationBar(context),
    );
  }

  _buildBottomNavigationBar(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Flexible(
            flex: 1,
            child: RaisedButton(
              onPressed: () {
                String alamatUtama = "";
                _isDefault == true ? alamatUtama = "1" : alamatUtama = "0";
                // debug
/*
                print("address.id.toString()="+address.id.toString());
                print("_nama.text="+_nama.text.toString());
                print("Globals.provinceID="+Globals.provinceID.toString());
                print("Globals.cityID="+Globals.cityID.toString());
                print("Globals.subdistrictID="+Globals.subdistrictID.toString());
                print("_alamat.text="+_alamat.text.toString());
                print("Globals.postalCode="+Globals.postalCode.toString());
                print("_telpon.text="+_telpon.text.toString());
                print("alamatUtama="+alamatUtama.toString());
*/

                // send api
/*
                _scaffoldKey.currentState.showSnackBar(
                    new SnackBar(
                      duration: new Duration(seconds: 2),
                      content: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          FutureBuilder<dynamic>(
                              future: addressModel.updateAddress(
                                  address.id.toString(),
                                  _nama.text,
                                  Globals.provinceID == "" ? address.provinceID.toString() : Globals.provinceID,
                                  Globals.cityID == "" ? address.cityID.toString() : Globals.cityID,
                                  Globals.subdistrictID == "" ? address.subdistrictID.toString() : Globals.subdistrictID,
                                  _alamat.text,
                                  Globals.postalCode == "" ? address.postalCode.toString() : Globals.postalCode,
                                  _telpon.text,
                                  alamatUtama
                              ),
                              builder: (context, snapshot) {
                                return (snapshot.hasData)
                                    ? Text(snapshot.data['message'])
                                    : CircularProgressIndicator();
                              }
                          ),
                        ],
                      ),
                    )
                );
*/

                _scaffoldKey.currentState.showSnackBar(
                    new SnackBar(
                      duration: new Duration(minutes: 2),
                      content: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new CircularProgressIndicator(),
                          new Text("  Updating...")
                        ],
                      ),
                    )
                );

                addressModel.updateAddress(
                    address.id.toString(),
                    _nama.text,
                    Globals.provinceID == "" ? address.provinceID.toString() : Globals.provinceID,
                    Globals.cityID == "" ? address.cityID.toString() : Globals.cityID,
                    Globals.subdistrictID == "" ? address.subdistrictID.toString() : Globals.subdistrictID,
                    _alamat.text,
                    //Globals.postalCode == "" ? address.postalCode.toString() : Globals.postalCode,
                    _kodepos.text,
                    _telpon.text,
                    _desa.text,
                    alamatUtama
                ).then((response) {

                  _scaffoldKey.currentState.hideCurrentSnackBar();
                  Navigator.of(context).pop();

                });

              },
              color: Colors.white,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.check_circle,
                      color: Colors.black,
                    ),
                    SizedBox(
                      width: 4.0,
                    ),
                    Text(
                      "SIMPAN",
                      style: TextStyle(color: Colors.black),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _buildAddressAddPageBody(Address address) {

    // reload data
/*
    Globals.provinceName = address.provinceName;
    Globals.cityName = address.cityName;
    Globals.subdistrictName = address.subdistrictName;
*/
    //(address.isDefault == 1) ? _onSwitchChanged(true) : _onSwitchChanged(false);

    return Theme(
      // Create a unique theme with "ThemeData"
        data: ThemeData(
          primaryColor: Colors.blue[400],
          accentColor: Colors.grey,
          hintColor: Colors.grey,
          cursorColor: Colors.blue[400],
        ),
        child: Padding(
          padding: EdgeInsets.only(top: 3),
          child: Card(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 2, horizontal: 5),
              child: ListView(
                children: <Widget>[

/*
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
*/
                  // Nama
                  TextField(
                    controller: _nama,
                    decoration: InputDecoration(
                      labelText: 'Nama Lengkap',
                      errorText: _validateNama ? 'Nama tidak boleh kosong' : null,
                    ),
                  ),
                  SizedBox(height: 5.0),

                  // Alamat
                  TextField(
                    controller: _alamat,
                    decoration: InputDecoration(
/*
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.deepPurple[400],
                      ),
                    ),
*/
                      labelText: 'Nama Jalan/Gedung dan Nomornya',
                      errorText: _validateAlamat ? 'Alamat tidak boleh kosong' : null,
                    ),
                  ),

                  // Provinsi
                  SizedBox(height: 25.0),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) {
                              return AddressProvincePage();
                            },
                          )
                      );
                    },
                    child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            (Globals.provinceName != "")
                                ? Text(Globals.provinceName, style: TextStyle(color: Colors.black, fontSize: 16))
                                : Text(address.provinceName, style: TextStyle(color: Colors.black, fontSize: 16)),
                            Icon(Icons.arrow_forward_ios,
                                color: Colors.grey[400]),
                          ],
                        )
                    ),
                  ),
                  SizedBox(height: 18.0),
                  new Container(
                    height: 1.8,
                    color: Colors.grey[400],
                  ),

                  // Kota/Kabupaten
                  SizedBox(height: 25.0),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) {
                              return AddressProvincePage();
                            },
                          )
                      );
                    },
                    child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            (Globals.cityName != "")
                                ? Text(Globals.cityName, style: TextStyle(color: Colors.black, fontSize: 16))
                                : Text(address.cityName, style: TextStyle(color: Colors.black, fontSize: 16)),
                            Icon(Icons.arrow_forward_ios,
                                color: Colors.grey[400]),
                          ],
                        )
                    ),
                  ),
                  SizedBox(height: 18.0),
                  new Container(
                    height: 1.8,
                    color: Colors.grey[400],
                  ),

                  // Kecamatan
                  SizedBox(height: 25.0),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) {
                              return AddressProvincePage();
                            },
                          )
                      );
                    },
                    child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            (Globals.subdistrictName != "")
                                ? Text(Globals.subdistrictName, style: TextStyle(color: Colors.black, fontSize: 16))
                                : Text(address.subdistrictName, style: TextStyle(color: Colors.black, fontSize: 16)),
                            Icon(Icons.arrow_forward_ios,
                                color: Colors.grey[400]),
                          ],
                        )
                    ),
                  ),
                  SizedBox(height: 18.0),
                  new Container(
                    height: 1.8,
                    color: Colors.grey[400],
                  ),

                  // Desa
                  TextField(
                    controller: _desa,
                    decoration: InputDecoration(
                      labelText: 'Desa',
                    ),
                  ),

                  // Kode Pos
                  TextField(
                    controller: _kodepos,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: 'Kode Pos',
                    ),
                  ),

                  // Telp
                  TextField(
                    controller: _telpon,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: 'Nomor Telepon',
                      errorText: _validateTelpon ? 'Telepon tidak boleh kosong' : null,
                    ),
                  ),
                  SizedBox(height: 20.0),

                  // default
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Switch(
                        value: _isDefault,
                        onChanged: _onSwitchChanged,
                        activeTrackColor: Colors.redAccent[100],
                        activeColor: Colors.red,
                      ),
                      Text("Jadikan alamat utama"),
                    ],
                  ),


                ],
              ),
            ),
          ),
        )
    );
  }
}

