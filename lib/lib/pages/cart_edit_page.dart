import 'package:scoped_model/scoped_model.dart';
import 'package:zb_store/scoped_model/cart_scoped_model.dart';
import 'package:flutter/material.dart';
import 'package:zb_store/model/cart_item.dart';
import 'package:zb_store/util/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:zb_store/util/languages.dart';

class CartEditPage extends StatefulWidget {
  final CartItem cartItem;

  CartEditPage({this.cartItem});

  @override
  _CartEditPageState createState() => _CartEditPageState(cartItem);
}

class _CartEditPageState extends State<CartEditPage> {
  final CartItem cartItem;
  //final _cutNote = TextEditingController();
  _CartEditPageState(this.cartItem);
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<int> _potongJadi  = new List();
  String _cutNotes;
  bool edited = false;

  @override
  Widget build(BuildContext context) {
    CartScopedModel cartScopedModel = CartScopedModel();
    return ScopedModel<CartScopedModel>(
      model: cartScopedModel,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: 1,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(
              Icons.chevron_left,
              size: 40.0,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          backgroundColor: Colors.white,
          title: Text(
            AppText.CART_UPDATE[Globals.langSelected],
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        body: _buildCartEditPage(context),
        bottomNavigationBar: _buildBottomNavigationBar(context),
      ),
    );
  }

  _buildCartEditPage(BuildContext context) {
    int maxItemQty = cartItem.itemQty;
    var stringSplit = cartItem.productModel.trim().split(",");

    return ScopedModelDescendant<CartScopedModel>(
      builder: (context,child,model) {
        return ListView(
          children: <Widget>[
            Container(
              //height: MediaQuery.of(context).size.height,
              padding: const EdgeInsets.all(7),
              child: Card(
                elevation: 0.2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                    // title
                    Center(
                      child: Container(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: Stack(
                          children: <Widget>[

                            Image.network(cartItem.productImage, fit: BoxFit.cover),

                            // pre order
                            (cartItem.preOrder == 1)
                              ? Padding(
                                  padding: EdgeInsets.only(left: 5.0, top: 5.0),
                                  child: Container(
                                    alignment: Alignment(-1.0, -1.0),
                                    width: 50.0,
                                    height: 20.0,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10.0),
                                        color: Colors.grey
                                    ),
                                    child: Center(child: Text("PO", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold))),
                                  ),
                                )
                            : Container(),

                          ]
                        ),

                        width: 270,
                        height: 405,
                      ),
                    ),

                    SizedBox(
                      height: 4.0,
                    ),

                    // name, model and price
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12.0),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "${cartItem.productName} (${stringSplit[1]})",
                              style: TextStyle(fontSize: 18.0, color: Colors.black, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),
                            ),
                            Text(
                              "${AppText.PRODUCT_COLOR[Globals.langSelected]}: ${stringSplit[0]}",
                              style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal),
                            ),
                            Text(
                              cartItem.priceBeforeCutText,
                              style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold),
                            ),
                            (cartItem.isCut == 1)
                            ? Text(
                              "(+${AppText.PRODUCT_CUT_FEE[Globals.langSelected]} ${cartItem.priceCutText})",
                              style: TextStyle(fontSize: 13.0, color: Colors.grey),
                            )
                            : Container(),
                          ],
                        ),
                      ),
                    ),
                    Divider(),
                    SizedBox(height: 12.0,),

                    // qty
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 23.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.add_shopping_cart,
                                color: Colors.grey[600],
                              ),
                              SizedBox(
                                width: 12.0,
                              ),
                              Text(
                                AppText.PRODUCT_ITEM_QUANTITY[Globals.langSelected] + ": " + cartItem.itemQty.toString(),
                                style: TextStyle(
                                  color: Colors.grey[600],
                                ),
                              ),
                            ],
                          ),
                          InkWell(
                            onTap: () {
                              // function
/*
                              Widget cupertinoActionItem(int qty) {
                                return CupertinoActionSheetAction(
                                  child: Text(qty.toString()),
                                  onPressed: () {
                                    model.updateItemQuantity(cartItem, qty);
                                    Navigator.pop(context);
                                  },
                                );
                              }

                              List<Widget> listCupertinoQty = new List();
                              for (int i = 1; i < maxItemQty+1; i++) {
                                listCupertinoQty.add(cupertinoActionItem(i));
                              }
                              showCupertinoModalPopup(
                                context: context,
                                builder: (BuildContext context) =>
                                    CupertinoActionSheet(
                                      title: const Text('Pilih Jumlah'),
                                      message: const Text('Jumlah yang tersedia '),
                                      actions: listCupertinoQty,
                                    ),
                              );
*/
                              showModalBottomSheet(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return Container(
                                        height: 200.0,
                                        color: Colors.grey[100],
                                        child: Column(
                                          children: <Widget>[
                                            SizedBox(height: 15),
                                            Text(
                                                AppText.PRODUCT_SELECT_ITEM_QTY[Globals.langSelected],
                                                style: TextStyle(fontSize: 18)
                                            ),
                                            Expanded(
                                              child: CupertinoPicker(
                                                backgroundColor: Colors.grey[100],
                                                scrollController:
                                                new FixedExtentScrollController(
                                                  initialItem: cartItem.itemQty - 1,
                                                ),
                                                itemExtent: 38.0,
                                                //magnification: 1.0,
                                                onSelectedItemChanged: (int index) {
                                                  edited = true;
                                                  int _selectedQty = index+1;
                                                  model.updateItemQuantity(cartItem, _selectedQty);
                                                },
                                                children: new List<Widget>.generate(maxItemQty,
                                                        (int index) {
                                                      return new Center(
                                                        child: new Text('${index+1}', style: TextStyle(fontSize: 22)),
                                                      );
                                                    }
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                    );
                                  }
                              );

                            },
                            child: Text(
                              AppText.CHANGE[Globals.langSelected],
                              style: TextStyle(
                                color: Colors.blue[400],
                                fontSize: 14.0,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 12.0,),

                    // cut
                    (cartItem.cutOption == 1)
                    ?Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 23.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.content_cut,
                                color: Colors.grey[600],
                              ),
                              SizedBox(
                                width: 12.0,
                              ),
                              (cartItem.isCut == 1)
                              ? Text(
                                AppText.CART_CUT_YES[Globals.langSelected],
                                style: TextStyle(
                                  color: Colors.grey[600],
                                ),
                              )
                              : Text(
                                AppText.CART_CUT_NO[Globals.langSelected],
                                style: TextStyle(
                                  color: Colors.grey[600],
                                ),
                              ),
                            ],
                          ),
                          InkWell(
                            onTap: () {
                              edited = true;
                              showCupertinoModalPopup(
                                context: context,
                                builder: (BuildContext context) =>
                                    CupertinoActionSheet(
                                      //title: const Text('Pilih Jumlah'),
                                      message: Text(AppText.CART_CUT_OPTION[Globals.langSelected]),
                                      actions: <Widget>[

                                        CupertinoActionSheetAction(
                                            child: Text(AppText.CART_CUT_NO[Globals.langSelected]),
                                            onPressed: () {
                                              model.updateIsCut(cartItem, 0);
                                              Navigator.pop(context);
                                            }
                                        ),
                                        CupertinoActionSheetAction(
                                            child: Text(AppText.CART_CUT_YES[Globals.langSelected]),
                                            onPressed: () {
                                              model.updateIsCut(cartItem, 1);
                                              Navigator.pop(context);
                                            }
                                        ),
                                      ],
                                    ),
                              );

                            },
                            child: Text(
                              AppText.CHANGE[Globals.langSelected],
                              style: TextStyle(
                                color: Colors.blue[400],
                                fontSize: 14.0,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                    : Container(),
                    SizedBox(height: 12.0,),

                    // cut note
                    (cartItem.isCut == 1)
                    ? Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 23.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.note_add,
                                color: Colors.grey[600],
                              ),
                              SizedBox(
                                width: 12.0,
                              ),
                              Text(
                                AppText.PRODUCT_CUT_NOTE[Globals.langSelected],
                                style: TextStyle(
                                  color: Colors.grey[600],
                                ),
                              ),
                            ],
                          ),
/*
                          InkWell(
                            onTap: () {
                              if (cartItem.cutNotes != "")
                                _cutNote.text = cartItem.cutNotes;
                              return showDialog(
                                context: context,
                                builder: (context) =>
                                new AlertDialog(
                                  title: new Text(Constants.PRODUCT_DETAIL_CUT_NOTE),
                                  content: TextField(
                                    maxLines: 8,
                                    controller: _cutNote,
                                    decoration: InputDecoration.collapsed(filled: true,
                                        hintText: Constants.PRODUCT_DETAIL_HINT),
                                  ),
                                  actions: <Widget>[
                                    new FlatButton(
                                      onPressed: () {
                                        model.updateCutNote(cartItem, _cutNote.text);
                                        Navigator.of(context).pop(false);
                                      },
                                      child: new Text('Simpan'),
                                    ),
                                    new FlatButton(
                                      onPressed: () {
                                        // close dialog
                                        _cutNote.text = "";
                                        model.updateCutNote(cartItem, _cutNote.text);
                                        Navigator.of(context).pop(true);
                                      },
                                      child: new Text('Batal'),
                                    ),
                                  ],
                                ),
                              );
                            },
                            child: Text(
                              Constants.PRODUCT_DETAIL_EDIT,
                              style: TextStyle(
                                color: Colors.blue[400],
                                fontSize: 12.0,
                              ),
                            ),
                          ),
*/
                        ],
                      ),
                    )
                    : Container(),
                    SizedBox(height: 8.0),

                    // cut note
                    (cartItem.isCut == 1 && cartItem.cutNotes != null)
                      ? _buildCutItemWidget(cartItem.itemQty)
/*
                        Padding(
                          padding: const EdgeInsets.only(left: 48.0,),
                          child: Text(cartItem.cutNotes, style: TextStyle(fontSize: 13.0, color: Colors.grey[500]))
                        )
*/
                      : Container(),
                    //SizedBox(height: 20.0),

                    // button
/*
                    Center(
                      child: RaisedButton(
                        color: Colors.deepPurple[400],
                        onPressed: () {
                          // update to API
                          _scaffoldKey.currentState.showSnackBar(
                            new SnackBar(
                              duration: new Duration(seconds: 2),
                              content: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  FutureBuilder<dynamic>(
                                    future: model.updateCart(
                                      cartItem.updateUrl,
                                      cartItem.itemQty,
                                      cartItem.isCut,
                                      cartItem.cutNotes
                                    ),
                                    builder: (context, snapshot) {
                                    return (snapshot.hasData)
                                      ? Text(snapshot.data['message'])
                                      : CircularProgressIndicator();
                                    }
                                  ),
                                ],
                              ),
                            )
                          );

                        },
                        child: Text(
                          "UPDATE PESANAN",
                          style: TextStyle(
                            //fontWeight: FontWeight.bold,
                              color: Colors.white,
                              fontSize: 15.0)
                      )
                      ),
                    ),
*/
                    //SizedBox(height: 20.0),

                  ],
                ),
              ),
            ),
          ],
        );
      }
    );
  }

  _buildCutItemWidget(int cutQty) {
    return ScopedModelDescendant<CartScopedModel>(
        builder: (context,child,model) {
          //int cutQty = model.getTotalCartItem();
          List<Widget> item = List();
          var cutNoteSplit;
          //String colorSize = product.selectedColor + "," + product.selectedSize;
          int height = cartItem.height;
          int cutRange = cartItem.cutRange;
          _potongJadi.add(0);

          //if (cartItem.cutNotes != null || cartItem.cutNotes != "")
          cutNoteSplit = cartItem.cutNotes.split("|");
          print("cutNotes: " + cartItem.cutNotes);

          for (int i=0; i < cutQty; i++) {
            _potongJadi.add(0);
            String selected = "";
            if (cartItem.cutNotes == null || cartItem.cutNotes == "") {
              selected = height.toString();
              print("if null");
            } else {
              selected = cutNoteSplit[i];
              print("if ada note");
            }
            print("selected: $selected");
            item.add(
              _buildCutItemDialog(
                  i+1,
                  height,
                  cutRange,
                  int.parse(selected)
              )
            );
          }

          return Padding(
            padding: const EdgeInsets.only(left: 55.0, top: 5.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: item
            ),
          );
        }
    );
  }

  _buildCutItemDialog(int i, int height, int cutRange, int cutSelected) {

    //print("Build cut item no: $i");
    return ScopedModelDescendant<CartScopedModel>(
        builder: (context,child,model)
        {
          if (_potongJadi[i] == 0) _potongJadi[i] = cutSelected;
          //print("potong jadi $i:" + _potongJadi[i].toString());
          return Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[


              //SizedBox(width: 15.0),
              InkWell(
                  onTap: () {
                    _cutNotes = "";
                    showModalBottomSheet(
                        context: context,
                        builder: (BuildContext context) {
                          return Container(
                              height: 200.0,
                              color: Colors.grey[100],
                              child: Column(
                                children: <Widget>[
                                  SizedBox(height: 15),
                                  Text(AppText.PRODUCT_CUT_ITEM[Globals.langSelected]+"$i"+AppText.PRODUCT_CUT_TO[Globals.langSelected], style: TextStyle(fontSize: 18)),
                                  Expanded(
                                    child: CupertinoPicker(
                                      backgroundColor: Colors.grey[100],
                                      scrollController:
                                      new FixedExtentScrollController(
                                        initialItem: (height - _potongJadi[i]),
                                      ),
                                      itemExtent: 38.0,
                                      //magnification: 1.0,
                                      onSelectedItemChanged: (int index) {
                                        edited = true;
                                        setState(() {
                                          _potongJadi[i] = height - index;
                                          //print("_potongJadi:$_potongJadi");
                                        });

                                      },
                                      children: new List<Widget>.generate(height - cutRange,
                                              (int index) {
                                            return new Center(
                                              child: new Text('${height-index}', style: TextStyle(fontSize: 22)),
                                            );
                                          }
                                      ),
                                    ),
                                  ),
                                ],
                              )
                          );
                        }
                    );

                    //_cutNote = _potongJadi[i].toString() + "|";
                  },
                  child:

                  Text(
                    "$i. " + AppText.PRODUCT_CUT_ITEM[Globals.langSelected]+"$i"+AppText.PRODUCT_CUT_TO[Globals.langSelected]+ "${_potongJadi[i]} cm",
                    //"$i. Barang ke-$i dipotong jadi: ${_potongJadi[i]} cm",
                    style: TextStyle(
                      color: Colors.blue[400], fontSize: 12.0,
                    ),
                  )
              ),

              SizedBox(height: 12),

            ],
          );
        }
    );
  }


  _buildBottomNavigationBar(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 50.0,
        child: ScopedModelDescendant<CartScopedModel>(
            builder: (context, child, model) {
              return Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Flexible(
                    flex: 1,
                    child: RaisedButton(
                      onPressed: () {

                        //if (edited) {
                          _cutNotes = "";
                          // append cut notes
                          String item = "";
                          print("_potongJadi.length="+_potongJadi.length.toString());
                          for (int i=1; i < cartItem.itemQty+1; i++) {
                            if (_potongJadi.length != 0) {
                              (i == cartItem.itemQty)
                                  ? item = _potongJadi[i].toString()
                                  : item = _potongJadi[i].toString() + "|";
                              _cutNotes = _cutNotes + item;
                            }
                          }
                          model.updateCutNote(cartItem, _cutNotes);
                          //model.updateItemQuantity(cartItem, qty);

                          // debug before send
                          print("updateUrl:"+cartItem.updateUrl.toString());
                          print("itemQty:"+cartItem.itemQty.toString());
                          print("isCut:"+cartItem.isCut.toString());
                          print("cutNotes:"+cartItem.cutNotes);

                          // update to API
/*
                        _scaffoldKey.currentState.showSnackBar(
                            new SnackBar(
                              duration: new Duration(seconds: 2),
                              content: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  FutureBuilder<dynamic>(
                                      future: model.updateCart(
                                          cartItem.updateUrl,
                                          cartItem.itemQty,
                                          cartItem.isCut,
                                          cartItem.cutNotes
                                      ),
                                      builder: (context, snapshot) {
                                        return (snapshot.hasData)
                                            ? Text(snapshot.data['message'])
                                            : CircularProgressIndicator();
                                      }
                                  ),
                                ],
                              ),
                            )
                        );
*/
                          _scaffoldKey.currentState.showSnackBar(
                              new SnackBar(
                                duration: new Duration(minutes: 2),
                                content: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new CircularProgressIndicator(),
                                    new Text("  Updating...")
                                  ],
                                ),
                              )
                          );

                          model.updateCart(
                              cartItem.updateUrl,
                              cartItem.itemQty,
                              cartItem.isCut,
                              cartItem.cutNotes
                          ).then((response) {
                            edited = false;
                            _scaffoldKey.currentState.hideCurrentSnackBar();
                            Navigator.of(context).pop();
                          });

/*
                        } else {
                          _scaffoldKey.currentState.showSnackBar(
                              new SnackBar(
                                duration: new Duration(seconds: 2),
                                content: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    //new CircularProgressIndicator(),
                                    new Text("Tidak ada yg dirubah...")
                                  ],
                                ),
                              )
                          );

                        }
*/

                      },
                      color: Colors.white,
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.check_circle,
                              color: Colors.black,
                            ),
                            SizedBox(
                              width: 4.0,
                            ),
                            Text(
                              AppText.CART_UPDATE[Globals.langSelected],
                              style: TextStyle(color: Colors.black, fontSize: 14.0),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              );
            }
        )
    );
  }

}
