import 'package:flutter/material.dart';
import 'package:zb_store/scoped_model/cart_scoped_model.dart';

class PaymentVoucherPage extends StatefulWidget {
  @override
  PaymentVoucherPageState createState() {
    return new PaymentVoucherPageState();
  }
}

class PaymentVoucherPageState extends State<PaymentVoucherPage> {
  final _kodeVoucher = TextEditingController();
  bool _fieldVoucherIsEmpty = false;
  bool _processSending = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  CartScopedModel cartModel = new CartScopedModel();

  @override
  void dispose() {
    super.dispose();
    _kodeVoucher.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0.5,
        leading: new IconButton(
          icon: new Icon(Icons.close, color: Colors.black54),
          onPressed: ()=>Navigator.of(context).pop(),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: new Text(
            "Voucher Coupon", style: TextStyle(color: Colors.black87)),
        iconTheme: new IconThemeData(color: Colors.black54),
      ),
      body: _paymentVoucherBody(),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  _paymentVoucherBody() {
    return Theme(
      // Create a unique theme with "ThemeData"
        data: ThemeData(
          primaryColor: Colors.blue[400],
          accentColor: Colors.grey,
          hintColor: Colors.grey,
          cursorColor: Colors.blue[400],
        ),
        child: Padding(
          padding: EdgeInsets.only(top: 3),
          child: Card(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 2, horizontal: 5),
              child: ListView(
                children: <Widget>[
                  // Nama
                  TextField(
                    controller: _kodeVoucher,
                    decoration: InputDecoration(
                      labelText: 'Kode voucher/kupon',
                      errorText: _fieldVoucherIsEmpty ? 'Kode voucher/kupon tidak boleh kosong' : null,
                    ),
                  ),
                  SizedBox(height: 5.0),

                ],
              ),
            ),
          ),
        )
    );
  }

  _buildBottomNavigationBar() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Flexible(
            flex: 1,
            child: RaisedButton(
              onPressed: () {

                // validate before save
                setState(() {
                  // validate
                  _kodeVoucher.text.isEmpty ? _fieldVoucherIsEmpty = true : _fieldVoucherIsEmpty = false;
                });


                if (!_fieldVoucherIsEmpty) {

                  if (!_processSending) {

                    // send api
                    _scaffoldKey.currentState.showSnackBar(
                        new SnackBar(
                          duration: new Duration(minutes: 2),
                          content: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new CircularProgressIndicator(),
                              new Text("  Please wait...")
                            ],
                          ),
                        )
                    );

                    setState(() {
                      _processSending = true;
                    });


                    cartModel.applyVoucher(
                      _kodeVoucher.text,
                    ).then((response) {

                      print("add voucher response: " + response.toString());
                      _scaffoldKey.currentState.hideCurrentSnackBar();
                      _scaffoldKey.currentState.showSnackBar(
                          new SnackBar(
                            duration: new Duration(minutes: 10),
                            content: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Expanded(child: Text(response['message'])),
                                RaisedButton(
                                  onPressed: () {
                                    _scaffoldKey.currentState.hideCurrentSnackBar();

                                    setState(() {
                                      _processSending = false;
                                    });

                                    Navigator.of(context).pop();
                                  },
                                  color: Colors.redAccent,
                                  child: Text("OK"),
                                )
                              ],
                            ),
                          )
                      );

                    });
                  }

                } else {

                  _scaffoldKey.currentState.showSnackBar(
                      new SnackBar(
                        duration: new Duration(seconds: 2),
                        content: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text("Please complete data...")
                          ],
                        ),
                      )
                  );

                }


              },
              color: Colors.white,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.check_circle,
                      color: Colors.black,
                    ),
                    SizedBox(
                      width: 4.0,
                    ),
                    Text(
                      "GUNAKAN",
                      style: TextStyle(color: Colors.black),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

}