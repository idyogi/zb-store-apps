import 'package:flutter/material.dart';
import 'package:zb_store/model/zone.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/scoped_model/address_scoped_model.dart';
import 'address_city_page.dart';

class AddressProvincePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      //key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0.5,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: new Text(
            "Provinsi", style: TextStyle(color: Colors.black87)),
        iconTheme: new IconThemeData(color: Colors.black54),
      ),
      body: AddressProvincePageBody(),
    );
  }
}

class AddressProvincePageBody extends StatelessWidget {
  final AddressScopedModel addressModel = AddressScopedModel();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
        child: FutureBuilder<List<Zone>>(
          future: addressModel.parseProvince(Constants.API_GET_PROVINCE, ""),
          builder: (context, snapshot) {
            if (snapshot.hasError) print(snapshot.error);

            return snapshot.hasData
              ? _buildProvinceList(snapshot.data)
              : Center(child: CircularProgressIndicator());
          },
        )
    );
  }

  _buildProvinceList(List<Zone> province) {
    return new ListView.builder(
        itemCount: province.length,
        itemBuilder: (context, index) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.of(context).pushReplacementNamed(Constants.ROUTE_ADD_CITY);
                  Globals.provinceID = province[index].id;
                  Globals.provinceName = province[index].name;
/*
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) {
                        return AddressCityPage(provinceID: province[index].id);
                      },
                    )
                  );
*/
                },
                child: ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(province[index].name),
                      Icon(Icons.arrow_forward_ios, color: Colors.grey[400]),
                    ],
                  )
                ),
              ),
              Divider(),
            ],
          );
        }
    );
  }
}
