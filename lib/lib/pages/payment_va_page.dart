import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zb_store/scoped_model/cart_scoped_model.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/model/steps_payment.dart';
import 'package:zb_store/util/languages.dart';

class PaymentVirtualAccPage extends StatefulWidget {

  final String orderID;
  final String jumlahTransfer;
  final String paymentType;
  final String paymentName;
  final String paymentImage;
  final String vaNumber;
  final String expired;
  final List<StepsPayment> stepsPayment;

  PaymentVirtualAccPage({
    @required this.orderID,
    @required this.jumlahTransfer,
    @required this.paymentType,
    @required this.paymentName,
    @required this.paymentImage,
    @required this.vaNumber,
    @required this.expired,
    @required this.stepsPayment,
  });

  @override
  PaymentVirtualAccPageState createState() {
    return new PaymentVirtualAccPageState(
        orderID: orderID,
        jumlahTransfer: jumlahTransfer,
        paymentType: paymentType,
        paymentName: paymentName,
        paymentImage: paymentImage,
        vaNumber: vaNumber,
        expired: expired,
        stepsPayment: stepsPayment,
    );
  }
}

class PaymentVirtualAccPageState extends State<PaymentVirtualAccPage> {

  final String orderID;
  final String jumlahTransfer;
  final String paymentType;
  final String paymentName;
  final String paymentImage;
  final String vaNumber;
  final String expired;
  final List<StepsPayment> stepsPayment;

  PaymentVirtualAccPageState({
    @required this.orderID,
    @required this.jumlahTransfer,
    @required this.paymentType,
    @required this.paymentName,
    @required this.paymentImage,
    @required this.vaNumber,
    @required this.expired,
    @required this.stepsPayment,
  });

  File file;
  //String orderID = "";
  String base64Image = "";
  CartScopedModel cartModel = CartScopedModel();
  bool fileImageExist = false;
  bool uploadStarted = false;
  bool uploadSuccess = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Keluar dari menu pembayaran?'),
        content: new Text('Anda bisa kembali mengakses informasi ini pada menu utama: Pesanan Saya'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('Tidak'),
          ),
          new FlatButton(
            onPressed: () => Navigator.pushReplacementNamed(context, Constants.ROUTE_HOME_PAGE),
            child: new Text('Ya'),
          ),
        ],
      ),
    ) ?? false;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: 0.5,
          leading: new IconButton(
            icon: new Icon(Icons.close, color: Colors.black54),
            //onPressed: () => _onWillPop(),
            onPressed: () => Navigator.pushReplacementNamed(context, Constants.ROUTE_HOME_PAGE),

          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: new Text(
              AppText.PAYMENT_INFO_HEADER[Globals.langSelected], style: TextStyle(color: Colors.black87)),
          iconTheme: new IconThemeData(color: Colors.black54),
        ),
        body: _paymentCheckoutBody(context),
        //bottomNavigationBar: _buildBottomNavigationBar(context),
      ),
    );
  }

  _paymentCheckoutBody(BuildContext context) {

    return Container(
        padding: EdgeInsets.all(13.0),
        child: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 3.0),
              child: Text(AppText.PAYMENT_INFO_TOTAL[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.bold)),
            ),
            SizedBox(height: 5.0),
            Card(
              elevation: 1.0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                //height: 195.0,
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(jumlahTransfer != null ? jumlahTransfer : "", style: TextStyle(fontSize: 24.0, color: Colors.black, fontWeight: FontWeight.bold)),
                      SizedBox(height: 5.0),
                      //Text("Bayar pesanan sesuai jumlah di atas", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                      //SizedBox(height: 5.0),
                    ],
                  ),
                ),
              ),
            ),


            SizedBox(height: 8.0),
            (paymentType != "cstore")
            ?
            Padding(
              padding: EdgeInsets.only(left: 3.0),
              child: Text("VIRTUAL ACCOUNT:", style: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.bold))

              )

            :
            Padding
            (
            padding: EdgeInsets.only(left: 3.0),
            child: Text("COUNTER:", style: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.bold)),

            ),

            SizedBox(height: 5.0),
            Card(
              elevation: 1.0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                //height: 170.0,
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 0.0),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: <Widget>[
                                Image.network(paymentImage),
                                SizedBox(width: 10.0),
                                Text(paymentName != null ? paymentName : "", style: TextStyle(fontSize: 12.0, color: Colors.black, fontWeight: FontWeight.normal)),
                              ],
                            ),
                            //Text(snapshot.data['subtotal_items_text'] != null ? snapshot.data['subtotal_items_text'] : "", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.bold)),
                          ]
                      ),
                      //SizedBox(height: 5.0),
                      (paymentType != "cstore")
                      ? Text("Nomor Virtual Account:", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal))
                      : Text("Kode Pembayaran:", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                      SizedBox(height: 3.0),

                      (paymentType != "cstore")
                          ? Text(vaNumber != null ? vaNumber : "", style: TextStyle(fontSize: 15.0, color: Colors.grey[800], fontWeight: FontWeight.normal))
                          : Text(vaNumber != null ? vaNumber : "", style: TextStyle(fontSize: 15.0, color: Colors.grey[800], fontWeight: FontWeight.normal)),
                      SizedBox(height: 5),

                      SizedBox(
                        width: 90,
                        height: 20,
                        child: FlatButton(
                          onPressed: () {
                            Clipboard.setData(new ClipboardData(text: vaNumber));
                          },
                          color: Colors.grey[200],
                          child: Text(
                            "COPY",
                            style: TextStyle(
                              //fontWeight: FontWeight.bold,
                                fontSize: 10.0,
                                color: Colors.blue)
                          ),
                        ),
                      ),
                      SizedBox(height: 10.0),
                      Text(expired != null ? "Silahkan bayar sebelum: " + expired : "", style: TextStyle(fontSize: 12.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),

                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 8.0),

            Padding(
            padding: EdgeInsets.only(left: 3.0),
            child:     Text(AppText.PAYMENT_INFO_HOW_TO[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.bold)),


            ),

            SizedBox(height: 5.0),
            Card(
              elevation: 1.0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                //height: 170.0,
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
/*
                      SizedBox(height: 14.0),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: <Widget>[
                                //Text(paymentName != null ? paymentName : "", style: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.normal)),
                              ],
                            ),
                          ]
                      ),
*/
/*
                      SizedBox(height: 5.0),
                      Text("Title: " + stepsPayment[0].title, style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                      SizedBox(height: 3.0),
                      Text("Content: " + stepsPayment[0].content, style: TextStyle(fontSize: 12.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
*/

                      _buildPaymentSteps(),
                    ],
                  ),
                ),
              ),
            ),
/*
            SizedBox(height: 10.0),
            (uploadSuccess && fileImageExist && file != null)
              ? RaisedButton(
                  color: Colors.deepPurple[400],
                  onPressed: () {
                    Navigator.pushReplacementNamed(context, Constants.ROUTE_HOME_PAGE);
                  },
                  child: Text(
                      "KEMBALI BELANJA",
                      style: TextStyle(
                        //fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 15.0)
                  )
                )
              : RaisedButton(
                  color: Colors.deepPurple[400],
                  onPressed: null,
                  child: Text(
                      "KEMBALI BELANJA",
                      style: TextStyle(
                        //fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 15.0)
                  )
                ),
*/
          ],
        )
    );
  }

  _buildPaymentSteps() {
    List<Widget> tutorial = List();
    for (int i=0; i < stepsPayment.length; i++) {
      tutorial.add(
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 10.0),
            Text(stepsPayment[i].title, style: TextStyle(fontSize: 14.0, color: Colors.grey[800], fontWeight: FontWeight.normal)),
            SizedBox(height: 3.0),
            _buildStepsCategory(stepsPayment[i].content),
          ],
        )
      );
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: tutorial,
    );
  }

  _buildStepsCategory(String content) {
    var splitContent = content.split("|");
    List<Widget> steps = List();
    for (int i=0; i < splitContent.length; i++) {
      steps.add(
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 3.0),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("${i+1}. ", style: TextStyle(fontSize: 12.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                  Expanded(
                    child: Text(splitContent[i], style: TextStyle(fontSize: 12.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                  ),
                ],
              ),
            ],
          )
      );
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: steps,
    );
  }

  _buildBottomNavigationBar(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Flexible(
            flex: 1,
            child: /*(uploadSuccess && fileImageExist && file != null) ?*/
            RaisedButton(
              onPressed: () {
                Navigator.pushReplacementNamed(context, Constants.ROUTE_HOME_PAGE);
              },
              color: Colors.redAccent,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.check_circle,
                      color: Colors.white,
                    ),
                    SizedBox(
                      width: 4.0,
                    ),
                    Text(
                      "KEMBALI BELANJA",
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
            )

/*
                :             RaisedButton(
              onPressed: null,
              color: Colors.greenAccent,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.check_circle,
                      color: Colors.white,
                    ),
                    SizedBox(
                      width: 4.0,
                    ),
                    Text(
                      "KEMBALI BELANJA",
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
*/


          ),
        ],
      ),
    );
  }

}
