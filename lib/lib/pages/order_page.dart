import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:zb_store/model/orders.dart';
import 'package:zb_store/scoped_model/cart_scoped_model.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/util/languages.dart';
import 'home_page.dart';
import 'order_detail_page.dart';
import 'package:zb_store/widgets/home_drawer.dart';

import 'order_web_page.dart';

class OrderPage extends StatefulWidget {
  @override
  OrderPageState createState() {
    return new OrderPageState();
  }
}

class OrderPageState extends State<OrderPage> {
  CartScopedModel cartModel = CartScopedModel();
  int countUnpaid;
  int countDikemas;
  int countDikirim;
  int countSelesai;

  @override
  Widget build(BuildContext context) {

    Future<bool> _onWillPop() {
      Navigator.of(context).pop();
      Route route = MaterialPageRoute(builder: (context) => HomePage());
      Navigator.pushReplacement(context, route);
/*
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) {
            return HomePage();
          },
        ),
      );
*/

      return null;
    }


    return WillPopScope(
      //onWillPop: _onWillPop,
      child: FutureBuilder<dynamic>(
          future: cartModel.getOrders(),
          builder: (context, snapshot) {

            if (snapshot.hasData && snapshot.data['status'] != "error") {
              //setState(() {
              countUnpaid = snapshot.data['data']['unpaid']['count'];
              countDikemas = snapshot.data['data']['packing']['count'];
              countDikirim = snapshot.data['data']['shipping']['count'];
              countSelesai = snapshot.data['data']['completed']['count'];
              //});
            }

            return DefaultTabController(
                length: 4,
                child: Scaffold(
                  //key: _scaffoldKey,
                  appBar: AppBar(
                    centerTitle: true,
                    elevation: 1,
                    backgroundColor: Colors.white,
                    title: new Text(AppText.HOME_ORDER[Globals.langSelected], style: TextStyle(color: Colors.black87)),
                    iconTheme: new IconThemeData(color: Colors.black54),
/*
                    leading: IconButton(
                      icon: Icon(
                        Icons.chevron_left,
                        size: 40.0,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        //Navigator.of(context).pop();
                        _onWillPop();
                      },
                    ),
*/
                    bottom: TabBar(
                      isScrollable: true,
                      indicatorColor: Colors.redAccent,
                      indicatorWeight: 3.0,
                      labelColor: Colors.black54,
                      //labelStyle: TextStyle(color: Colors.black87, fontSize: 12),
                      tabs: [
                        Tab(icon: Icon(Icons.monetization_on), text: "${AppText.HOME_MY_ORDER_UNPAID[Globals.langSelected]} ($countUnpaid)"),
                        Tab(icon: Icon(Icons.card_travel), text: "${AppText.HOME_MY_ORDER_PACKING[Globals.langSelected]} ($countDikemas)"),
                        Tab(icon: Icon(Icons.airplanemode_active), text: "${AppText.HOME_MY_ORDER_SENT[Globals.langSelected]} ($countDikirim)"),
                        Tab(icon: Icon(Icons.check_circle), text: "${AppText.HOME_MY_ORDER_COMPLETED[Globals.langSelected]} ($countSelesai)"),
                        //Tab(icon: Icon(Icons.delete_forever), text: "Dibatalkan"),
                      ],
                    ),
                  ),
                  body: snapshot.hasData
                      ? (snapshot.data['status']=="error")

                      ? TabBarView(
                    children: [
                      Center(child: Text(snapshot.data['message'])),
                      Center(child: Text(snapshot.data['message'])),
                      Center(child: Text(snapshot.data['message'])),
                      Center(child: Text(snapshot.data['message'])),
                    ],
                  )

                      : TabBarView(
                    children: [
                      _buildTabView(snapshot.data['data']['unpaid']['link'].toString()),
                      _buildTabView(snapshot.data['data']['packing']['link'].toString()),
                      _buildTabView(snapshot.data['data']['shipping']['link'].toString()),
                      _buildTabView(snapshot.data['data']['completed']['link'].toString()),
                    ],
                  )

                      : _buildSplashScreen(),
                  //bottomNavigationBar: _buildBottomNavigationBar(context)
                  //drawer: HomeDrawer(),
                )
            );
          }
      ),
    );
  }

  _buildSplashScreen() {

    return Stack(
      children: <Widget>[
        Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
        ),

        Center(
          child: GlowingProgressIndicator(
              duration: Duration(milliseconds: 400),
              child: Container(
                  height: 70.0,
                  width: 70.0,
                  child: Image.asset('assets/images/logo_zb_10.png', color: Colors.black54)
              )
          ),
        ),

/*
          Container(
            height: double.infinity,
            width: double.infinity,
          ),
*/
      ],
    );
  }


  _buildTabView(String url) {
    return FutureBuilder<List<Orders>>(
      future: cartModel.getOrdersCategory(url),
        builder: (context, snapshot) {
          return snapshot.hasData
            ? (snapshot.data.length > 0 ? _orderList(snapshot.data) : Center(child: Text(AppText.HOME_MY_ORDER_EMPTY[Globals.langSelected])))
            : _buildSplashScreen();
        }
    );
  }

  _orderList(List<Orders> orders) {
    return new ListView.builder(
      itemCount: orders.length,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return OrderWebPage(link: orders[index].link);
                  },
                )
            );

          },
          child: Card(
            elevation: 1.0,
            child: ListTile(
              title: Text("ID: " + orders[index].orderID),
              subtitle: Text("Shipment: " + orders[index].shippingCourier + " - " + orders[index].shippingService + "\n" + "Order status: " + orders[index].status),
            ),
          ),
        );
      }
    );
  }

}