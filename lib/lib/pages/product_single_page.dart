import 'dart:io';
import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:skeleton_text/skeleton_text.dart';
import 'package:zb_store/model/product.dart';
import 'package:zb_store/pages/cart_list_page.dart';
import 'package:zb_store/pages/product_image_page.dart';
import 'package:zb_store/scoped_model/cart_scoped_model.dart';
import 'package:zb_store/scoped_model/product_scoped_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zb_store/util/globals.dart';
import 'package:photo_view/photo_view.dart';
import 'package:zb_store/util/languages.dart';
import 'package:zb_store/util/network.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'product_desc_page.dart';
import 'product_size_page.dart';

class ProductSinglePage extends StatefulWidget {
  final String productURL;
  ProductSinglePage({this.productURL});

  @override
  ProductSinglePageState createState() => ProductSinglePageState(productURL: productURL);
}

class ProductSinglePageState extends State<ProductSinglePage> with TickerProviderStateMixin {
  final String productURL;
  ProductSinglePageState({this.productURL});

  bool processSendingCart = false;
  bool buttonFavePressed = false;

  String _cutNotes = "";
  List<int> _potongJadi  = new List();
  CartScopedModel cartModel = CartScopedModel();
  ProductsScopedModel productModel = ProductsScopedModel();
  Network network = new Network();
  TabController imagesController;
  TabPageSelector tabPageSelector;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final AsyncMemoizer _asyncMemoizer = AsyncMemoizer();
  Product product = new Product();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    buttonFavePressed = false;
    product.cutQuantity = 0;
    product.itemQuantity = 1;
    //if (Globals.imageIndex != null) imagesController.animateTo(Globals.imageIndex);
  }

  _getProductsByURL(url)  {

    String token;
    Future getSavedToken() async {
      final prefs = await SharedPreferences.getInstance();
      final alreadyLogin = prefs.getBool(Constants.IS_GOOGLE_LOGIN) ?? false;
      if (alreadyLogin) {
        token = prefs.getString(Constants.SAVED_GOOGLE_TOKEN) ?? "";
      } else {
        token = prefs.getString(Constants.SAVED_FACEBOOK_TOKEN) ?? "";
      }
    }

    return _asyncMemoizer.runOnce(() async {

      await getSavedToken();

      var response = await http.get(
        url,
        headers: { "X-Authorization":token},
      ).catchError((error) {
          return false;
        },
      );
      var dataFromResponse = json.decode(response.body)['data'];

/*
      List<String> imagesOfProductList = [];
      dataFromResponse["images"].forEach((newImage) {
        imagesOfProductList.add(newImage.toString().replaceAll(" ", "%20"));
      });
*/

      List<String> sizeOfProductList = [];
      dataFromResponse["variations"]["Size"].forEach((item) {
        sizeOfProductList.add(item.toString());
      });

      List<String> colorOfProductList = [];
      dataFromResponse["variations"]["Color"].forEach((item) {
        colorOfProductList.add(item.toString());
      });

      var color = dataFromResponse["variations"]["Color"][0];
      var size = dataFromResponse["variations"]["Size"][0];
      var colorSize = "$color,$size";

      List<String> imagesOfProductList = [];
      dataFromResponse["matching"][colorSize]["images"].forEach((newImage) {
        imagesOfProductList.add(newImage.toString().replaceAll(" ", "%20"));
      });

      Product product = new Product(
        productId: dataFromResponse["id"],
        productName: dataFromResponse["name"],
        description: dataFromResponse["description"],
        regularPrice: dataFromResponse["price"],
        salePrice: dataFromResponse["price_sale"],
        productUrl: dataFromResponse["product_url"],
        isFavorite: dataFromResponse["is_favorite"],
        cutOption: dataFromResponse["cut_option"],
        imageSize: dataFromResponse["product_detail"],
        //colorAvailable: dataFromResponse["color_available"],
        discount: ((((dataFromResponse["price"] -
            dataFromResponse["price_sale"]) /
            (dataFromResponse["price"])) *
            100))
            .round(),
        images: imagesOfProductList,
        size: sizeOfProductList,
        color: colorOfProductList,
        selectedColor: color,
        selectedSize: size,
        skuId: dataFromResponse["matching"][colorSize]["sku_id"],
        skuPriceText: dataFromResponse["matching"][colorSize]["price_text"],
        skuSalePriceText: dataFromResponse["matching"][colorSize]["price_sale_text"],
        ifSaleSku: (dataFromResponse["matching"][colorSize]["price_text"] !=
            dataFromResponse["matching"][colorSize]["price_sale_text"]),
        matching: dataFromResponse["matching"],
        preOrder: dataFromResponse["preorder"],
        preOrderDate: dataFromResponse["preorder_date"],
      );
      product.itemQuantity = 1;
      product.cutQuantity = 0;
      product.stockQuantity = 0;
      print("product_single_page product name: " + product.productName);
      print("product_single_page product is fave: " + product.isFavorite.toString());
      print("product_single_page cut option: " + product.cutOption.toString());
      //print("product_single_page color_available: " + product.colorAvailable.toString());
      return product;
    });
  }


  @override
  Widget build(BuildContext context) {
    ProductsScopedModel productModel = ProductsScopedModel();

    Future<bool> _onWillPop() {
      Navigator.of(context).pop();
      if (buttonFavePressed) {
        print("product_single_page _onWillPop product.name:" + product.productName);
        print("product_single_page _onWillPop product.isFavorite:" + productModel.getFavorite(product).toString());
        FutureBuilder<dynamic>(
            future: productModel.getFavorite(product)
                ? productModel.addFave(product, product.productId)
                : productModel.removeFave(product, product.productId),
            builder: (context, snapshot) {
              (snapshot.hasData)
                  ? print(snapshot.data)
                  : print("no data");
            }
        );
      }
      return null;
    }

    String brandName;
    if (Globals.brandID == 1) {
      brandName = 'assets/images/logo_zb_1.png';
    } else if (Globals.brandID == 2) {
      brandName = 'assets/images/logo_zb_2.png';
    } else if (Globals.brandID == 3) {
      brandName = 'assets/images/logo_zb_3.png';
    }


    _buildSplashScreen() {

      return Stack(
        children: <Widget>[
          Container(
            height: double.infinity,
            width: double.infinity,
            color: Colors.white,
          ),

          Center(
            child: GlowingProgressIndicator(
                duration: Duration(milliseconds: 400),
                child: Container(
                    height: 70.0,
                    width: 70.0,
                    child: (Globals.brandID == 1)
                      ? Image.asset('assets/images/logo_zb_10.png', color: Colors.black54)
                      : (Globals.brandID == 2)
                        ? Image.asset('assets/images/logo_zb_20.png', color: Colors.black54)
                        : Image.asset('assets/images/logo_zb_30.png', color: Colors.black54),
                )
            ),
          ),

/*
          Container(
            height: double.infinity,
            width: double.infinity,
          ),
*/
        ],
      );
    }

    return WillPopScope(
      onWillPop: _onWillPop,
      child: new ScopedModel<ProductsScopedModel>(
        model: productModel,
        child: Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              elevation: 0,
              centerTitle: true,
              leading: IconButton(
                icon: Icon(
                  Icons.chevron_left,
                  size: 40.0,
                  color: Colors.black,
                ),
                onPressed: () {
                  //Navigator.of(context).pop();
                  _onWillPop();
                },
              ),
              backgroundColor: Colors.white,
              title: new Image.asset(brandName, width: 120.0, height: 40.0,),
/*
              title: Text(
                AppText.PRODUCT_HEADER[Globals.langSelected],
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
*/
              //iconTheme: new IconThemeData(color: Colors.black),
              actions: <Widget>[

                InkWell(
                  onTap: () {
                    //Navigator.pushNamed(context, Constants.ROUTE_CART_LIST);
                    _onWillPop();
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) {
                          return CartListPage();
                        },
                      ),
                    );
                  },
                  child: Stack(
                    children: <Widget>[
                      new IconButton(
                        icon: Icon(Icons.shopping_cart, color: Colors.black),
                      ),

                      FutureBuilder<int>(
                          future: network.getNotificationBadge(),
                          builder: (context, snapshot) {
                            return (snapshot.hasData && snapshot.data != 0)
                                ? new Positioned(
                              right: 6,
                              top: 5,
                              child: new Container(
                                padding: EdgeInsets.all(2),
                                decoration: new BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.circular(6),
                                ),
                                constraints: BoxConstraints(
                                  minWidth: 14,
                                  minHeight: 14,
                                ),
                                child: Text(
                                  snapshot.data.toString(),
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 8,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            )
                                : new Container();
                          }
                      ),
                    ],
                  ),
                ),

              ],
            ),
            body: FutureBuilder(
                future: _getProductsByURL(productURL),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    product = snapshot.data;
                    //print("API fired!");
                  }
                  return (snapshot.hasData)
                      ? _buildProductDetailsPage() // nitip notif timeout
                      : _buildSplashScreen();
                      //: Center(child: CircularProgressIndicator());
                }
            ),

            bottomNavigationBar: _buildBottomNavigationBar()
        ),
      ),
    );
  }


  _buildProductDetailsPage() {
    //ProductsScopedModel productModel = ProductsScopedModel();
    Size screenSize = MediaQuery.of(context).size;

    return  Container(
      padding: const EdgeInsets.all(4.0),
      child:
            ListView(
              children:<Widget>[
                Card(
                  elevation: 4.0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[

                      Center(
                        child: Container(
                          width: 270,
                          child: Column(
                            children: <Widget>[

                              _buildProductImagesWidgets(),
                              _buildTitleWidgets(),
                              SizedBox(height: 12.0),
                              _buildDivider(screenSize),
                              // size
                              SizedBox(height: 12.0),

                              _buildSizeWidgets(),
                              // color
                              SizedBox(height: 15.0),
                              _buildColorWidgets(),
                              // item qty
                              SizedBox(height: 15.0),
                              _buildItemQuantityWidgets(),
                              // cut qty
                              SizedBox(height: 12.0),
                              _buildCutQuantityWidgets(),
                              // cut note
                              SizedBox(height: 12.0),
                              //_buildCutNoteWidgets(),
                              // cut item
                              _buildCutItemWidget(),

                              _buildDivider(screenSize),

                              SizedBox(height: 16.0),
                              _buildDescriptionAndDetail(screenSize),

                            ],
                          ),
                        ),
                      ),
/*
                      Padding(
                          padding: EdgeInsets.only(left: 8.0, right: 8.0),
                          child: ,
                      ),
*/
                      //_showCutNote(),

/*                      SizedBox(height: 12.0),
                      _buildDetailsAndMaterialWidgets(),

                      SizedBox(height: 12.0),
                      //SizedBox(height: 20.0),
                      _buildMoreInfoHeader(),
                      SizedBox(height: 6.0),
                      _buildDivider(screenSize),
                      SizedBox(height: 4.0),
                      _buildMoreInfoData(),
*/
                      SizedBox(height: 24.0),
                    ],
                  ),
                ),
              ],
            )
          //  : Center(child: CircularProgressIndicator());

      //    }
      //  ),


      );
  }

  _buildDescriptionAndDetail(Size screenSize) {
    return /*Padding(
      padding: EdgeInsets.only(left: 0.0, right: 0.0),
      child: */Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[

          InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return ProductDescPage(description: product.description);
                  },
                ),
              );
            },
            child: Text(
              AppText.PRODUCT_DESCRIPTION[Globals.langSelected],
              style: TextStyle(
                color: Colors.blue[400],
                fontSize: 14.0,
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return ProductSizePage(imageURL: product.imageSize);
                  },
                ),
              );
            },
            child: Text(
              AppText.PRODUCT_SIZE_DETAILS[Globals.langSelected],
              style: TextStyle(
                color: Colors.blue[400],
                fontSize: 14.0,
              ),
            ),
          ),

        ],
      );
    //);
  }


  _buildDivider(Size screenSize) {
    return Column(
      children: <Widget>[
        Container(
          color: Colors.grey[600],
          width: screenSize.width,
          height: 0.25,
        ),
      ],
    );
  }

  _buildProductImagesWidgets() {
    imagesController = TabController(length: product.images.length, vsync: this);
    tabPageSelector = TabPageSelector(
      controller: imagesController,
      selectedColor: Colors.grey,
      color: Colors.white,
    );


    return ScopedModelDescendant<ProductsScopedModel>(
      builder: (context,child,model)
      {
      return Padding(
        padding: const EdgeInsets.all(12.0),
        child: Center(
          child: Container(
            width: 270,
            height: 405,
            child: Center(
              child: DefaultTabController(
                length: product.images.length,
                child: Stack(
                  children: <Widget>[

                    SkeletonAnimation(
                      child: Container(
                        color: Colors.grey[200],
                        child: Center(
                          child: Container(
                            height: 70.0,
                            width: 70.0,
                            child: (Globals.brandID == 1)
                              ? Image.asset('assets/images/logo_zb_10.png', color: Colors.white)
                              : (Globals.brandID == 2)
                              ? Image.asset('assets/images/logo_zb_20.png', color: Colors.white)
                              : Image.asset('assets/images/logo_zb_30.png', color: Colors.white),
                          ),
                        ),
                      ),
                    ),


                    TabBarView(
                      controller: imagesController,
                      children: model.getImageSet(product).map(
                            (image) {
                          // zoom 1
/*
                        return PhotoView(
                          minScale: PhotoViewComputedScale.contained * 1.0,
                          maxScale: PhotoViewComputedScale.contained * 4.0,
                          imageProvider: NetworkImage(image),
                          backgroundDecoration: BoxDecoration(
                              color: Colors.white),
                        );
*/
                          // zoom 2
                          //return ZoomableImage(new NetworkImage(image));
                          // no zoom
                          //return Image.network(image);
                          return InkWell(
                            child: Hero(
                              tag: 'imageHero',
                              child: image != null
                                ? Image.network(image, fit: BoxFit.cover)
                                : Container()
                            ),
                            onTap: () {
                              //Globals.imageIndex = imagesController.index;
                              //print("ProductListitem imageIndex=$imageIndex");
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) {
                                    return ProductImagePage(imageURL: image);
                                  },
                                ),
                              );
                            },
                          );

                        },
                      ).toList(),
                    ),

                    // sale
                    (product.ifSaleSku)?
                    Padding(
                      padding: EdgeInsets.only(left: 5.0, top: 12.0),
                      child: Container(
                        alignment: Alignment(-1.0, -1.0),
                        width: 50.0,
                        height: 20.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.redAccent
                        ),
                        child: Center(child: Text("SALE", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold))),
                      ),
                    )
                    :Container(),

                    // pre order
                    (product.preOrder == 1)?
                    Padding(
                      padding: EdgeInsets.only(left: 5.0, top: 12.0),
                      child: Container(
                        alignment: Alignment(-1.0, -1.0),
                        width: 50.0,
                        height: 20.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.grey
                        ),
                        child: Center(child: Text("PO", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold))),
                      ),
                    )
                    :Container(),

                    // favorite
                    (Globals.alreadyLogin)
                    ?Container(
                      alignment: Alignment(1.0, -1.0),
                      child: IconButton(
                          icon: Opacity(
                            opacity: 0.7,
                            child: model.getFavorite(product)
                                ? Icon(
                              Icons.favorite,
                              size: 30.0,
                              color: Colors.grey[700],
                            )
                                : Icon(
                              Icons.favorite_border,
                              size: 30.0,
                              color: Colors.grey[700],
                            ),
                          ),
                          onPressed: () {

                            buttonFavePressed = true;
                            if (product.isFavorite == 0) {
                              model.setFavorite(product, true);
                            } else {
                              model.setFavorite(product, false);
                            }

                        },
                      ),
                    )
                    :Container(),

                    Container(
                      alignment: FractionalOffset(0.5, 0.95),
                      child: tabPageSelector

/*
                      TabPageSelector(
                        controller: imagesController,
                        selectedColor: Colors.grey,
                        color: Colors.white,
                      ),
*/
                    )

                  ],
                ),
              ),
            ),
          ),
        ),
      );
    }
    );
  }

/*
  _buildProductTitleWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0),
      child: Center(
        child: Text(
          //name,
          product.productName,
          style: TextStyle(fontSize: 16.0, color: Colors.black),
        ),
      ),
    );
  }
*/

  _buildTitleWidgets() {

    return ScopedModelDescendant<ProductsScopedModel>(
        builder: (context,child,model) {
          return Padding(
            padding: const EdgeInsets.only(top: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text(
                  //name,
                  product.productName,
                  style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),
                ),

                Column(
                  children: <Widget>[
                    Text(
                      model.getSkuSalePrice(product),
                      style: TextStyle(fontSize: 16.0, color: Colors.black),
                    ),
                    // sale
                    (product.ifSaleSku)
                    ?Text(
                      model.getSkuPrice(product),
                      style: TextStyle(fontSize: 12.0, color: Colors.grey, decoration: TextDecoration.lineThrough),
                    )
                    : Container(),
                    // pre order
                    (product.preOrder == 1)
                    ?Text(
                      AppText.PRODUCT_PRE_ORDER[Globals.langSelected] + product.preOrderDate,
                      style: TextStyle(fontSize: 12.0, color: Colors.grey),
                    )
                        : Container(),
                  ],
                )
              ],
            ),
          );
        }
    );
  }

  _buildSizeWidgets() {

    Widget cupertinoActionItem(String sizeText, ProductsScopedModel model) {
      return CupertinoActionSheetAction(
        child: Text(sizeText),
        onPressed: () {
          if (this.mounted) {
            model.setSelectedSize(product, sizeText);
            model.setImageByColorSize(product, product.selectedColor, product.selectedSize);
            String colorSize = product.selectedColor + "," + product.selectedSize;
            product.skuId = product.matching['$colorSize']['sku_id'];
            String skuPrice = product.matching['$colorSize']['price_text'];
            model.setSkuPrice(product, skuPrice);
            String skuSalePrice = product.matching['$colorSize']['price_sale_text'];
            model.setSkuSalePrice(product, skuSalePrice);
            Navigator.pop(context);

          }
        },
      );
    }

    return ScopedModelDescendant<ProductsScopedModel>(
        builder: (context,child,model) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
/*
                    Icon(
                      Icons.straighten,
                      color: Colors.grey[600],
                    ),

                    SizedBox(
                      width: 12.0,
                    ),
*/
                    Text(
                      AppText.PRODUCT_SIZE[Globals.langSelected],
                      style: TextStyle(
                        color: Colors.grey[600],
                      ),
                    ),

                    SizedBox(
                      width: 5.0,
                    ),


                    Text(
                      product.selectedSize,
                      style: TextStyle(
                        color: Colors.grey[600],
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ],
                ),
                InkWell(
                  //onTap: () => _cupertinoPicker(listSize),
                  onTap: () {
                    List<Widget> listCupertinoSize = new List();
                    for (int i=0; i<product.size.length; i++) {
                      listCupertinoSize.add(cupertinoActionItem(product.size[i], model));
                    }
                    showCupertinoModalPopup(
                      context: context,
                      builder: (BuildContext context) => CupertinoActionSheet(
                        title: Text(AppText.PRODUCT_SELECT_SIZE[Globals.langSelected]),
                        //message: const Text('Ukuran yang tersedia '),
                        actions: listCupertinoSize,
                      ),
                    );
                  },
                  child: Text(
                    AppText.CHANGE[Globals.langSelected],
                    style: TextStyle(
                      color: Colors.blue[400],
                      fontSize: 14.0,
                    ),
                  ),
                ),
              ],
            ),
          );
        }
    );
  }

  _buildColorWidgets() {

    Widget cupertinoActionItem(int index, String sizeText, ProductsScopedModel model) {
      return CupertinoActionSheetAction(
        child: Text(sizeText),
        onPressed: () {
          //if (product.images.length > 1) imagesController.animateTo(index);
          if (this.mounted) {
            imagesController.animateTo(0);
            model.setSelectedColor(product, sizeText);
            String colorSize = product.selectedColor + "," + product.selectedSize;
            model.setImageByColorSize(product, product.selectedColor, product.selectedSize);

            product.skuId = product.matching['$colorSize']['sku_id'];

            String skuPrice = product.matching['$colorSize']['price_text'];
            model.setSkuPrice(product, skuPrice);

            String skuSalePrice = product.matching['$colorSize']['price_sale_text'];
            model.setSkuSalePrice(product, skuSalePrice);

            imagesController = TabController(length: product.images.length, vsync: this);
            //imagesController.animateTo(0);
            tabPageSelector = TabPageSelector(
              controller: imagesController,
              selectedColor: Colors.grey,
              color: Colors.white,
            );

            Navigator.pop(context);
          }
        },
      );
    }

    return ScopedModelDescendant<ProductsScopedModel>(
        builder: (context,child,model) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
/*
                    Icon(
                      Icons.color_lens,
                      color: Colors.grey[600],
                    ),

                    SizedBox(
                      width: 12.0,
                    ),
*/
                    Text(
                      AppText.PRODUCT_COLOR[Globals.langSelected],
                      style: TextStyle(
                        color: Colors.grey[600],
                      ),
                    ),

                    SizedBox(
                      width: 5.0,
                    ),

                    Text(
                      product.selectedColor,
                      style: TextStyle(
                        color: Colors.grey[600],
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ],
                ),
                InkWell(
                  onTap: () {
                    List<Widget> listCupertinoColor = new List();
                    for (int i = 0; i < product.color.length; i++) {
                      listCupertinoColor.add(
                          cupertinoActionItem(i, product.color[i], model));
                    }
                    showCupertinoModalPopup(
                      context: context,
                      builder: (BuildContext context) =>
                          CupertinoActionSheet(
                            title: Text(AppText.PRODUCT_SELECT_COLOR[Globals.langSelected]),
                            //message: const Text('Ukuran yang tersedia '),
                            actions: listCupertinoColor,
                          ),
                    );
                  },
                  child: Text(
                    AppText.CHANGE[Globals.langSelected],
                    style: TextStyle(
                      color: Colors.blue[400],
                      fontSize: 14.0,
                    ),
                  ),
                ),
              ],
            ),
          );
        }
    );
  }

  _buildItemQuantityWidgets() {

    return ScopedModelDescendant<ProductsScopedModel>(
        builder: (context,child,model) {
          // always reset cut qty when this selected
          //product.cutQuantity = 0;

          //print("selected size: " + product.selectedSize);
          //print("selected color: " + product.selectedColor);
          String colorSize = product.selectedColor + "," + product.selectedSize;
          product.stockQuantity = product.matching[colorSize]['qty'];
          // if stock habis item qty = 0
          if (product.stockQuantity == 0) model.setItemQuantity(product, 0);
          if (product.stockQuantity != 0 && product.itemQuantity == 0) model.setItemQuantity(product, 1);

          return (product.stockQuantity != 0)
              ? Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
/*
                    Icon(
                      Icons.add_shopping_cart,
                      color: Colors.grey[600],
                    ),
                    SizedBox(
                      width: 12.0,
                    ),
*/
                    Text(
                      AppText.PRODUCT_CHOSE_QTY[Globals.langSelected],
                      style: TextStyle(
                        color: Colors.grey[600],
                      ),
                    ),

                    SizedBox(
                      width: 5.0,
                    ),

                    Text(
                      product.itemQuantity.toString(),
                      style: TextStyle(
                        color: Colors.grey[600],
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                InkWell(
                  onTap: () {
/*
                      List<Widget> listCupertinoQty = new List();
                      for (int i = 1; i < product.stockQuantity+1; i++) {
                        listCupertinoQty.add(cupertinoActionItem(i, model));
                      }
                      showCupertinoModalPopup(
                        context: context,
                        builder: (BuildContext context) =>
                            CupertinoActionSheet(
                              title: const Text('Pilih Jumlah'),
                              message: const Text('Jumlah yang tersedia '),
                              actions: listCupertinoQty,
                            ),
                      );
*/
                    _cutNotes = "";
                    showModalBottomSheet(
                        context: context,
                        builder: (BuildContext context) {
                          return Container(
                              height: 200.0,
                              color: Colors.grey[100],
                              child: Column(
                                children: <Widget>[
                                  SizedBox(height: 15),
                                  Text(AppText.PRODUCT_SELECT_ITEM_QTY[Globals.langSelected], style: TextStyle(fontSize: 18)),
                                  Expanded(
                                    child: CupertinoPicker(
                                      backgroundColor: Colors.grey[100],
                                      scrollController:
                                      new FixedExtentScrollController(
                                        initialItem: product.itemQuantity - 1,
                                      ),
                                      itemExtent: 38.0,
                                      //magnification: 1.0,
                                      onSelectedItemChanged: (int index) {
                                        int _selectedQty = index+1;
                                        model.setItemQuantity(product, _selectedQty);
                                      },
                                      children: new List<Widget>.generate(product.stockQuantity,
                                              (int index) {
                                            return new Center(
                                              child: new Text('${index+1}', style: TextStyle(fontSize: 22)),
                                            );
                                          }
                                      ),
                                    ),
                                  ),
                                ],
                              )
                          );
                        }
                    );

                    //setState(() {
                    // print("final qty: "+product.itemQuantity.toString());
                    //});

                  },
                  child: Text(
                    AppText.CHANGE[Globals.langSelected],
                    style: TextStyle(
                      color: Colors.blue[400],
                      fontSize: 14.0,
                    ),
                  ),
                ),
              ],
            ),
          )
              : Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
/*
                          Icon(
                            Icons.add_shopping_cart,
                            color: Colors.grey[600],
                          ),
                          SizedBox(
                            width: 12.0,
                          ),
*/
                          Text(
                            AppText.PRODUCT_CHOSE_QTY[Globals.langSelected],
                            style: TextStyle(
                              color: Colors.grey[600],
                            ),
                          ),

                          SizedBox(
                            width: 5.0,
                          ),

                          Text(
                            "0",
                            style: TextStyle(
                              color: Colors.grey[600],
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      InkWell(
                        onTap: null,
                        child: Text(
                          Constants.PRODUCT_DETAIL_SOLD_OUT,
                          style: TextStyle(
                            color: Colors.grey[400],
                            fontSize: 12.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 16.0),
                  Center(
                    child: Text(product.selectedSize + " - " + product.selectedColor + AppText.PRODUCT_SOLD_OUT[Globals.langSelected]),
                  )
                ],
              )
          );
        }
    );
  }

  _buildCutQuantityWidgets() {

    return ScopedModelDescendant<ProductsScopedModel>(
        builder: (context,child,model) {
          return (product.cutOption == 1 && product.stockQuantity != 0 && product.itemQuantity != 0) ?
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(
                      Icons.content_cut,
                      color: Colors.grey[600],
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Text(
                      /*AppText.PRODUCT_CUT_QUANTITY[Globals.langSelected] + ": " +*/ product.cutQuantity.toString(),
                      style: TextStyle(
                        color: Colors.grey[600],
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                InkWell(
                  onTap: () {
                    _cutNotes = "";
                    showModalBottomSheet(
                        context: context,
                        builder: (BuildContext context) {
                          return Container(
                              height: 200.0,
                              color: Colors.grey[100],
                              child: Column(
                                children: <Widget>[
                                  SizedBox(height: 15),
                                  Text(AppText.PRODUCT_SELECT_CUT_QTY[Globals.langSelected], style: TextStyle(fontSize: 18)),
                                  Expanded(
                                    child: CupertinoPicker(
                                      backgroundColor: Colors.grey[100],
                                      scrollController:
                                      new FixedExtentScrollController(
                                        initialItem: product.cutQuantity,
                                      ),
                                      itemExtent: 38.0,
                                      //magnification: 1.0,
                                      onSelectedItemChanged: (int index) {
                                        int _selectedQty = index;
                                        model.setCutQuantity(product, _selectedQty);
                                      },
                                      children: new List<Widget>.generate(product.itemQuantity + 1,
                                              (int index) {
                                            return new Center(
                                              child: new Text('$index', style: TextStyle(fontSize: 22)),
                                            );
                                          }
                                      ),
                                    ),
                                  ),
                                ],
                              )
                          );
                        }
                    );

                  },
                  child: Text(
                    AppText.CHANGE[Globals.langSelected],
                    style: TextStyle(
                      color: Colors.blue[400],
                      fontSize: 14.0,
                    ),
                  ),
                ),
              ],
            ),
          )
              : Container();
        }
    );
  }

  _buildCutNoteWidgets() {
    return ScopedModelDescendant<ProductsScopedModel>(
        builder: (context,child,model)
        {
          return (model.getStockQuantity(product) > 0 && model.getCutQuantity(product) > 0) ?
          //return (model.getCutQuantity(product) != 0) ?
          Padding(
            padding: const EdgeInsets.only(left: 12.0, bottom: 12.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(
                      Icons.note_add,
                      color: Colors.grey[600],
                    ),
                    SizedBox(
                      width: 12.0,
                    ),
                    Text(
                      AppText.PRODUCT_CUT_NOTE[Globals.langSelected],
                      style: TextStyle(
                        color: Colors.grey[600],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
              :
          Container();
        }
    );
  }

  _buildCutItemDialog(int i, int height, int cutRange) {

    return ScopedModelDescendant<ProductsScopedModel>(
        builder: (context,child,model)
        {
          //_potongJadi = [model.getCutQuantity(product)];

          return (model.getCutQuantity(product) != 0)
              ? Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //SizedBox(width: 15.0),
              InkWell(
                  onTap: () {
                    _cutNotes = "";
                    showModalBottomSheet(
                        context: context,
                        builder: (BuildContext context) {
                          return Container(
                              height: 200.0,
                              color: Colors.grey[100],
                              child: Column(
                                children: <Widget>[
                                  SizedBox(height: 15),
                                  //Text("Barang ke-$i dipotong jadi:", style: TextStyle(fontSize: 18)),
                                  Text(AppText.PRODUCT_CUT_ITEM[Globals.langSelected]+"$i"+AppText.PRODUCT_CUT_TO[Globals.langSelected], style: TextStyle(fontSize: 18)),
                                  Expanded(
                                    child: CupertinoPicker(
                                      backgroundColor: Colors.grey[100],
                                      scrollController:
                                      new FixedExtentScrollController(
                                        //initialItem: 90,
                                      ),
                                      itemExtent: 38.0,
                                      //magnification: 1.0,
                                      onSelectedItemChanged: (int index) {
                                        if (this.mounted) {
                                          setState(() {
                                            _potongJadi[i-1] = height - index;
                                          });
                                        }
                                      },
                                      children: new List<Widget>.generate(height - cutRange,
                                              (int index) {
                                            return new Center(
                                              child: new Text('${height-index}', style: TextStyle(fontSize: 22)),
                                            );
                                          }
                                      ),
                                    ),
                                  ),
                                ],
                              )
                          );
                        }
                    );

                    //_cutNote = _potongJadi[i].toString() + "|";
                  },
                  child:

                  Text(
                    //"$i. Barang ke-$i dipotong jadi: ${_potongJadi[i-1]} cm",
                    "$i. " + AppText.PRODUCT_CUT_ITEM[Globals.langSelected]+"$i"+AppText.PRODUCT_CUT_TO[Globals.langSelected]+ "${_potongJadi[i-1]} cm",
                    style: TextStyle(
                      color: Colors.blue[400], fontSize: 12.0,
                    ),
                  )
              ),

              SizedBox(height: 12),

            ],
          )

          //)
          : Container();
        }
    );
  }

  _buildCutItemWidget() {
    return ScopedModelDescendant<ProductsScopedModel>(
        builder: (context,child,model) {
          int cutQty = model.getCutQuantity(product);
          List<Widget> item = List();
          //_potongJadi.clear();
          String colorSize = product.selectedColor + "," + product.selectedSize;
          int height = product.matching[colorSize]['height'];
          int cutRange = product.matching[colorSize]['cutRange'];
          _potongJadi.add(height);


          for (int i=0; i < cutQty; i++) {
            item.add(_buildCutItemDialog(i+1, height, cutRange)
            );
          }

          return Padding(
          padding: const EdgeInsets.only(left: 0.0),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: item
            ),
          );
        }
    );
  }

  _buildDetailsAndMaterialWidgets() {
    TabController tabController = new TabController(length: 2, vsync: this);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TabBar(
            controller: tabController,
            tabs: <Widget>[
              Tab(
                child: Text(
                  "DETAILS",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
              Tab(
                child: Text(
                  "MATERIAL & CARE",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 6.0),
            height: 60.0,
            child: TabBarView(
              controller: tabController,
              children: <Widget>[
                Text(
                  "76% acrylic, 19% polyster, 5% metallic yarn Hand-wash cold",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                Text(
                  "86% acrylic, 9% polyster, 1% metallic yarn Hand-wash cold",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  _buildMoreInfoHeader() {
    return Padding(
      padding: const EdgeInsets.only(
        left: 12.0,
      ),
      child: Text(
        "MORE INFO",
        style: TextStyle(
          color: Colors.grey[800],
        ),
      ),
    );
  }

  _buildMoreInfoData() {
    return Padding(
      padding: const EdgeInsets.only(
        left: 12.0,
      ),
      child: Text(
        "Product Code: ${product
            .productId}\nTax info: Applicable GST will be charged at the time of chekout",
        style: TextStyle(
          color: Colors.grey[600],
        ),
      ),
    );
  }

  Future<bool> _checkIfAlreadyLogin() async {
    final prefs = await SharedPreferences.getInstance();
    bool _isGoogleLoggedIn = prefs.getBool(Constants.IS_GOOGLE_LOGIN) ?? false;
    bool _isFacebookLoggedIn = prefs.getBool(Constants.IS_FACEBOOK_LOGIN) ?? false;
    return _isGoogleLoggedIn || _isFacebookLoggedIn;
  }

  _buildBottomNavigationBar() {
    ProductsScopedModel productsScopedModel = new ProductsScopedModel();

    return ScopedModelDescendant<ProductsScopedModel>(
        builder: (context,child,model) {
          return Container(
            width: MediaQuery.of(context).size.width,
            height: 60.0,
/*
            decoration: BoxDecoration(
              border: new Border(top: BorderSide(color: Colors.black54, width: 1.5)),
              //color: Colors.grey[400]
            ),
*/

            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
/*
                Flexible(
                  fit: FlexFit.tight,
                  flex: 2,
                  child: RaisedButton(
                    onPressed: () {

                      setState(() {
                        buttonFavePressed = true;
                      });
                      if (product.isFavorite == 0) {
                        model.setFavorite(product, true);
                      } else {
                        model.setFavorite(product, false);
                      }

                    },
                    color: Colors.greenAccent,
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[

                          (model.getFavorite(product))
                              ? Icon(
                            Icons.favorite,
                            color: Colors.white,
                          )
                              : Icon(
                            Icons.favorite_border,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 4.0,
                          ),
                          Text(
                            "FAVORIT",
                            style: TextStyle(color: Colors.white, fontSize: 11.0),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
*/

                FutureBuilder<bool>(
                    future: _checkIfAlreadyLogin(),
                    builder: (context, snapshot) {
                      return (snapshot.hasData && snapshot.data)
                      // add cart enabled
                      ? Flexible(
                        flex: 2,
                        child: RaisedButton(
                          onPressed: () {
                            if (!processSendingCart) {
                              if (product.cutNote == null) product.cutNote = "";
                              // append cut note
                              String item = "";
                              for (int i=1; i < product.cutQuantity+1; i++) {
                                (i == product.cutQuantity)
                                    ? item = _potongJadi[i-1].toString()
                                    : item = _potongJadi[i-1].toString()+"|";
                                _cutNotes = _cutNotes + item;
                              }

                              productsScopedModel.setCutNote(product, _cutNotes);

                              // debug before send
/*
                        print("add cart->product id: " + product.productId.toString());
                        print("add cart->sku id: " + product.skuId.toString());
                        print("add cart->item qty: " + product.itemQuantity.toString());
                        print("add cart->cut qty: " + product.cutQuantity.toString());
                        print("add cart->cut notes: " + product.cutNote);
*/
                              // send to api
                              setState(() {
                                processSendingCart = true;
                              });

                              _scaffoldKey.currentState.showSnackBar(
                                  new SnackBar(
                                    duration: new Duration(seconds: 2),
                                    content: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        new CircularProgressIndicator(),
                                        //new Text("  Mohon tunggu...")
                                      ],
                                    ),
                                  )
                              );

                              cartModel.addCart(
                                  product.productId,
                                  product.skuId,
                                  product.itemQuantity,
                                  product.cutQuantity,
                                  product.cutNote
                              ).then((response) {

                                _scaffoldKey.currentState.hideCurrentSnackBar();
                                product.stockQuantity = response['qty_stock'];
                                _scaffoldKey.currentState.showSnackBar(
                                    new SnackBar(
                                      duration: new Duration(minutes: 10),
                                      content: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Expanded(child: Text(response['message'])),
                                          RaisedButton(
                                            onPressed: () {
                                              setState(() {
                                                processSendingCart = false;
                                              });

                                              _scaffoldKey.currentState.hideCurrentSnackBar();

                                            },
                                            color: Colors.redAccent,
                                            child: Text("OK"),
                                          )
                                        ],
                                      ),
                                    )
                                );
                              });

                            }
                          },
                          color: (!processSendingCart) ? Colors.white : Colors.grey[500],
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.shopping_cart,
                                  color: Colors.black,
                                ),
                                SizedBox(
                                  width: 4.0,
                                ),
                                Text(
                                  AppText.ADD_TO_CART[Globals.langSelected],
                                  style: TextStyle(color: Colors.black),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                          :
                      // add cart disabled redirect to login
                      Flexible(
                        flex: 2,
                        child: RaisedButton(
                          onPressed: () {
                            //cartModel.addCart(product.productId, 1);
                            _scaffoldKey.currentState.showSnackBar(
                                new SnackBar(
                                  duration: new Duration(seconds: 2),
                                  content:
                                  new Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      //new CircularProgressIndicator(),
                                      new Text(AppText.PRODUCT_LOGIN_WARNING[Globals.langSelected])
                                    ],
                                  ),
                                ));
                          },
                          color: Colors.white,
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.shopping_cart,
                                  color: Colors.black,
                                ),
                                SizedBox(
                                  width: 4.0,
                                ),
                                Text(
                                  AppText.ADD_TO_CART[Globals.langSelected],
                                  style: TextStyle(color: Colors.black),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    }
                ),
              ],
            ),
          );
        }
    );
  }
}
