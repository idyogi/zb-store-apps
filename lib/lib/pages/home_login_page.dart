import 'package:flutter/material.dart';
import 'package:zb_store/util/globals.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'dart:async';
import 'dart:convert' show json;
import "package:http/http.dart" as http;
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zb_store/util/languages.dart';
//import 'package:toast/toast.dart';
//import 'package:products_tutorial/util/remote_config.dart';
//import 'product_list_page.dart';


class LoginPage extends StatefulWidget {
  @override
  State createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {

  // ---------------------------------
  //            Facebook
  // ---------------------------------

  Future<dynamic> _sendFacebookData(String id, name, email) async {

    var response = await http.get(
        Constants.API_FACEBOOK_LOGIN
        + "?id=$id&email=$email&name=$name"
    ).catchError(
        (error) {
        return false;
      },
    );
    return json.decode(response.body);
  }

  void initiateFacebookLogin() async {
    FacebookLogin _facebookLogin;
    FacebookLoginResult _facebookLoginResult;
    dynamic _facebookUser;
    bool _isFacebookLoggedIn = false;


    print("fb->mau login....");
    _facebookLogin = FacebookLogin();
    _facebookLoginResult = await _facebookLogin.logInWithReadPermissions(['email']);

    var graphResponse = await http.get(
        'https://graph.facebook.com/v2.12/me?'
            'fields=name,first_name,last_name,email,picture.height(200)&access_token=${_facebookLoginResult
            .accessToken.token}');

    _facebookUser = json.decode(graphResponse.body);
    print("fb->"+_facebookUser.toString());

    var facebookPic = _facebookUser['picture']['data']['url'];
    if (_facebookLoginResult.status == FacebookLoginStatus.loggedIn) {
      //onFacebookLoginStatusChanged(true);
      // debug
      print("initiateFacebookLogin");
      print("fb id: " + _facebookUser['id']);
      print("fb name: " + _facebookUser['name']);
      print("fb email: " + _facebookUser['email']);
      print("fb pic: " + facebookPic);
      // save shared prefs
      final prefs = await SharedPreferences.getInstance();
      setState(() {
        _isFacebookLoggedIn = true;
        prefs.setBool(Constants.IS_FACEBOOK_LOGIN, _isFacebookLoggedIn);
        prefs.setString(Constants.SAVED_FACEBOOK_ID, _facebookUser['id']);
        prefs.setString(Constants.SAVED_FACEBOOK_NAME, _facebookUser['name']);
        prefs.setString(Constants.SAVED_FACEBOOK_EMAIL, _facebookUser['email']);
        prefs.setString(Constants.FACEBOOK_PIC_URL, facebookPic);
      });
      // send api
      await _sendFacebookData(
          _facebookUser['id'],
          _facebookUser['name'],
          _facebookUser['email']).then((jsonResult){
            print("FB response:" + jsonResult.toString());
            print("Saved Facebook token: " + jsonResult['token']);
            prefs.setString(Constants.SAVED_FACEBOOK_TOKEN, jsonResult['token']);
            prefs.setString(Constants.SAVED_USER_MEMBERSHIP, jsonResult['member']);
      });
      // logout
      _facebookLogin.logOut();
      // close page
      Navigator.of(context).pop();
    }
  }

  // ---------------------------------
  //            Google
  // ---------------------------------

  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );

  Future<dynamic> _sendGoogleData(String id, name, email) async {

    var response = await http.get(
        Constants.API_GOOGLE_LOGIN
        + "?id=$id&email=$email&name=$name"
    ).catchError(
        (error) {
        return false;
      },
    );
    return json.decode(response.body);
  }

  Future<void> initiateGoogleLogin() async {
    bool _isGoogleLoggedIn = false;
    GoogleSignInAccount _googleUser;

    try {
      _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {_googleUser = account;});
      _googleSignIn.signInSilently();

      await _googleSignIn.signIn();
/*
      var jsonPic = await http.get("https://www.googleapis.com/plus/v1/people/"
          "${_googleUser.id}?fields=image&key=${Constants.GOOGLE_API_KEY}");
      var googlePicUrl = json.decode(jsonPic.body);
*/
      //String _googleImage = googlePicUrl['image']['url'] != null ? googlePicUrl['image']['url'] : "";
      // debug
      print("initiateGoogleLogin");
      print("google id: " + _googleUser.id);
      print("google name: " + _googleUser.displayName);
      print("google email: " + _googleUser.email);
      //print("google pic: " + googlePicUrl.toString());
      // save shared prefs
      final prefs = await SharedPreferences.getInstance();
      setState(() {
        _isGoogleLoggedIn = true;
        prefs.setBool(Constants.IS_GOOGLE_LOGIN, _isGoogleLoggedIn);
        prefs.setString(Constants.SAVED_GOOGLE_ID, _googleUser.id);
        prefs.setString(Constants.SAVED_GOOGLE_NAME, _googleUser.displayName);
        prefs.setString(Constants.SAVED_GOOGLE_EMAIL, _googleUser.email);
        //prefs.setString(Constants.GOOGLE_PIC_URL, googlePicUrl);
        prefs.setString(Constants.GOOGLE_PIC_URL, "");
      });
      // send api
      await _sendGoogleData(
          _googleUser.id,
          _googleUser.displayName,
          _googleUser.email).then((jsonResult) {
            print("Saved Google token: " + jsonResult['token']);
            prefs.setString(Constants.SAVED_GOOGLE_TOKEN, jsonResult['token']);
            prefs.setString(Constants.SAVED_USER_MEMBERSHIP, jsonResult['member']);
      });
      // logout
      _googleSignIn.disconnect();
      // close page
      Navigator.of(context).pop();
    } catch (error) {
      print(error);
    }
  }

  // ---------------------------------
  //            Main Build
  // ---------------------------------
  _removeSharedPrefsLogin() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setBool(Constants.IS_GOOGLE_LOGIN, false);
      prefs.setBool(Constants.IS_FACEBOOK_LOGIN, false);
    });

  }

  Widget displayLoginButton() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const Text("You are not currently signed in."),
        SizedBox(height: 30.0),

        RaisedButton(
          child: const Text('  GOOGLE LOGIN  '),
          onPressed: initiateGoogleLogin,
        ),

        RaisedButton(
            child: const Text('FACEBOOK LOGIN'),
            onPressed: () => initiateFacebookLogin()
        ),
      ],
    );
  }



  Widget loginPage() {

    var backgroundColor1 = Color(0xFF444152);
    var backgroundColor2 = Color(0xFF6f6c7d);
    var googleColor = Color(0xfff65aa3);
    var facebookColor = Colors.indigo[400];
    var foregroundColor = Colors.white;

    return Scaffold(
      appBar: AppBar(
        //backgroundColor: Colors.indigo[400],
        centerTitle: true,
        title: const Text('Sign In'),
      ),
      body:

      Container(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
            begin: Alignment.centerLeft,
            end: new Alignment(1.0, 0.0), // 10% of the width, so there are ten blinds.
            colors: [backgroundColor1, backgroundColor2], // whitish to gray
            tileMode: TileMode.repeated, // repeats the gradient over the canvas
          ),
        ),
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
              child: Center(
                child: new Column(
                  children: <Widget>[
                    SizedBox(height: 85.0),
                    Container(
                      height: 135.0,
                      width: 135.0,
                      child: new CircleAvatar(
                        backgroundColor: Colors.transparent,
                        //foregroundColor: foregroundColor,
                        radius: 120.0,
                        child: Image.asset('assets/images/logo_zb_10.png'),
/*
                        child: new Text(
                          "ZB",
                          style: TextStyle(
                            fontSize: 50.0,
                            fontWeight: FontWeight.w100,
                          ),
                        ),
*/
                      ),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: foregroundColor,
                          width: 1.0,
                        ),
                        shape: BoxShape.circle,
                        //image: DecorationImage(image: this.logo)
                      ),
                    ),
/*
                    new Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: new Text(
                        "Shar'iy never goes out of style",
                        style: TextStyle(color: foregroundColor),
                      ),
                    )
*/
                  ],
                ),
              ),
            ),

            new Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[

                // Google login
                new Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.only(left: 40.0, right: 40.0, top: 50.0),
                  alignment: Alignment.center,
                  child: new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new FlatButton(
                          padding: const EdgeInsets.symmetric(
                              vertical: 15.0, horizontal: 15.0),
                          color: googleColor,
                          onPressed: () => initiateGoogleLogin(),
                          child: new Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                const IconData(0xea88,
                                    fontFamily: 'icomoon'),
                                color: Colors.white,
                                size: 15.0,
                              ),
                              Text(
                                "Google",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              Container(),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                // Facebook Login
                new Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.only(left: 40.0, right: 40.0, top: 20.0),
                  alignment: Alignment.center,
                  child: new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new FlatButton(
                          padding: const EdgeInsets.symmetric(
                              vertical: 15.0, horizontal: 15.0),
                          color: facebookColor,
                          onPressed: () => initiateFacebookLogin(),
                          child: new Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                const IconData(0xea90,
                                    fontFamily: 'icomoon'),
                                color: Colors.white,
                                size: 15.0,
                              ),
                              Text(
                                "Facebook",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              Container(),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

              ],
            ),

            new Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0),
              alignment: Alignment.center,
              child: new Row(
                children: <Widget>[
                  new Expanded(
                    child: new FlatButton(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20.0, horizontal: 20.0),
                      color: Colors.transparent,
                      onPressed: () => {},
                      child: Text(
                        AppText.SIGN_IN_MSG[Globals.langSelected],
                        style: TextStyle(color: foregroundColor.withOpacity(0.5)),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            //new Expanded(child: Divider()),

          ],
        ),
      ),

/*
      Container(
          child: LoginScreen(
            backgroundColor1: Color(0xFF444152),
            backgroundColor2: Color(0xFF6f6c7d),
            highlightColor: Color(0xfff65aa3),
            foregroundColor: Colors.white,
            //logo: new AssetImage("assets/images/full-bloom.png"),
          ),
*/
/*
      ConstrainedBox(
        constraints: const BoxConstraints.expand(),
        child: displayLoginButton(),
*/
      );
  }


  @override
  Widget build(BuildContext context) {
    return //FutureBuilder<bool>(
        //future: getSavedLoginStatus(),
        //builder: (context, snapshot) {
        //  return //---(snapshot.hasData && snapshot.data == true) ?
          // alreadyLogin = true
          //-- _showAlreadyLogin() :
          // alreadyLogin = false
          loginPage();
        }
    //);
  //}
}


