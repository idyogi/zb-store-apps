import 'package:flutter/material.dart';
import 'package:zb_store/model/zone.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/scoped_model/address_scoped_model.dart';
import 'address_subdistrict_page.dart';

class AddressCityPage extends StatelessWidget {
  final String provinceID;
  AddressCityPage({this.provinceID});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      //key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0.5,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: new Text(
            "Kota/Kabupaten", style: TextStyle(color: Colors.black87)),
        iconTheme: new IconThemeData(color: Colors.black54),
      ),
      body: _buildAddressCityPageBody(),
    );
  }

  _buildAddressCityPageBody() {
    final AddressScopedModel addressModel = AddressScopedModel();
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
      child: FutureBuilder<List<Zone>>(
        future: addressModel.parseCity(Constants.API_GET_CITY, "?id=${Globals.provinceID}"),
        //future: addressModel.parseCity(Constants.API_GET_CITY, "?id=5"),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
            ? _buildCityList(snapshot.data)
            : Center(child: CircularProgressIndicator());
        },
      )
    );
  }

  _buildCityList(List<Zone> city) {
    return new ListView.builder(
      itemCount: city.length,
      itemBuilder: (context, index) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            InkWell(
              onTap: () {
                Navigator.of(context).pushReplacementNamed(Constants.ROUTE_ADD_SUBDISTRICT);
                Globals.cityID = city[index].id;
                Globals.cityName = city[index].name;
                Globals.postalCode = city[index].postalCode;
                /*
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return AddressSubdistrictPage(subdistrictID: province[index].id);
                    },
                  )
                );
*/
              },
              child: ListTile(
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(city[index].name),
                    Icon(Icons.arrow_forward_ios, color: Colors.grey[400]),
                  ],
                )
              ),
            ),
            Divider(),
          ],
        );
      }
    );
  }

}


