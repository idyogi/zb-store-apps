import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:zb_store/scoped_model/product_scoped_model.dart';
import 'package:zb_store/util/languages.dart';
import 'package:zb_store/widgets/products_list_item.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/util/network.dart';

class Global {
  static String search = "";
}

class ProductSearchPage extends StatelessWidget {
  final String search;
  const ProductSearchPage({Key key, @required this.search})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Global.search= search;
    Network network = new Network();
    ProductsScopedModel productModel = ProductsScopedModel();
    productModel.parseProductsFromResponse(Constants.API_SEARCH+"?q=$search", 1);


    //print("total cart = " + globals.totalCartItem.toString());
    return new ScopedModel<ProductsScopedModel>(
      model: productModel,
      child: Scaffold(
        //key: _scaffoldKey,
        appBar: AppBar(
          elevation: 1,
          centerTitle: true,
          backgroundColor: Colors.white,
          title: new Text("Search", style: TextStyle(color: Colors.black)),
          //title: new Image.asset(brandName, width: 120.0, height: 40.0,),
          iconTheme: new IconThemeData(color: Colors.black),
        ),
        body: ProductsListPageBody(),
        //drawer: HomeDrawer(),

      ),
    );
  }
}

class ProductsListPageBody extends StatefulWidget {
  //final String url;
  //const ProductsListPageBody({Key key, @required this.url}) : super(key: key);
  @override
  ProductsListPageBodyState createState() {
    return new ProductsListPageBodyState();
  }
}

class ProductsListPageBodyState extends State<ProductsListPageBody> {
  int pageIndex = 1;
  int currentPage = 0;
  PageController controller;

  @override
  Widget build(BuildContext context) {

    return ScopedModelDescendant<ProductsScopedModel>(
      builder: (context, child, model) {
        return model.isLoading
            ? _buildSplashScreen()
            : _buildListView(context, model);
      },
    );
  }

  _buildSplashScreen() {

    return Stack(
      children: <Widget>[
        Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
        ),

        Center(
          child: GlowingProgressIndicator(
              duration: Duration(milliseconds: 400),
              child: Container(
                height: 70.0,
                width: 70.0,
                child: (Globals.brandID == 1)
                    ? Image.asset('assets/images/logo_zb_10.png', color: Colors.black54)
                    : (Globals.brandID == 2)
                    ? Image.asset('assets/images/logo_zb_20.png', color: Colors.black54)
                    : Image.asset('assets/images/logo_zb_30.png', color: Colors.black54),
              )
          ),
        ),

/*
          Container(
            height: double.infinity,
            width: double.infinity,
          ),
*/
      ],
    );
  }

  _buildCircularProgressIndicator() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  _buildListView(BuildContext context, ProductsScopedModel model) {
    Size screenSize = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
      child: model.getProductsCount() == 0
          ? Center(child: Text("No products found."))
          : ListView.builder(
        itemCount: model.getProductsCount() + 2,
        itemBuilder: (context, index) {
          if (index == model.getProductsCount() && !model.produkHabis) {
            if (model.hasMoreProducts && model.getProductsCount() != 0) {
              pageIndex++;
              model.parseProductsFromResponse(Constants.API_SEARCH+"?q=${Global.search}", pageIndex);
              return Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Container(),
                //child: Center(child: CircularProgressIndicator()),
              );
            }
            return Container();
          } else if (index == 0) {
            //0th index would contain filter icons
            return _buildFilterWidgets(screenSize); //"http://beta.devsandbox.me/api/brand/1"
          } else if (index % 2 == 0) {
            //2nd, 4th, 6th.. index would contain nothing since this would
            //be handled by the odd indexes where the row contains 2 items
            return Container();
          } else {
            // jika jumlah data ganjil tampilkan single produk
            if (index % 2 == 1 && model.getProductsCount() == index) {
              return ProductsListItem(
                product1: model.productsList[index - 1],
                product2: null,
              );
            }
            //1st, 3rd, 5th.. index would contain a row containing 2 products
            if (index > model.getProductsCount() - 1) {
              //print("ProductSearchPage masuk sini...... ");
              return Container();
            }
            //print("ProductSearchPage product1: " + model.productsList[index - 1].toString());
            //print("ProductSearchPage product2: " + model.productsList[index].toString());
            return ProductsListItem(
              product1: model.productsList[index - 1],
              product2: model.productsList[index],
            );
          }
        },
      ),
    );
  }

  _buildFilterWidgets(Size screenSize) {
    return Container(
      margin: const EdgeInsets.only(left: 1.0, right: 1.0),
      width: screenSize.width,
      child: Card(
        elevation: 1.0,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          child: Center(
            child: Text(AppText.SEARCH_RESULT[Globals.langSelected] + Global.search),
          ),
        ),
      ),
    );
  }

  _buildFilterButton(String title) {
    return InkWell(
      onTap: () {
        print(title);
      },
      child: Row(
        children: <Widget>[
          Icon(
            Icons.arrow_drop_down,
            color: Colors.black,
          ),
          SizedBox(
            width: 2.0,
          ),
          Text(title),
        ],
      ),
    );
  }
}

