import 'package:flutter/material.dart';
import 'package:zb_store/model/address.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:zb_store/scoped_model/address_scoped_model.dart';
import 'package:zb_store/util/languages.dart';
import 'address_add_page.dart';
import 'package:zb_store/util/globals.dart';
import 'address_edit_page.dart';
import 'package:zb_store/scoped_model/product_scoped_model.dart';

class HomeLangPage extends StatefulWidget {
  @override
  HomeLangPageState createState() {
    return new HomeLangPageState();
  }
}

class HomeLangPageState extends State<HomeLangPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  // lang
  // 0 = en
  // 1 = id
  //default id
  int langSelected = 1;
  bool _processSending = false;
  ProductsScopedModel scopedModel = new ProductsScopedModel();

  @override
  void initState() {
    super.initState();
    if (Globals.langSelected != null)
      langSelected = Globals.langSelected;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return /*new ScopedModel<AddressScopedModel>(
      model: addressModel,
      child:*/ Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: 1,
          centerTitle: true,
          backgroundColor: Colors.white,
          title: new Text(
              AppText.HOME_LANG[Globals.langSelected], style: TextStyle(color: Colors.black87)),
          iconTheme: new IconThemeData(color: Colors.black54),
        ),
        body: _langListPageBody(),
        bottomNavigationBar: _buildBottomNavigationBar(context),
      );
    //);
  }

  _langListPageBody() {
    List<String> languages = List();
    languages.add("English");
    languages.add("Bahasa Indonesia");
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
        child: _langList(languages),
    );
  }

  _langList(List<String> languages) {
    return new ListView.builder(
        itemCount: languages.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              if (!_processSending) {
                setState(() {
                  langSelected = index;
                });
              }
            },
            child: Card(
              child: ListTile(
                //leading: IconButton(icon: Icon(Icons.check_circle, color: Colors.grey)),
                title: Text(languages[index],
                    style: TextStyle(
                        color: Colors.grey, fontSize: 18)),
                trailing: (langSelected == index)
                  ? IconButton(
                      icon: Icon(Icons.check_circle, color: Colors.red),
                    )
                  : null,
              ),
            ),
          );

        }
          );
  }

  _buildBottomNavigationBar(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          // add address
          Flexible(
            flex: 1,
            child: RaisedButton(
              elevation: 2.0,
              onPressed: () {

                if (!_processSending) {

                  _scaffoldKey.currentState.showSnackBar(
                      new SnackBar(
                        duration: new Duration(minutes: 2),
                        content: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new CircularProgressIndicator(),
                            new Text("  Updating...")
                          ],
                        ),
                      )
                  );

                  setState(() {
                    _processSending = true;
                  });

                  Globals.langSelected = langSelected;

                  scopedModel.changeLang(
                    langSelected == 0 ? "en" : "id",
                  ).then((response) {

                    //print("add voucher response: " + response.toString());
                    _scaffoldKey.currentState.hideCurrentSnackBar();
                    _scaffoldKey.currentState.showSnackBar(
                        new SnackBar(
                          duration: new Duration(minutes: 10),
                          content: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(child: Text(response['message'])),
                              RaisedButton(
                                onPressed: () {
                                  _scaffoldKey.currentState.hideCurrentSnackBar();

                                  setState(() {
                                    _processSending = false;
                                  });

                                  Navigator.of(context).pop();
                                },
                                color: Colors.red,
                                child: Text("OK"),
                              )
                            ],
                          ),
                        )
                    );

                  });

                }
                // change checkout address


/*
              Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return AddressAddPage();
                    },
                  )
              );
*/
                //Route route = MaterialPageRoute(builder: (context) => AddressAddPage());
                //Navigator.pushNamed(context, Constants.ROUTE_ADD_ADDRESS);
              },
              color: Colors.white,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.check_circle,
                      color: Colors.black,
                    ),
                    SizedBox(
                      width: 4.0,
                    ),
                    Text(
                      AppText.HOME_LANG_CHANGE[Globals.langSelected],
                      style: TextStyle(color: Colors.black),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

}


