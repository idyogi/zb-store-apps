import 'package:flutter/material.dart';
import 'package:zb_store/model/shipment.dart';
import 'package:zb_store/scoped_model/address_scoped_model.dart';

class PaymentShipmentPage extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final AddressScopedModel addressModel = AddressScopedModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0.5,
        leading: new IconButton(
          icon: new Icon(Icons.close, color: Colors.black54),
          onPressed: ()=>Navigator.of(context).pop(),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: new Text(
            "Pilih Jenis Pengiriman", style: TextStyle(color: Colors.black87)),
        iconTheme: new IconThemeData(color: Colors.black54),
      ),
      body: _paymentShipmentPageBody(),
      //bottomNavigationBar: _buildBottomNavigationBar(context),
    );
  }

  _paymentShipmentPageBody() {

    return Padding(
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
        child: FutureBuilder<List<Shipment>>(
          future: addressModel.getShipmentService(),
          builder: (context, snapshot) {
            if (snapshot.hasError) print(snapshot.error);

            return snapshot.hasData
                ? _buildShipmentList(snapshot.data)
                : Center(child: CircularProgressIndicator());
          },
        )
    );
  }

  _buildShipmentList(List<Shipment> shipmentService) {
    return new ListView.builder(
        itemCount: shipmentService.length,
        itemBuilder: (context, index) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              InkWell(
                onTap: () {

                  _scaffoldKey.currentState.showSnackBar(
                      new SnackBar(
                        duration: new Duration(seconds: 2),
                        content: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new CircularProgressIndicator(),
                            new Text("  Mohon tunggu...")
                          ],
                        ),
                      )
                  );

                  addressModel.updateShipmentService(shipmentService[index].service).
                    then((response) {

                      Navigator.of(context).pop();

                  });


                },
                child: ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(shipmentService[index].name + " - " + shipmentService[index].service),
                      //Icon(Icons.arrow_forward_ios, color: Colors.grey[400]),
                    ],
                  ),
                  subtitle: Text("Perkiraan sampai: "+shipmentService[index].etd + " hari\nHarga: Rp." + shipmentService[index].price),
                ),
              ),
              Divider(),
            ],
          );
        }
    );
  }
}
