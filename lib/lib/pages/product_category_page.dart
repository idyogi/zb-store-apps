import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:zb_store/scoped_model/product_scoped_model.dart';
import 'package:zb_store/widgets/products_list_item.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/util/network.dart';
import 'package:zb_store/widgets/home_drawer.dart';
import 'package:http/http.dart' as http;
import 'package:carousel_slider/carousel_slider.dart';
import 'product_single_page.dart';

class Global {
  static String urlCategory = "";
  static String categoryName = "";
}

class ProductsCategoryPage extends StatelessWidget {
  //final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final int id;
  final String urlCategory;
  final String name;
  const ProductsCategoryPage({Key key, @required this.id, @required this.urlCategory, @required this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Global.urlCategory = urlCategory;
    Global.categoryName= name;
    Network network = new Network();
    ProductsScopedModel productModel = ProductsScopedModel();
    productModel.parseProductsFromResponse(Global.urlCategory, 1);

    String brandName;
    if (id == 1) {
      brandName = 'assets/images/logo_zb_1.png';
    } else if (id == 2) {
      brandName = 'assets/images/logo_zb_2.png';
    } else if (id == 3) {
      brandName = 'assets/images/logo_zb_3.png';
    }

    //print("total cart = " + globals.totalCartItem.toString());
    return new ScopedModel<ProductsScopedModel>(
      model: productModel,
      child: Scaffold(
        //key: _scaffoldKey,
        appBar: AppBar(
          centerTitle: true,
          elevation: 1,
          backgroundColor: Colors.white,
          //title: new Text(title, style: TextStyle(color: Colors.black)),
          title: new Image.asset(brandName, width: 120.0, height: 40.0,),
          actions: <Widget>[
/*
            IconButton(
              icon: Icon(Icons.shopping_cart, color: Colors.black),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return CartListPage();
                    },
                  ),
                );
              },
            ),
*/
            // Using Stack to show Notification Badge
            InkWell(
              onTap: () {
/*
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return CartListPage();
                    },
                  ),
                );
*/
                Navigator.pushNamed(context, Constants.ROUTE_CART_LIST);

              },
              child: Stack(
                children: <Widget>[
                  new IconButton(
                    icon: Icon(Icons.shopping_cart, color: Colors.black, size: 32.0),
                  ),

                  FutureBuilder<int>(
                      future: network.getNotificationBadge(),
                      builder: (context, snapshot) {
                        return (snapshot.hasData && snapshot.data != 0)
                            ? new Positioned(
                          right: 6,
                          top: 5,
                          child: new Container(
                            padding: EdgeInsets.all(2),
                            decoration: new BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(6),
                            ),
                            constraints: BoxConstraints(
                              minWidth: 14,
                              minHeight: 14,
                            ),
                            child: Text(
                              snapshot.data.toString(),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 8,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        )
                            : new Container();
                      }
                  ),
                ],
              ),
            ),

          ],
          iconTheme: new IconThemeData(color: Colors.black),
        ),
        body: ProductsListPageBody(),
        //drawer: HomeDrawer(),

      ),
    );
  }
}

/*
class BuildDrawer extends StatelessWidget {

  const BuildDrawer({
    Key key,
  }) : super(key: key);

  Future<Map> getSavedLoginData() async{
    final prefs = await SharedPreferences.getInstance();
    Map savedUser = new Map();
    bool _isGoogleLoggedIn = prefs.getBool(Constants.IS_GOOGLE_LOGIN) ?? false;
    bool _isFacebookLoggedIn = prefs.getBool(Constants.IS_FACEBOOK_LOGIN) ?? false;

    if (_isGoogleLoggedIn) {
      savedUser['provider'] = "Google";
      savedUser['id'] = prefs.getString(Constants.SAVED_GOOGLE_ID);
      savedUser['name'] = prefs.getString(Constants.SAVED_GOOGLE_NAME);
      savedUser['email'] = prefs.getString(Constants.SAVED_GOOGLE_EMAIL);
      savedUser['pic'] = prefs.getString(Constants.GOOGLE_PIC_URL);
      savedUser['token'] = prefs.getString(Constants.SAVED_GOOGLE_TOKEN);
    } else if (_isFacebookLoggedIn) {
      savedUser['provider'] = "Facebook";
      savedUser['id'] = prefs.getString(Constants.SAVED_FACEBOOK_ID);
      savedUser['name'] = prefs.getString(Constants.SAVED_FACEBOOK_NAME);
      savedUser['email'] = prefs.getString(Constants.SAVED_FACEBOOK_EMAIL);
      savedUser['pic'] = prefs.getString(Constants.FACEBOOK_PIC_URL);
      savedUser['token'] = prefs.getString(Constants.SAVED_FACEBOOK_TOKEN);
    }
    return savedUser;
  }

  void removeSavedPrefs(String provider) async {
    final prefs = await SharedPreferences.getInstance();

    if (provider == "Google") {
      prefs.setBool(Constants.IS_GOOGLE_LOGIN, false);
    } else {
      prefs.setBool(Constants.IS_FACEBOOK_LOGIN, false);
    }
  }

  @override
  Widget build(BuildContext context) {
    String loginProvider = "";
    return Drawer(
      child: ListView(
        children: <Widget>[
          // header
          FutureBuilder<Map>(
              future: getSavedLoginData(),
              builder: (context, snapshot) {
                if (snapshot.hasData) loginProvider = snapshot.data['provider'];
                return (snapshot.hasData) ?
                UserAccountsDrawerHeader(
                  accountName: Text(snapshot.data['name']*/
/*+" (${snapshot.data['provider']})"*//*
, style: TextStyle(color: Colors.black)),
                  accountEmail: Text(snapshot.data['email'], style: TextStyle(color: Colors.black)),
                  currentAccountPicture:
                  CircleAvatar (
                    backgroundImage: NetworkImage(snapshot.data['pic']),
                  ),
                  decoration: BoxDecoration(color: Colors.white),
                ) :
                new Container();
              }
          ),

          // menu
          ListTile(
            leading: Icon(Icons.person),
            title: Text("Profile"),
            trailing: Icon(Icons.arrow_forward),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
          ListTile(
            leading: Icon(Icons.map),
            title: Text("Map"),
            trailing: Icon(Icons.arrow_forward),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text("Preferences"),
            trailing: Icon(Icons.arrow_forward),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text("Logout"),
            trailing: Icon(Icons.arrow_forward),
            onTap: () {
              //print('login provider: $loginProvider');
              removeSavedPrefs(loginProvider);
              Navigator.pushReplacementNamed(context, Constants.ROUTE_LOGIN_PAGE);
            },
          ),
        ],
      ),
    );
  }
}
*/

class ProductsListPageBody extends StatefulWidget {
  //final String url;
  //const ProductsListPageBody({Key key, @required this.url}) : super(key: key);
  @override
  ProductsListPageBodyState createState() {
    return new ProductsListPageBodyState();
  }
}

class ProductsListPageBodyState extends State<ProductsListPageBody> {
  int pageIndex = 1;
  int currentPage = 0;
  PageController controller;

  @override
  Widget build(BuildContext context) {

    return ScopedModelDescendant<ProductsScopedModel>(
      builder: (context, child, model) {
        return model.isLoading
            ? _buildSplashScreen()
            : _buildListView(context, model);
      },
    );
  }
  _buildSplashScreen() {

    return Stack(
      children: <Widget>[
        Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
        ),

        Center(
          child: GlowingProgressIndicator(
              duration: Duration(milliseconds: 400),
              child: Container(
                height: 70.0,
                width: 70.0,
                child: (Globals.brandID == 1)
                    ? Image.asset('assets/images/logo_zb_10.png', color: Colors.black54)
                    : (Globals.brandID == 2)
                    ? Image.asset('assets/images/logo_zb_20.png', color: Colors.black54)
                    : Image.asset('assets/images/logo_zb_30.png', color: Colors.black54),
              )
          ),
        ),

/*
          Container(
            height: double.infinity,
            width: double.infinity,
          ),
*/
      ],
    );
  }


  _buildCircularProgressIndicator() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  _buildListView(BuildContext context, ProductsScopedModel model) {
    Size screenSize = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
      child: model.getProductsCount() == 0
          ? Center(child: Text("No products available."))
          : ListView.builder(
        itemCount: model.getProductsCount() + 2,
        itemBuilder: (context, index) {
          if (index == model.getProductsCount() && !model.produkHabis) {
            if (model.hasMoreProducts) {
              pageIndex++;
              model.parseProductsFromResponse(Global.urlCategory, pageIndex);
              return Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Container(),
                //child: Center(child: CircularProgressIndicator()),
              );
            }
            return Container();
          } else if (index == 0) {
            //0th index would contain filter icons
            return _buildFilterWidgets(screenSize); //"http://beta.devsandbox.me/api/brand/1"
          } else if (index % 2 == 0) {
            //2nd, 4th, 6th.. index would contain nothing since this would
            //be handled by the odd indexes where the row contains 2 items
            return Container();
          } else {
            // jika jumlah data ganjil tampilkan single produk
            if (index % 2 == 1 && model.getProductsCount() == index) {
              return ProductsListItem(
                product1: model.productsList[index - 1],
                product2: null,
              );
            }

            //1st, 3rd, 5th.. index would contain a row containing 2 products
            if (index > model.getProductsCount() - 1) {
              return Container();
            }

            return ProductsListItem(
              product1: model.productsList[index - 1],
              product2: model.productsList[index],
            );
          }
        },
      ),
    );
  }

  _buildFilterWidgets(Size screenSize) {
    return Container(
      margin: const EdgeInsets.only(left: 1.0, right: 1.0),
      width: screenSize.width,
      child: Card(
        elevation: 1.0,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          child: Center(
            child: Text("Category: " + Global.categoryName),
          ),
        ),
      ),
    );
  }

  _buildFilterButton(String title) {
    return InkWell(
      onTap: () {
        print(title);
      },
      child: Row(
        children: <Widget>[
          Icon(
            Icons.arrow_drop_down,
            color: Colors.black,
          ),
          SizedBox(
            width: 2.0,
          ),
          Text(title),
        ],
      ),
    );
  }
}

