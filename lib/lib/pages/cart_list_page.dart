import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:zb_store/scoped_model/cart_scoped_model.dart';
import 'package:zb_store/util/languages.dart';
import 'package:zb_store/widgets/cart_list_item.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:zb_store/util/globals.dart';
import 'payment_page.dart';

class CartListPage extends StatefulWidget {
  @override
  CartListPageState createState() {
    return new CartListPageState();
  }
}

class CartListPageState extends State<CartListPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    CartScopedModel cartModel = CartScopedModel();
    // get API category_id = null, page = 1
    cartModel.parseCartFromResponse(Constants.API_GET_CART);

    return new ScopedModel<CartScopedModel>(
      model: cartModel,
      child: new WillPopScope(
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            elevation: 0,
            backgroundColor: Colors.white,
            //title: new IconButton(icon: Icon(Icons.shopping_cart, color: Colors.black),),
            //title: new Text("KERANJANG BELANJA", style: TextStyle(color: Colors.black87)),
            title: Stack(
              children: <Widget>[

                new IconButton(
                  icon: Icon(Icons.shopping_cart, color: Colors.black),
                ),

                Positioned(
                  right: 0,
                  top: 0,
                  child: new Container(
                    padding: EdgeInsets.all(2),
/*
                    decoration: new BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(6),
                    ),
*/
                    constraints: BoxConstraints(
                      minWidth: 14,
                      minHeight: 14,
                    ),
                    child: ScopedModelDescendant<CartScopedModel>(
                        builder: (context, child, model) {
                          return Text(
                            cartModel.getTotalCartItem() != null ? cartModel.getTotalCartItem().toString() : "",
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          );
                        }
                    ),
                  ),
                )
              ],
            ),
            iconTheme: new IconThemeData(color: Colors.black54),
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 5.0),
                child: Center(
                  child: ScopedModelDescendant<CartScopedModel>(
                    builder: (context, child, model) {
                      return Text(cartModel.getTotalCartMoney() != null ? cartModel.getTotalCartMoney() : "",
                        style: TextStyle(
                          color: Colors.redAccent,
                          fontSize: 16,
                          fontWeight:
                          FontWeight.bold
                        )
                      );
                    }
                  ),
                ),
              ),
            ],
          ),
          body: CartListPageBody(),
          bottomNavigationBar: _buildBottomNavigationBar(context),
        ),
      ),
    );
  }

  _buildBottomNavigationBar(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 60.0,
      child: ScopedModelDescendant<CartScopedModel>(
        builder: (context, child, model) {
          return Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              // --------------------------------------------
              //           TOTAL ITEM AND PURCHASE
              // --------------------------------------------
/*
              Flexible(
                fit: FlexFit.tight,
                flex: 3,
                child: RaisedButton(
                  onPressed: () {},
                  color: Colors.black26,
                  child: Column(
                    //crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Total Tagihan:",
                        style: TextStyle(color: Colors.white, fontSize: 12.0),
                      ),
                      SizedBox(
                        width: 2.0,
                      ),
                      Text(
                        model.getTotalCartMoney() != null ? model.getTotalCartMoney() : "",
                        style: TextStyle(
                            color: Colors.white, fontSize: 19.0),
                      ),
                      SizedBox(
                        width: 2.0,
                      ),
                      Text(
                        model.getTotalCartItem() != null ? model.getTotalCartItem().toString() + " barang" : "",
                        style: TextStyle(color: Colors.white, fontSize: 12.0),
                      ),
                    ],
                  ),
                ),
              ),
*/
              // --------------------------------------------
              //                   CHECKOUT
              // --------------------------------------------
              Flexible(
                flex: 2,
                child: RaisedButton(
                  elevation: 2,
                  onPressed: () {
                    if (model.getTotalCartItem() == null || model.getTotalCartItem()==0) {

                    _scaffoldKey.currentState.showSnackBar(
                      new SnackBar(
                        duration: new Duration(seconds: 2),
                        content:
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text(AppText.CART_EMPTY[Globals.langSelected])
                          ],
                        ),
                      ));

                    } else {
                      // send api checkout get

/*
                      _scaffoldKey.currentState.showSnackBar(
                        new SnackBar(duration: new Duration(seconds: 4), content:
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new CircularProgressIndicator(),
                            new Text("  Mohon tunggu...")
                          ],
                        ),
                        ));
*/

                      //model.checkoutGet()
                      //  .whenComplete(() {

                      //    _scaffoldKey.currentState.hideCurrentSnackBar();

                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) {
                                return PaymentPage();
                              },
                            )
                          );
                      //Navigator.pushNamed(context, '/payment');

                      //   }
                     // );


                    }
                  },
                  color: Colors.white,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.check_circle,
                          color: Colors.black,
                        ),
                        SizedBox(
                          width: 2.0,
                        ),
                        Text(
                          "CHECKOUT",
                          style: TextStyle(color: Colors.black, fontSize: 14.0),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          );
        }
      )
    );
  }
}

class CartListPageBody extends StatelessWidget {
  BuildContext context;
  CartScopedModel model;

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return ScopedModelDescendant<CartScopedModel>(
      builder: (context, child, model) {
        this.model = model;
        return model.isLoading
            ? _buildSplashScreen()
            : _buildListView(); // nitip notif timeout
      },
    );
  }

  _buildSplashScreen() {

    return Stack(
      children: <Widget>[
        Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
        ),

        Center(
          child: GlowingProgressIndicator(
              duration: Duration(milliseconds: 400),
              child: Container(
                height: 70.0,
                width: 70.0,
                child: (Globals.brandID == 1)
                    ? Image.asset('assets/images/logo_zb_10.png', color: Colors.black54)
                    : (Globals.brandID == 2)
                    ? Image.asset('assets/images/logo_zb_20.png', color: Colors.black54)
                    : Image.asset('assets/images/logo_zb_30.png', color: Colors.black54),
              )
          ),
        ),

/*
          Container(
            height: double.infinity,
            width: double.infinity,
          ),
*/
      ],
    );
  }


  _buildCircularProgressIndicator() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }


  _buildListView() {

    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: (model.getCartCount() == 0)
          ? Center(child: Text(AppText.CART_EMPTY[Globals.langSelected]))
          : ListView.builder(
        itemCount: model.getCartCount(),
        itemBuilder: (context, index) {
          //if  (index == 0) {
            //0th index would contain filter icons
            //return _buildFilterWidgets(MediaQuery.of(context).size);
            //return SizedBox(width: 10.0, height: 10.0);
          //} else {
            //1st, 3rd, 5th.. index would contain a row containing 2 products
            //print("model.cartList[$index]: " + model.cartList[index].toString());
            return ProductsListItem(product: model.cartList[index]);
          //}
        },
      ),
    );
  }

}
