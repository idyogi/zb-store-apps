import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:zb_store/model/payment.dart';
import 'package:zb_store/pages/payment_terms_page.dart';
import 'package:zb_store/scoped_model/cart_scoped_model.dart';
import 'package:zb_store/util/globals.dart';
import 'address_list_page.dart';
import 'payment_va_page.dart';
import 'payment_redirect_page.dart';
import 'package:zb_store/model/steps_payment.dart';
import 'payment_voucher_page.dart';
import 'payment_shipment_page.dart';
import 'package:zb_store/util/languages.dart';

class PaymentPage extends StatefulWidget {
  //final int totalQuantity;
  //final String totalPrice;

/*
  PaymentPage({
    @required this.totalQuantity,
    @required this.totalPrice,
  });
*/

  @override
  PaymentPageState createState() {
    return new PaymentPageState();
  }
}

class PaymentPageState extends State<PaymentPage> {
  List<Payment> transferList = [];
  List<Payment> virtualAccountList = [];
  List<Payment> internetBankingList = [];
  List<Payment> counterList = [];
  String paymentID = "";
  String paymentName = "";
  String paymentImage = "";
  CartScopedModel cartModel = CartScopedModel();
  final _nama = TextEditingController();
  final _telpon = TextEditingController();
  bool addressIsOK = false;
  bool shipmentIsOK = false;
  bool paymentIsOK = false;
  bool termsIsOK = false;
  bool isSendingData = false;
  bool _dropshipperEnabled = false;
  bool _validateNama = false;
  bool _validateTelpon = false;
  bool _payment1Selected = false;
  bool _payment2Selected = false;
  bool _payment3Selected = false;
  bool _processSending = false;


  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _processSending = false;
  }

  void _onSwitchChanged(bool value) {
    if (this.mounted) {
      setState(() {
        _dropshipperEnabled = value;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        elevation: 1,
        backgroundColor: Colors.white,
        title: new Text(
          AppText.CHECKOUT_HEADER[Globals.langSelected], style: TextStyle(color: Colors.black87)),
        iconTheme: new IconThemeData(color: Colors.black54),
      ),
      body: _paymentPageBody(),
      bottomNavigationBar: _buildBottomNavigationBar(context),
    );
    //);
  }

  Widget cupertinoActionItem(String actionText, String actionID, String image) {
    return CupertinoActionSheetAction(
      child: Text(actionText),
      onPressed: () {
        if (this.mounted){
          setState(() {
            paymentID = actionID;
            paymentName = actionText;
            paymentImage = image;
          });
        }
        Navigator.pop(context);
        //print("Transfer Bank: " + actionText);
      },
    );
  }

  _buildCupertinoList(List<Widget> listCupertino, List<Payment> paymentList) {
    for (int i = 0; i < paymentList.length; i++) {
      listCupertino.add(
          cupertinoActionItem(
            paymentList[i].name,
            paymentList[i].id,
            paymentList[i].image,
          )
      );
    }

  }

  _paymentPageBody() {

    //return ScopedModelDescendant<CartScopedModel>(
    //builder: (context, child, model) {

    return FutureBuilder<dynamic>(
        future: cartModel.checkoutGet(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            // debug
            print("response checkout get: "+snapshot.data.toString());
            // api check
            addressIsOK = snapshot.data['address_ok'];
            shipmentIsOK = snapshot.data['shipping_ok'];
            paymentIsOK = snapshot.data['create_order'];
            // api payment
            transferList = cartModel.parsePayment(snapshot.data['payment_options']['Bank']);
            virtualAccountList = cartModel.parsePayment(snapshot.data['payment_options']['Virtual Account']);
            internetBankingList = cartModel.parsePayment(snapshot.data['payment_options']['Internet Banking']);
            counterList = cartModel.parsePayment(snapshot.data['payment_options']['Counter']);
            //print("addressIsOK: " + addressIsOK.toString());
            //print("shipmentIsOK: " + shipmentIsOK.toString());
          }
          return (snapshot.hasData)
              ? Container(
              padding: EdgeInsets.all(13.0),
              child: ListView(
                children: <Widget>[
                  Text(AppText.CHECKOUT_ADDRESS[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.bold)),
                  SizedBox(height: 5.0),
                  Card(
                    elevation: 1.0,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      //height: 195.0,
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            (snapshot.data['address'].toString()=="null")
                              ? Center(child: Text(AppText.CHECKOUT_ADDRESS_EMPTY[Globals.langSelected], style: TextStyle(fontSize: 13.0, color: Colors.grey[600])))
                              : Text(snapshot.data['address']['name'], style: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.bold)),
                            SizedBox(height: 2.0),

                            (snapshot.data['address'].toString()=="null")
                              ? Container()
                              : Text(snapshot.data['address']['address'], style: TextStyle(fontSize: 13.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                            //SizedBox(height: 2.0),
                            Center(
                              child: FlatButton(
                                  onPressed: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) {
                                            return AddressListPage();
                                          },
                                        )
                                    );
                                  },
                                  //color: Colors.deepPurple[400],
                                  child: Text(
                                      AppText.ADDRESS_LIST[Globals.langSelected],
                                      style: TextStyle(
                                        //fontWeight: FontWeight.bold,
                                          fontSize: 13.0,
                                          color: Colors.blue[400])
                                  )
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),

                  SizedBox(height: 8.0),
                  //Text("KIRIM SEBAGAI DROPSHIPPER:", style: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.bold)),
                  //SizedBox(height: 5.0),
                  Card(
                    elevation: 1.0,
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical:5.0, horizontal: 10.0),
                      //height: 60.0,
                      child: Column(
                        children: <Widget>[

                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(AppText.CHECKOUT_DROPSHIPPER[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                              Switch(
                                value: _dropshipperEnabled,
                                onChanged: _onSwitchChanged,
                                activeTrackColor: Colors.redAccent[100],
                                activeColor: Colors.red,
                              ),
                            ]
                          ),

                          // dropshipper name
                          (_dropshipperEnabled)
                          ? Column(
                            children: <Widget>[
                              TextField(
                                controller: _nama,
                                cursorColor: Colors.blue[400],
                                style: TextStyle(fontSize: 15, color: Colors.grey[800]),
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.grey[100],
                                  contentPadding: const EdgeInsets.all(7.0),
                                  hintText: "Nama",
                                  errorText: _validateNama ? 'Nama tidak boleh kosong' : null,
                                  focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.blue[400]),),
                                ),
                              ),
                              SizedBox(height: 10.0),
                              // dropshipper address
                              TextField(
                                controller: _telpon,
                                cursorColor: Colors.blue[400],
                                style: TextStyle(fontSize: 15, color: Colors.grey[800]),
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.grey[100],
                                  contentPadding: const EdgeInsets.all(7.0),
                                  hintText: "Nomor Telpon",
                                  errorText: _validateTelpon ? 'No telpon tidak boleh kosong' : null,
                                  focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.blue[400]),),
                                ),
                              ),
                              SizedBox(height: 15.0),
                            ],
                          )
                          : Container(),

                        ],
                      ),
                    ),
                  ),

/*
                  (addressIsOK)
                  ? Column(
                    children: <Widget>[
                      SizedBox(height: 8.0),
                      Card(
                        elevation: 1.0,
                        child: Container(
                          //height: 55.0,
                          //padding: const EdgeInsets.symmetric(vertical: 0.0),
                            child: ListTile(
                              title: Text(AppText.CHECKOUT_SHIPPING[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.grey[600])),
                              trailing: Icon(Icons.arrow_forward),
                              onTap: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(builder: (context) {
                                    return PaymentShipmentPage();
                                  },
                                  ),
                                );
                              },
                            )
                        ),
                      ),
                    ],
                  )
                  : Container(),
*/

                  SizedBox(height: 8.0),
                  Card(
                    elevation: 1.0,
                    child: Container(
                      //height: 55.0,
                      //padding: const EdgeInsets.symmetric(vertical: 0.0),
                        child: ListTile(
                          title: Text(AppText.CHECKOUT_VOUCHER[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.grey[600])),
                          trailing: Icon(Icons.arrow_forward),
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) {
                                  return PaymentVoucherPage();
                                },
                              ),
                            );
                          },
                        )
                    ),
                  ),
/*
                  SizedBox(height: 8.0),
                  Text("VOUCHER BELANJA:", style: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.bold)),
                  SizedBox(height: 5.0),
                  Card(
                    elevation: 1.0,
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical:5.0, horizontal: 10.0),
                      height: 60.0,
                      //width: 100.0,
                      //padding: const EdgeInsets.symmetric(vertical: 0.0),
                      child: new Row(
                        children: <Widget>[
                          new Flexible(
                            child: TextField(
                              //maxLines: 8,
                              //maxLength: 4,
                              controller: null,
                              cursorColor: Colors.deepPurple[400],
                              style: TextStyle(fontSize: 15, color: Colors.grey[800]),
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.grey[100],
                                contentPadding: const EdgeInsets.all(7.0),
                                hintText: "Kode Voucher",
                                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.deepPurple[400]),
                                ),
                              ),
                            ),
                          ),

                          SizedBox(width: 10.0),

                          FlatButton(
                              onPressed: () {

                              },
                              color: Colors.deepPurple[400],
                              child: Text(
                                  "GUNAKAN",
                                  style: TextStyle(
                                    //fontWeight: FontWeight.bold,
                                      fontSize: 13.0,
                                      color: Colors.white)
                              )
                          )

                        ],
                      ),
                    ),
                  ),
*/

                  SizedBox(height: 8.0),
                  Text(AppText.CHECKOUT_SUMMARY[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.bold)),
                  SizedBox(height: 5.0),
                  Card(
                    elevation: 1.0,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      //height: 170.0,
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 14.0),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(AppText.CHECKOUT_SHIPPING_SERVICE[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                                  Text(snapshot.data['shipping_service'] != null ? snapshot.data['shipping_service'] : "", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.bold)),
                                ]
                            ),
                            SizedBox(height: 14.0),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(AppText.CHECKOUT_ETA[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                                  InkWell(
                                    onTap: () {
                                      if (addressIsOK) {
                                        Navigator.of(context).push(
                                          MaterialPageRoute(builder: (context) {
                                            return PaymentShipmentPage();
                                          },
                                          ),
                                        );
                                      }
                                    },
                                    child: Text(snapshot.data['shipping_etd'] != null ? snapshot.data['shipping_etd'] + " hari (UBAH)" : "", style: TextStyle(fontSize: 14.0, color: Colors.blue[400], fontWeight: FontWeight.normal)),
                                  ),
                                ]
                            ),
                            SizedBox(height: 14.0),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(AppText.CHECKOUT_SHIPPING_FEE[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                                  Text(snapshot.data['subtotal_shipping_text'] != null ? snapshot.data['subtotal_shipping_text'] : "", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.bold)),
                                ]
                            ),
                            SizedBox(height: 14.0),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(AppText.CHECKOUT_CUT_FEE[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                                  Text(snapshot.data['subtotal_cuts_text'] != null ? snapshot.data['subtotal_cuts_text'] : "", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.bold)),
                                ]
                            ),
                            SizedBox(height: 14.0),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(AppText.CHECKOUT_ITEM_QUANTITY[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                                  Text(snapshot.data['item_count'].toString() != null ? snapshot.data['item_count'].toString() : "", style: TextStyle(fontSize: 14.0, color: Colors.redAccent, fontWeight: FontWeight.bold)),
                                ]
                            ),
                            SizedBox(height: 14.0),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(AppText.CHECKOUT_PRICE[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                                  Text(snapshot.data['subtotal_items_text'] != null ? snapshot.data['subtotal_items_text'] : "", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.bold)),
                                ]
                            ),
                            SizedBox(height: 14.0),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(AppText.CHECKOUT_DISCOUNT[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                                  Text(snapshot.data['discount_text'] != null ? snapshot.data['discount_text'] : "", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.bold)),
                                ]
                            ),
                            SizedBox(height: 14.0),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(AppText.CHECKOUT_TOTAL[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                                  Text(snapshot.data['total_text'] != null ? snapshot.data['total_text'] : "", style: TextStyle(fontSize: 14.0, color: Colors.redAccent, fontWeight: FontWeight.bold)),
                                ]
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),

                  SizedBox(height: 8.0),
                  Card(
                    elevation: 1.0,
                    child: Container(
                      //height: 55.0,
                      //padding: const EdgeInsets.symmetric(vertical: 0.0),
                        child: ListTile(
                          title: Text("I Agree terms and agreements", style: TextStyle(fontSize: 14.0, color: Colors.blue[400])),
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) {
                                return ProductTermsPage();
                              },
                              ),
                            );
                          },
                          trailing: InkWell(
                            onTap: () {
                              setState(() {
                                (termsIsOK) ? termsIsOK = false : termsIsOK = true;
                              });
                            },
                            child: (termsIsOK)
                              ? Icon(Icons.check_box, color: Colors.redAccent)
                              : Icon(Icons.check_box_outline_blank, color: Colors.grey),
                          ),
                        )
                    ),
                  ),

                  SizedBox(height: 10.0),
                  Text(AppText.CHECKOUT_PAYMENT[Globals.langSelected], style: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.bold)),
                  SizedBox(height: 5.0),

                  Card(
                    elevation: 1.0,
                    child: Container(
                      //height: 55.0,
                      //padding: const EdgeInsets.symmetric(vertical: 0.0),
                      child: ListTile(
                        contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
                        leading: Image.asset('assets/images/bca_mandiri.jpg', width: 120.0, height: 50.0),
                        title: Text("Virtual Account", style: TextStyle(fontSize: 13.5, color: Colors.black)),
                        subtitle: (_payment1Selected && paymentName != "") ? Text(paymentName, style: TextStyle(fontSize: 10, color: Colors.grey[500])) : null,
                        //trailing: (_payment1Selected && paymentName != "") ? Icon(Icons.check_box, color: Colors.redAccent) : Icon(Icons.check_box_outline_blank, color: Colors.grey),
                        trailing: (_payment1Selected && paymentName != "") ? Icon(Icons.check_circle, color: Colors.redAccent) : null,
                        onTap: () {
                          if (this.mounted) {
                            setState(() {
                              _payment1Selected = true;
                              _payment2Selected = false;
                              _payment3Selected = false;
                              paymentName = "";
                            });
                          }

                          List<Widget> listCupertino = new List();
                          _buildCupertinoList(listCupertino, virtualAccountList);

                          showCupertinoModalPopup(
                            context: context,
                            builder: (BuildContext context) =>
                                CupertinoActionSheet(
                                  title: Text(AppText.VA_TITLE[Globals.langSelected]),
                                  message: Text(AppText.VA_SUBTITLE[Globals.langSelected]),
                                  actions: listCupertino,
                                ),
                          );

                        },
                      ),
                    ),
                  ),
                  Card(
                    elevation: 1.0,
                    child: Container(
                      //height: 55.0,
                      //padding: const EdgeInsets.symmetric(vertical: 0.0),
                      child: ListTile(
                        contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
                        leading: Image.asset('assets/images/bca_mandiri.jpg', width: 120.0, height: 50.0),
                        title: Text("Direct Debit", style: TextStyle(fontSize: 13.5, color: Colors.black)),
                        subtitle: (_payment2Selected && paymentName != "") ? Text(paymentName, style: TextStyle(fontSize: 10, color: Colors.grey[500])) : null,
                        trailing: (_payment2Selected && paymentName != "") ? Icon(Icons.check_circle, color: Colors.redAccent) : null,
                        onTap: () {
                          if (this.mounted) {
                            setState(() {
                              _payment1Selected = false;
                              _payment2Selected = true;
                              _payment3Selected = false;
                              paymentName = "";
                            });
                          }

                          List<Widget> listCupertino = new List();
                          _buildCupertinoList(listCupertino, internetBankingList);

                          showCupertinoModalPopup(
                            context: context,
                            builder: (BuildContext context) =>
                                CupertinoActionSheet(
                                  title: Text(AppText.DD_TITLE[Globals.langSelected]),
                                  message: Text(AppText.DD_SUBTITLE[Globals.langSelected]),
                                  actions: listCupertino,
                                ),
                          );

                        },
                      ),
                    ),
                  ),
                  Card(
                    elevation: 1.0,
                    child: Container(
                      //height: 55.0,
                      //padding: const EdgeInsets.symmetric(vertical: 0.0),
                      child: ListTile(
                        contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
                        leading: Image.asset('assets/images/indo_alfa.jpg', width: 120.0, height: 50.0),
                        title: Text("Counter", style: TextStyle(fontSize: 13.5, color: Colors.black)),
                        subtitle: (_payment3Selected && paymentName != "") ? Text(paymentName, style: TextStyle(fontSize: 10, color: Colors.grey[500])) : null,
                        trailing: (_payment3Selected && paymentName != "") ? Icon(Icons.check_circle, color: Colors.redAccent) : null,
                        onTap: () {
                          List<Widget> listCupertino = new List();
                          if (this.mounted) {
                            setState(() {
                              _payment1Selected = false;
                              _payment2Selected = false;
                              _payment3Selected = true;
                              paymentName = "";
                            });
                          }

                          _buildCupertinoList(listCupertino, counterList);

                          showCupertinoModalPopup(
                            context: context,
                            builder: (BuildContext context) =>
                                CupertinoActionSheet(
                                  title: Text(AppText.COUNTER_TITLE[Globals.langSelected]),
                                  message: Text(AppText.COUNTER_SUBTITLE[Globals.langSelected]),
                                  actions: listCupertino,
                                ),
                          );

                        },
                      ),
                    ),
                  ),

/*
                  Card(
                    elevation: 1.0,
                    child: Container(
                      //height: 55.0,
                      //padding: const EdgeInsets.symmetric(vertical: 0.0),
                      child: ListTile(
                        contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
                        leading: Image.asset('assets/images/indomaret_icon.png', width: 50.0, height: 30.0),
                        title: Text("Indomaret", style: TextStyle(fontSize: 13.5, color: Colors.grey[600])),
                        trailing: Icon(Icons.arrow_forward, color: Colors.grey[600]),
                        onTap: () {

                        },
                      ),
                    ),
                  ),
                  Card(
                    elevation: 1.0,
                    child: Container(
                      //height: 55.0,
                      //padding: const EdgeInsets.symmetric(vertical: 0.0),
                      child: ListTile(
                        contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
                        leading: Image.asset('assets/images/bank_icon.png', width: 50.0, height: 30.0),
                        title: Text("e-Pay BRI", style: TextStyle(fontSize: 13.5, color: Colors.grey[600])),
                        trailing: Icon(Icons.arrow_forward, color: Colors.grey[600]),
                        onTap: () {

                        },
                      ),
                    ),
                  ),
*/
/*
                  SizedBox(height: 20.0),
                  (isSendingData == true) 
                  ? Center(child: CircularProgressIndicator())
                  : Container(),
                  
                  (addressIsOK && shipmentIsOK && paymentID != "")
                  ? RaisedButton(
                      color: Colors.deepPurple[400],
                      onPressed: () {

                        setState(() {
                          isSendingData = true;
                        });

                        cartModel.checkoutCommit(0, paymentID, "", "").then((response) {

                          setState(() {
                            isSendingData = false;
                          });

                          // go to page without back (no way back ma man)
                          Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PaymentCheckoutPage(
                                  paymentImage: paymentImage,
                                  paymentName: paymentName,
                                  orderID: response['order_id'],
                                  jumlahTransfer: response['gross_amount'],
                                  noRekening: response['payment_acc_number'],
                                  expired: response['expired_at'],
                                )
                            ),
                            (Route<dynamic> route) => false
                          );

                        });
                        },
                      child: Text(
                          "BAYAR",
                          style: TextStyle(
                            //fontWeight: FontWeight.bold,
                              color: Colors.white,
                              fontSize: 15.0)
                      )
                  )
                  : RaisedButton(
                    color: Colors.deepPurple[400],
                    onPressed: null,
                    child: Text(
                        "BAYAR",
                        style: TextStyle(
                          //fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: 15.0)
                    )
                  ),
*/
                ],
              )
          )
          : _buildSplashScreen();
        }
    );
  }

  _buildSplashScreen() {

    return Stack(
      children: <Widget>[
        Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
        ),

        Center(
          child: GlowingProgressIndicator(
              duration: Duration(milliseconds: 400),
              child: Container(
                height: 70.0,
                width: 70.0,
                child: (Globals.brandID == 1)
                    ? Image.asset('assets/images/logo_zb_10.png', color: Colors.black54)
                    : (Globals.brandID == 2)
                    ? Image.asset('assets/images/logo_zb_20.png', color: Colors.black54)
                    : Image.asset('assets/images/logo_zb_30.png', color: Colors.black54),
              )
          ),
        ),

/*
          Container(
            height: double.infinity,
            width: double.infinity,
          ),
*/
      ],
    );
  }


  _buildBottomNavigationBar(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Flexible(
            flex: 1,
            child:
            // bayar
            //?
            RaisedButton(
               onPressed: () {

                 if (/*paymentIsOK &&*/ termsIsOK && addressIsOK && shipmentIsOK && paymentID != "" &&
                     (_dropshipperEnabled ? (_nama.text != "" && _telpon.text != "") : true)) {

                   if (!_processSending) {

                     setState(() {
                       _processSending = true;
                     });

                     _scaffoldKey.currentState.showSnackBar(
                         new SnackBar(
                           duration: new Duration(seconds: 2),
                           content: Row(
                             mainAxisAlignment: MainAxisAlignment.center,
                             children: <Widget>[
                               new CircularProgressIndicator(),
                               new Text("  Please wait...")
                             ],
                           ),
                         )
                     );

                     cartModel.checkoutCommit(
                         _dropshipperEnabled ? 1 : 0,
                         paymentID,
                         _nama.text != null ? _nama.text : "",
                         _telpon.text != null ? _telpon.text : "")
                         .then((response) {

                       _scaffoldKey.currentState.hideCurrentSnackBar();
                       // go to page without back (no way back ma man)
                       //print("response status"+response['status']);

                       if (response['status'] != "error") {

                         if (_payment1Selected || _payment3Selected) {

                           var dataFromResponse = response['steps'];

                           List<StepsPayment> steps = [];
                           dataFromResponse.forEach((newSteps) {
                             steps.add(
                                 new StepsPayment(
                                   title: newSteps["title"],
                                   content: newSteps["content"],
                                 )
                             );
                           }
                           );

                           Navigator.pushAndRemoveUntil(
                               context,
                               MaterialPageRoute(
                                   builder: (context) => PaymentVirtualAccPage(
                                     paymentImage: paymentImage,
                                     paymentName: paymentName,
                                     paymentType: response['payment_type'],
                                     orderID: response['order_id'],
                                     jumlahTransfer: response['gross_amount'],
                                     vaNumber: response['va_number'],
                                     expired: response['expired_at'],
                                     stepsPayment: steps,
                                   )
                               ),
                                   (Route<dynamic> route) => false
                           );
                         }

                         if (_payment2Selected) {
                           Navigator.pushAndRemoveUntil(
                               context,
                               MaterialPageRoute(
                                   builder: (context) => PaymentRedirectPage(
                                     paymentImage: paymentImage,
                                     paymentName: paymentName,
                                     paymentType: response['payment_type'],
                                     orderID: response['order_id'],
                                     jumlahTransfer: response['gross_amount'],
                                     redirectURL: response['redirect_url'],
                                     expired: response['expired_at'],
                                   )
                               ),
                                   (Route<dynamic> route) => false
                           );
                         }
                       } else {
                         //error message from api
                         _scaffoldKey.currentState.showSnackBar(
                             new SnackBar(
                               duration: new Duration(minutes: 30),
                               content: Row(
                                 mainAxisAlignment: MainAxisAlignment.center,
                                 children: <Widget>[
                                    Expanded(
                                      child: Text(response['message'])
                                    ),

                                   RaisedButton(
                                     onPressed: () {

                                       _scaffoldKey.currentState.hideCurrentSnackBar();
                                       Navigator.of(context).pop();

                                     },
                                     color: Colors.redAccent,
                                     child: Text("OK"),
                                   )


                                 ],
                               ),
                             )
                         );
                       }

                     });

                   }

                 } else {

                   // error validation from app
                   if (this.mounted) {
                     setState(() {
                       _nama.text.isEmpty ? _validateNama = true : _validateNama = false;
                       _telpon.text.isEmpty ? _validateTelpon = true : _validateTelpon = false;
                     });

                   }

                   // generate error message
                   String errorMessage = "";
                   if (!addressIsOK) errorMessage += "- Address not defined\n";
                   if (!shipmentIsOK) errorMessage += "- Shipment not defined\n";
                   if (!termsIsOK) errorMessage += "- You must agree with Terms\n";
                   if (paymentID == "") errorMessage += "- Payment not defined\n";
                   if (_dropshipperEnabled ? (_nama.text == "" || _telpon.text == "") : false) errorMessage += "- Dropshimpment not complete\n";

                   _scaffoldKey.currentState.showSnackBar(
                       new SnackBar(
                         duration: new Duration(seconds: 4),
                         content: Row(
                           mainAxisAlignment: MainAxisAlignment.center,
                           children: <Widget>[
                              Expanded(child: Text(errorMessage.trim())),
/*
                               RaisedButton(
                                 onPressed: () {

                                   _scaffoldKey.currentState.hideCurrentSnackBar();

                                 },
                                 color: Colors.redAccent,
                                 child: Text("OK"),
                               )
*/

                           ],
                         ),
                       )
                   );

                 }


              },
              color: Colors.white,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.monetization_on,
                      color: Colors.black,
                    ),
                    SizedBox(
                      width: 4.0,
                    ),
                    Text(
                      AppText.CHECKOUT_PAY_BUTTON[Globals.langSelected],
                      style: TextStyle(color: Colors.black),
                    ),
                  ],
                ),
              ),
            )

/*
          : RaisedButton(
              onPressed: () {
                // validate
                 setState(() {
                   _nama.text.isEmpty ? _validateNama = true : _validateNama = false;
                   _telpon.text.isEmpty ? _validateTelpon = true : _validateTelpon = false;
                 });

                 _scaffoldKey.currentState.showSnackBar(
                     new SnackBar(
                       duration: new Duration(seconds: 2),
                       content: Row(
                         mainAxisAlignment: MainAxisAlignment.center,
                         children: <Widget>[
                           new Text("Data tidak lengkap...")
                         ],
                       ),
                     )
                 );


              },
              color: Colors.grey,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.check_circle,
                      color: Colors.white,
                    ),
                    SizedBox(
                      width: 4.0,
                    ),
                    Text(
                      "BAYAR",
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
*/


          ),
        ],
      ),
    );
  }

}

