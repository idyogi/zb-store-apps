import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zb_store/scoped_model/cart_scoped_model.dart';
//import 'package:image_picker/image_picker.dart';
import 'package:zb_store/util/globals.dart';

class PaymentTransferPage extends StatefulWidget {

  final String orderID;
  final String jumlahTransfer;
  final String paymentName;
  final String paymentImage;
  final String noRekening;
  final String expired;

  PaymentTransferPage({
    @required this.orderID,
    @required this.jumlahTransfer,
    @required this.paymentName,
    @required this.paymentImage,
    @required this.noRekening,
    @required this.expired,
  });

  @override
  PaymentTransferPageState createState() {
    return new PaymentTransferPageState(
      orderID: orderID,
      jumlahTransfer: jumlahTransfer,
      paymentName: paymentName,
      paymentImage: paymentImage,
      noRekening: noRekening,
      expired: expired
    );
  }
}

class PaymentTransferPageState extends State<PaymentTransferPage> {

  final String orderID;
  final String jumlahTransfer;
  final String paymentName;
  final String paymentImage;
  final String noRekening;
  final String expired;

  PaymentTransferPageState({
    @required this.orderID,
    @required this.jumlahTransfer,
    @required this.paymentName,
    @required this.paymentImage,
    @required this.noRekening,
    @required this.expired,
  });

  File file;
  //String orderID = "";
  String base64Image = "";
  CartScopedModel cartModel = CartScopedModel();
  bool fileImageExist = false;
  bool uploadStarted = false;
  bool uploadSuccess = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0.5,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: new Text(
            "Info Pembayaran", style: TextStyle(color: Colors.black87)),
        iconTheme: new IconThemeData(color: Colors.black54),
      ),
      body: _paymentCheckoutBody(context),
      bottomNavigationBar: _buildBottomNavigationBar(context),
    );
  }

  _paymentCheckoutBody(BuildContext context) {

/*
    void _chooseImage(String source) async {
      (source == "camera")
      ? file = await ImagePicker.pickImage(source: ImageSource.camera)
      : file = await ImagePicker.pickImage(source: ImageSource.gallery);
      setState(() {
        fileImageExist = true;
      });
    }
*/

    void _convertImage() {
      if (file == null) return;
      base64Image = base64Encode(file.readAsBytesSync());
      print("base64Image: $base64Image");
    }

    return Container(
      padding: EdgeInsets.all(13.0),
        child: ListView(
          children: <Widget>[
            Text("TOTAL PEMBAYARAN:", style: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.bold)),
            SizedBox(height: 5.0),
            Card(
              elevation: 1.0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                //height: 195.0,
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(jumlahTransfer != null ? jumlahTransfer : "", style: TextStyle(fontSize: 32.0, color: Colors.black, fontWeight: FontWeight.bold)),
                      SizedBox(height: 5.0),
                      Text("Bayar pesanan sesuai jumlah di atas", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                      SizedBox(height: 5.0),
                    ],
                  ),
                ),
              ),
            ),


            SizedBox(height: 8.0),
            Text("TRANSFER KE REKENING:", style: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.bold)),
            SizedBox(height: 5.0),
            Card(
              elevation: 1.0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                //height: 170.0,
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 14.0),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: <Widget>[
                                Image.network(paymentImage),
                                SizedBox(width: 10.0),
                                Text(paymentName != null ? paymentName : "", style: TextStyle(fontSize: 20.0, color: Colors.black, fontWeight: FontWeight.normal)),
                              ],
                            ),
                            //Text(snapshot.data['subtotal_items_text'] != null ? snapshot.data['subtotal_items_text'] : "", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.bold)),
                          ]
                      ),
                      SizedBox(height: 14.0),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(noRekening != null ? "Nomor Rekening: " + noRekening : "", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),
                            //Text(snapshot.data['subtotal_shipping_text'] != null ? snapshot.data['subtotal_shipping_text'] : "", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.bold)),
                          ]
                      ),
                      SizedBox(height: 14.0),
                      Text(expired != null ? "Silahkan upload bukti transfer sebelum: " + expired : "", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)),

                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: 8.0),

            Center(
              child: fileImageExist && file != null
                ? Card(
                    elevation: 1.0,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      //height: 170.0,
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(child: Text("Foto bukti transfer:", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal))),
                            SizedBox(height: 15.0),
                            Padding(
                              padding: EdgeInsets.only(left: 25, right: 25, bottom: 5),
                              child: Column(
                                children: <Widget>[

                                  Image.file(file),
                                  SizedBox(height: 10.0),

                                  (uploadStarted)
                                  ? Center(child: CircularProgressIndicator())
                                  : Container(),

                                  (uploadSuccess)
                                  ? Center(child: Text("Bukti transfer suskes terupload", style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.normal)))
                                  : Container(),

                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                )
              : Container(),
            ),

            SizedBox(height: 15.0),
            RaisedButton(
                color: Colors.deepPurple[400],
                onPressed: () {
                  setState(() {
                    uploadSuccess = false;
                  });
                  showCupertinoModalPopup(
                    context: context,
                    builder: (BuildContext context) =>
                        CupertinoActionSheet(
                          title: const Text('Pilih Sumber Foto'),
                          message: const Text('Camera atau Gallery'),
                          actions: <Widget>[

                            CupertinoActionSheetAction(
                                child: Text("Camera"),
                                onPressed: () {
                                  //_chooseImage("camera");
                                  Navigator.pop(context);
                                }
                            ),
                            CupertinoActionSheetAction(
                                child: Text("Gallery"),
                                onPressed: () {
                                  //_chooseImage("gallery");
                                  Navigator.pop(context);
                                }
                            ),
                          ],
                        ),
                  );
                },
                child: Text(
                    "FOTO BUKTI TRANSFER",
                    style: TextStyle(
                      //fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 15.0)
                )
            ),


            SizedBox(height: 10.0),
            (fileImageExist && file != null)
            ? RaisedButton(
                color: Colors.deepPurple[400],
                onPressed: () {
                  setState(() {
                    uploadStarted = true;
                  });

                  _convertImage();
                  print("order id: $orderID");
                  print("base64Image: $base64Image");

                  cartModel.uploadTransfer(orderID, base64Image).then((response) {
                    print("response: " + response.toString());
                    setState(() {
                      uploadSuccess = true;
                      uploadStarted = false;
                    });
                  });
                },
                child: Text(
                    "UPLOAD BUKTI TRANSFER",
                    style: TextStyle(
                      //fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 15.0)
                )
              )
            : RaisedButton(
                color: Colors.deepPurple[400],
                onPressed: null,
                child: Text(
                    "UPLOAD BUKTI TRANSFER",
                    style: TextStyle(
                      //fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 15.0)
                )
            ),

/*
            SizedBox(height: 10.0),
            (uploadSuccess && fileImageExist && file != null)
              ? RaisedButton(
                  color: Colors.deepPurple[400],
                  onPressed: () {
                    Navigator.pushReplacementNamed(context, Constants.ROUTE_HOME_PAGE);
                  },
                  child: Text(
                      "KEMBALI BELANJA",
                      style: TextStyle(
                        //fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 15.0)
                  )
                )
              : RaisedButton(
                  color: Colors.deepPurple[400],
                  onPressed: null,
                  child: Text(
                      "KEMBALI BELANJA",
                      style: TextStyle(
                        //fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 15.0)
                  )
                ),
*/

          ],
        )
      );
  }

  _buildBottomNavigationBar(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 60.0,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Flexible(
            flex: 1,
            child: (uploadSuccess && fileImageExist && file != null)

                ?

            RaisedButton(
              onPressed: () {
                Navigator.pushReplacementNamed(context, Constants.ROUTE_HOME_PAGE);
              },
              color: Colors.greenAccent,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.check_circle,
                      color: Colors.white,
                    ),
                    SizedBox(
                      width: 4.0,
                    ),
                    Text(
                      "KEMBALI BELANJA",
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
            )

                :             RaisedButton(
              onPressed: null,
              color: Colors.greenAccent,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.check_circle,
                      color: Colors.white,
                    ),
                    SizedBox(
                      width: 4.0,
                    ),
                    Text(
                      "KEMBALI BELANJA",
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),


          ),
        ],
      ),
    );
  }

}
