import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/util/languages.dart';


class ProductDescPage extends StatelessWidget {
  final String description;
  ProductDescPage({this.description});

  @override
  Widget build(BuildContext context) {
    print("web view url: $description");
    return WebviewScaffold(
      //key: _scaffoldKey,
      //url: "https://www.google.com",
      //url: "https://en.wikipedia.org/wiki/IOS",
      url: description,
      appBar: AppBar(
        elevation: 1.0,
        leading: new IconButton(
          icon: new Icon(Icons.close, color: Colors.black54),
          onPressed: ()=>Navigator.of(context).pop(),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: new Text(
            AppText.PRODUCT_DESCRIPTION[Globals.langSelected], style: TextStyle(color: Colors.black87)),
        iconTheme: new IconThemeData(color: Colors.black54),
      ),
/*
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
        child: ListView(
          children: <Widget>[
            Text(
              description,
              style: TextStyle(
                color: Colors.grey[600],
              ),
            ),
          ],
        ),
      ),
*/
      //bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

}