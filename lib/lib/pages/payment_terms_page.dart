import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/util/languages.dart';


class ProductTermsPage extends StatelessWidget {
  //ProductDescPage({this.description});

  @override
  Widget build(BuildContext context) {
    String description = "http://beta.devsandbox.me/api/checkout/terms";
    //print("web view url: $description");
    return WebviewScaffold(
      //key: _scaffoldKey,
      //url: "https://www.google.com",
      //url: "https://en.wikipedia.org/wiki/IOS",
      url: description,
      appBar: AppBar(
        elevation: 1.0,
        leading: new IconButton(
          icon: new Icon(Icons.close, color: Colors.black54),
          onPressed: ()=>Navigator.of(context).pop(),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: new Text(
            "Terms and Aggreements", style: TextStyle(color: Colors.black87)),
        iconTheme: new IconThemeData(color: Colors.black54),
      ),
/*
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
        child: ListView(
          children: <Widget>[
            Text(
              description,
              style: TextStyle(
                color: Colors.grey[600],
              ),
            ),
          ],
        ),
      ),
*/
      //bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

}