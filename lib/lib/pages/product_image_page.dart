import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class ProductImagePage extends StatelessWidget {
  final String imageURL;
  ProductImagePage({this.imageURL});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      //onWillPop: _onWillPop,
      child: Stack(
        children: <Widget>[

          Container(
            child: Hero(
              tag: 'imageHero',
              child: PhotoView(
                minScale: PhotoViewComputedScale.contained * 1.0,
                maxScale: PhotoViewComputedScale.contained * 4.0,
                imageProvider: NetworkImage(imageURL),
                backgroundDecoration: BoxDecoration(
                    color: Colors.white),
              ),
            ),
          ),

          new Positioned( //Place it at the top, and not use the entire screen
            top: 0.0,
            left: 0.0,
            right: 0.0,
            child: AppBar(
              elevation: 0,
              backgroundColor: Colors.transparent,
              leading: new IconButton(
                icon: new Icon(Icons.close, color: Colors.black54),
                //onPressed: () => _onWillPop(),
                onPressed: () => Navigator.of(context).pop(),
                ),
              ),
          ),

        ],
      ),
    );
  }

}