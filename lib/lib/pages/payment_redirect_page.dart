import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:zb_store/util/globals.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:zb_store/util/languages.dart';


class PaymentRedirectPage extends StatefulWidget {

  final String orderID;
  final String jumlahTransfer;
  final String paymentType;
  final String paymentName;
  final String paymentImage;
  final String redirectURL;
  final String expired;

  PaymentRedirectPage({
    @required this.orderID,
    @required this.jumlahTransfer,
    @required this.paymentType,
    @required this.paymentName,
    @required this.paymentImage,
    @required this.redirectURL,
    @required this.expired,
  });

  @override
  PaymentRedirectPageState createState() {
    return new PaymentRedirectPageState(
        orderID: orderID,
        jumlahTransfer: jumlahTransfer,
        paymentType: paymentType,
        paymentName: paymentName,
        paymentImage: paymentImage,
        redirectURL: redirectURL,
        expired: expired
    );
  }
}

class PaymentRedirectPageState extends State<PaymentRedirectPage> {

  final String orderID;
  final String jumlahTransfer;
  final String paymentType;
  final String paymentName;
  final String paymentImage;
  final String redirectURL;
  final String expired;

  PaymentRedirectPageState({
    @required this.orderID,
    @required this.jumlahTransfer,
    @required this.paymentType,
    @required this.paymentName,
    @required this.paymentImage,
    @required this.redirectURL,
    @required this.expired,
  });

  final _key = UniqueKey();
  num _stackToView = 1;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void _handleLoad(String value) {
    setState(() {
      _stackToView = 0;
    });
  }

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Keluar dari menu pembayaran?'),
        content: new Text('Anda bisa kembali mengakses informasi ini pada menu utama: Pesanan Saya'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('Tidak'),
          ),
          new FlatButton(
            onPressed: () => Navigator.pushReplacementNamed(context, Constants.ROUTE_HOME_PAGE),
            child: new Text('Ya'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new WillPopScope(
        onWillPop: _onWillPop,
        child: //Scaffold(
            //key: _scaffoldKey,
/*
            appBar: AppBar(
              //leading: CloseButton(),
              leading: new IconButton(
                icon: new Icon(Icons.close, color: Colors.black54),
                onPressed: () => _onWillPop(),
              ),
              elevation: 0.5,
              centerTitle: true,
              backgroundColor: Colors.white,
              title: new Text(
                  "Info Pembayaran", style: TextStyle(color: Colors.black87)),
              iconTheme: new IconThemeData(color: Colors.black54),
            ),
*/
            //body:
            WebviewScaffold(
              key: _scaffoldKey,
              url: redirectURL,
              appBar: AppBar(
                //leading: CloseButton(),
                leading: new IconButton(
                  icon: new Icon(Icons.close, color: Colors.black54),
                  //onPressed: () => _onWillPop(),
                  onPressed: () => Navigator.pushReplacementNamed(context, Constants.ROUTE_HOME_PAGE),

                ),
                elevation: 0.5,
                centerTitle: true,
                backgroundColor: Colors.white,
                title: new Text(
                    AppText.PAYMENT_INFO_HEADER[Globals.langSelected], style: TextStyle(color: Colors.black87)),
                iconTheme: new IconThemeData(color: Colors.black54),
              ),

/*
              appBar: new AppBar(
                title: const Text('Widget webview'),
              ),
*/
              withZoom: true,
              withLocalStorage: true,
              hidden: false,
              initialChild: Container(
                color: Colors.white,
                child: const Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ),

/*
            IndexedStack(
              index: _stackToView,
              children: <Widget>[
                Column(
                  children: [
                    Expanded(
                      child: WebView(
                        key: _key,
                        javascriptMode: JavascriptMode.unrestricted,
                        initialUrl: redirectURL,
                        onPageFinished: _handleLoad,
                      ),
                    ),
                  ],
                ),
                Container(
                  color: Colors.white,
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
              ],
            ),
*/
        );
    //);
  }
}
