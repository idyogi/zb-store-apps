import 'package:flutter/material.dart';
import 'package:zb_store/model/address.dart';
import 'package:zb_store/scoped_model/address_scoped_model.dart';
import 'package:zb_store/util/languages.dart';
import 'address_province_page.dart';
import 'package:zb_store/util/globals.dart';

class AddressAddPage extends StatefulWidget {
  @override
  AddressAddPageState createState() => new AddressAddPageState();
}

class AddressAddPageState extends State<AddressAddPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final AddressScopedModel addressModel = AddressScopedModel();

  final _nama = TextEditingController();
  final _alamat = TextEditingController();
  final _desa = TextEditingController();
  final _kodepos = TextEditingController();
  final _telpon = TextEditingController();
  bool _fieldNamaIsEmpty = false;
  bool _fieldAlamatIsEmpty = false;
  bool _fieldTelponIsEmpty = false;
  bool _fieldProvinceIsEmpty = false;
  bool _fieldCityIsEmpty = false;
  bool _fieldSubdistrictIsEmpty = false;
  bool _isDefault = false;
  bool allSet = false;



  @override
  void dispose() {
    _nama.dispose();
    _alamat.dispose();
    _telpon.dispose();
    _desa.dispose();
    _kodepos.dispose();
    super.dispose();
  }

  void _onSwitchChanged(bool value) {
    setState(() {
      _isDefault = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 1,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: new Text(
            AppText.CHECKOUT_ADDRESS_ADD[Globals.langSelected], style: TextStyle(color: Colors.black87)),
        iconTheme: new IconThemeData(color: Colors.black54),
      ),
      body: _buildAddressAddPageBody(),
      bottomNavigationBar: _buildBottomNavigationBar(context),
    );
  }

  _buildBottomNavigationBar(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Flexible(
            flex: 1,
            child: RaisedButton(
              onPressed: () {


                  String alamatUtama = "";
                  // validate before save
                  setState(() {
                    // validate
                    _nama.text.isEmpty ? _fieldNamaIsEmpty = true : _fieldNamaIsEmpty = false;
                    _alamat.text.isEmpty || _alamat.text.length < 10 ? _fieldAlamatIsEmpty = true : _fieldAlamatIsEmpty = false;
                    _telpon.text.isEmpty ? _fieldTelponIsEmpty = true : _fieldTelponIsEmpty = false;
                    Globals.provinceName == "" ? _fieldProvinceIsEmpty = true : _fieldProvinceIsEmpty = false;
                    Globals.cityName == "" ? _fieldCityIsEmpty = true : _fieldCityIsEmpty = false;
                    Globals.subdistrictName == "" ? _fieldSubdistrictIsEmpty = true : _fieldSubdistrictIsEmpty = false;
                    _isDefault == true ? alamatUtama = "1" : alamatUtama = "0";
                  });

                  allSet = (!_fieldNamaIsEmpty && !_fieldAlamatIsEmpty && !_fieldTelponIsEmpty &&
                      !_fieldProvinceIsEmpty && !_fieldCityIsEmpty && !_fieldSubdistrictIsEmpty);

                  if (allSet) {
                  // send api
/*
                _scaffoldKey.currentState.showSnackBar(
                    new SnackBar(
                      duration: new Duration(seconds: 2),
                      content: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          FutureBuilder<dynamic>(
                              future: addressModel.addAddress(
                                  _nama.text,
                                  Globals.provinceID,
                                  Globals.cityID,
                                  Globals.subdistrictID,
                                  _alamat.text,
                                  Globals.postalCode,
                                  _telpon.text,
                                  alamatUtama
                              ),
                              builder: (context, snapshot) {
                                return (snapshot.hasData)
                                    ? Text(snapshot.data['message'])
                                    : CircularProgressIndicator();
                              }
                          ),
                        ],
                      ),
                    )
                );
*/
                  _scaffoldKey.currentState.showSnackBar(
                      new SnackBar(
                        duration: new Duration(minutes: 2),
                        content: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new CircularProgressIndicator(),
                            new Text("  Please wait...")
                          ],
                        ),
                      )
                  );

                  addressModel.addAddress(
                      _nama.text,
                      Globals.provinceID,
                      Globals.cityID,
                      Globals.subdistrictID,
                      _desa.text,
                      _kodepos.text,
                      _alamat.text,
                      _telpon.text,
                      alamatUtama
                  ).then((response) {
                    print("add address response: " + response.toString());
                    _scaffoldKey.currentState.hideCurrentSnackBar();
                    Navigator.of(context).pop();
                  });

                } else {

                  _scaffoldKey.currentState.showSnackBar(
                      new SnackBar(
                        duration: new Duration(seconds: 2),
                        content: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Text("Please fill all fields...")
                          ],
                        ),
                      )
                  );

                }


              },
              color: Colors.white,
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.check_circle,
                      color: Colors.black,
                    ),
                    SizedBox(
                      width: 4.0,
                    ),
                    Text(
                      AppText.SAVE[Globals.langSelected],
                      style: TextStyle(color: Colors.black),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _buildAddressAddPageBody() {
    return Theme(
      // Create a unique theme with "ThemeData"
        data: ThemeData(
          primaryColor: Colors.blue[400],
          accentColor: Colors.grey,
          hintColor: Colors.grey,
          cursorColor: Colors.blue[400],
        ),
        child: Padding(
          padding: EdgeInsets.only(top: 3),
          child: Card(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 2, horizontal: 5),
              child: ListView(
                children: <Widget>[
/*
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
*/
                  // Nama
                  TextField(
                    controller: _nama,
                    decoration: InputDecoration(
                      labelText: 'Nama Lengkap',
                      errorText: _fieldNamaIsEmpty ? 'Nama tidak boleh kosong' : null,
                    ),
                  ),
                  SizedBox(height: 5.0),

                  // Alamat
                  TextField(
                    controller: _alamat,
                    decoration: InputDecoration(
/*
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.deepPurple[400],
                      ),
                    ),
*/
                          labelText: 'Nama Jalan/Gedung dan Nomornya',
                          errorText: _fieldAlamatIsEmpty ? 'Alamat tidak boleh kosong/kurang dari 10 karakter' : null,
                      ),
                  ),

                  // Provinsi
                  SizedBox(height: 25.0),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) {
                              return AddressProvincePage();
                            },
                          )
                      );
                    },
                    child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            (Globals.provinceName != "")
                                ? Text(Globals.provinceName, style: TextStyle(color: Colors.black, fontSize: 16))
                                : (_fieldProvinceIsEmpty ? Text("Provinsi tidak boleh kosong", style: TextStyle(color: Colors.red, fontSize: 16)) : Text("Provinsi", style: TextStyle(color: Colors.grey, fontSize: 16))),
                            Icon(Icons.arrow_forward_ios,
                                color: Colors.grey[400]),
                          ],
                        )
                    ),
                  ),
                  SizedBox(height: 18.0),
                  new Container(
                    height: 1.8,
                    color: Colors.grey[400],
                  ),

                  // Kota/Kabupaten
                  SizedBox(height: 25.0),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) {
                              return AddressProvincePage();
                            },
                          )
                      );
                    },
                    child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            (Globals.cityName != "")
                                ? Text(Globals.cityName, style: TextStyle(color: Colors.black, fontSize: 16))
                                : (_fieldCityIsEmpty ? Text("Kota/Kabupaten tidak boleh kosong", style: TextStyle(color: Colors.red, fontSize: 16)) : Text("Kota/Kabupaten", style: TextStyle(color: Colors.grey, fontSize: 16))),
                            Icon(Icons.arrow_forward_ios,
                                color: Colors.grey[400]),
                          ],
                        )
                    ),
                  ),
                  SizedBox(height: 18.0),
                  new Container(
                    height: 1.8,
                    color: Colors.grey[400],
                  ),

                  // Kecamatan
                  SizedBox(height: 25.0),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) {
                              return AddressProvincePage();
                            },
                          )
                      );
                    },
                    child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            (Globals.subdistrictName != "")
                                ? Text(Globals.subdistrictName, style: TextStyle(color: Colors.black, fontSize: 16))
                                : (_fieldSubdistrictIsEmpty ? Text("Kecamatan tidak boleh kosong", style: TextStyle(color: Colors.red, fontSize: 16)) : Text("Kecamatan", style: TextStyle(color: Colors.grey, fontSize: 16))),
                            Icon(Icons.arrow_forward_ios,
                                color: Colors.grey[400]),
                          ],
                        )
                    ),
                  ),
                  SizedBox(height: 18.0),
                  new Container(
                    height: 1.8,
                    color: Colors.grey[400],
                  ),

                  // desa
                  TextField(
                    controller: _desa,
                    decoration: InputDecoration(
                      labelText: 'Desa',
                      //errorText: _fieldAlamatIsEmpty ? 'Alamat tidak boleh kosong/kurang dari 10 karakter' : null,
                    ),
                  ),

                  // kodepos
                  TextField(
                    controller: _kodepos,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: 'Kode Pos',
                      //errorText: _fieldAlamatIsEmpty ? 'Alamat tidak boleh kosong/kurang dari 10 karakter' : null,
                    ),
                  ),

                  // Telp
                  TextField(
                    controller: _telpon,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: 'Nomor Telepon',
                      errorText: _fieldTelponIsEmpty ? 'Telepon tidak boleh kosong' : null,
                    ),
                  ),
                  SizedBox(height: 20.0),

                  // default
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Switch(
                        value: _isDefault,
                        onChanged: _onSwitchChanged,
                        activeTrackColor: Colors.redAccent[100],
                        activeColor: Colors.red,
                      ),
                      Text("Jadikan alamat utama"),
                    ],
                  ),


                ],
              ),
            ),
          ),
        )
    );
  }
}

