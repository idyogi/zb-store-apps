import 'package:flutter/material.dart';
import 'package:zb_store/model/zone.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/scoped_model/address_scoped_model.dart';
import 'address_add_page.dart';

class AddressSubdistrictPage extends StatelessWidget {
  final String subdistrictID;
  AddressSubdistrictPage({this.subdistrictID});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      //key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0.5,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: new Text(
            "Kecamatan", style: TextStyle(color: Colors.black87)),
        iconTheme: new IconThemeData(color: Colors.black54),
      ),
      body: _buildAddressSubdistrictPageBody(),
    );
  }

  _buildAddressSubdistrictPageBody() {
    final AddressScopedModel addressModel = AddressScopedModel();
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
        child: FutureBuilder<List<Zone>>(
          future: addressModel.parseSubdistrict(Constants.API_GET_SUBDISTRICT, "?id=${Globals.cityID}"),
          //future: addressModel.parseSubdistrict(Constants.API_GET_SUBDISTRICT, "?id=419"),
          builder: (context, snapshot) {
            if (snapshot.hasError) print(snapshot.error);
            return snapshot.hasData
                ? _buildSubdistrictList(snapshot.data)
                : Center(child: CircularProgressIndicator());
          },
        )
    );
  }

  _buildSubdistrictList(List<Zone> subdistrict) {
    return new ListView.builder(
        itemCount: subdistrict.length,
        itemBuilder: (context, index) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              InkWell(
                onTap: () {
                  Globals.subdistrictID = subdistrict[index].id;
                  Globals.subdistrictName = subdistrict[index].name;
                  Navigator.of(context).pop();
                  //Navigator.popUntil(context, ModalRoute.withName(Constants.ROUTE_ADD_ADDRESS));
                  //Navigator.pushReplacementNamed(context, Constants.ROUTE_ADD_ADDRESS);
                  //Navigator.of(context).popUntil((route) => route.isFirst);
/*
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddressAddPage()
                      ),
                      //(Route<dynamic> route) => true
                      ModalRoute.withName(Constants.ROUTE_ADD_ADDRESS)
                  );
*/
                },
                child: ListTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(subdistrict[index].name),
                        //Icon(Icons.arrow_forward_ios, color: Colors.grey[400]),
                      ],
                    )
                ),
              ),
              Divider(),
            ],
          );
        }
    );
  }

}


