import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zb_store/pages/product_list_page.dart';
import 'package:zb_store/util/globals.dart';
import 'package:http/http.dart' as http;
import 'package:zb_store/util/languages.dart';
import 'dart:convert';
import 'package:zb_store/util/network.dart';
import 'dart:io';
import 'package:zb_store/widgets/home_drawer.dart';
import 'cart_list_page.dart';
import 'package:progress_indicators/progress_indicators.dart';

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    Network network = new Network();

/*
    Widget _buildImageTab(String title, String imageURL, String productURL) {
      return InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return ProductsListPage(id: title, url: productURL);
              },
            ),
          );
        },
        child: Container( //My container or any other widget
          height: double.infinity,
          width: double.infinity,
          decoration:new BoxDecoration(
            image: new DecorationImage(
              image: new NetworkImage(imageURL),
              fit: BoxFit.cover,
            ),
          ),
        ),
      );
    }
*/
    _checkIfAlreadyLogin() async {
      final prefs = await SharedPreferences.getInstance();
      bool _isGoogleLoggedIn = prefs.getBool(Constants.IS_GOOGLE_LOGIN) ?? false;
      bool _isFacebookLoggedIn = prefs.getBool(Constants.IS_FACEBOOK_LOGIN) ?? false;
      Globals.alreadyLogin = _isGoogleLoggedIn || _isFacebookLoggedIn;
    }

    Future<dynamic> _getAPI() async {

      var response = await http.get(
        Constants.API_HOME_PAGE_NEW,
        headers: {},
      ).catchError((error) {
        print(error);
      },
      );

      //print("landing json: ${response.body}");
      return json.decode(response.body);
    }

    Future<List<dynamic>> _getAllAPI() async {
      List<dynamic> result = List();

      // home landing
      await http.get(
        Constants.API_HOME_PAGE_NEW,
        headers: {},
      ).then((response) {
        result.add(json.decode(response.body));
      });

      // brand 1
      await http.get(
        Constants.API_BRAND_PAGE+"1",
        headers: {},
      ).then((response) {
        result.add(json.decode(response.body));
      });

      // brand 2
      await http.get(
        Constants.API_BRAND_PAGE+"2",
        headers: {},
      ).then((response) {
        result.add(json.decode(response.body));
      });

      // brand 3
      await http.get(
        Constants.API_BRAND_PAGE+"3",
        headers: {},
      ).then((response) {
        result.add(json.decode(response.body));
      });

      // cek login
      await _checkIfAlreadyLogin();

      return result;
    }

    Future<bool> _onWillPop() {
      return showDialog(
        context: context,
        builder: (context) => new AlertDialog(
          title: new Text(AppText.POPUP_EXIT[Globals.langSelected]),
          //content: new Text('Anda bisa kembali mengakses informasi ini pada menu utama: Pesanan Saya'),
          actions: <Widget>[
            new FlatButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: new Text(AppText.POPUP_NO[Globals.langSelected]),
            ),
            new FlatButton(
              child: new Text(AppText.POPUP_YES[Globals.langSelected]),
              onPressed: () => exit(0),
            ),
          ],
        ),
      ) ?? false;
    }

    Widget _buildImageSection(int brandID, String imageURL, String productURL, String featureURL) {

      var backgroundColor1 = Color(0xFF444152);
      var backgroundColor2 = Color(0xFF6f6c7d);

      double imageWidth = MediaQuery.of(context).size.width / 3;
      return InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return ProductsListPage(brandID: brandID, urlProducts: productURL, jsonFeatures: featureURL);
              },
            ),
          );
        },
        child: Stack(
          children: <Widget>[

            // image splash
            Container( //My container or any other widget
              height: double.infinity,
              width: imageWidth,
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                  begin: Alignment.centerLeft,
                  end: new Alignment(10.0, 0.0), // 10% of the width, so there are ten blinds.
                  colors: [backgroundColor1, backgroundColor2], // whitish to gray
                  tileMode: TileMode.repeated, // repeats the gradient over the canvas
                ),
              ),
/*
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Center(
                    child: Container(
                      height: 100.0,
                      width: 100.0,
                      child: (brandID == 1)
                        ? Image.asset('assets/images/logo_zb_10.png')
                        : (brandID == 2)
                          ? Image.asset('assets/images/logo_zb_20.png')
                          : Image.asset('assets/images/logo_zb_30.png')
                    )
                  ),
                ],
              )
*/
            ),

            // image api
            Container( //My container or any other widget
              height: double.infinity,
              width: imageWidth,
              decoration:new BoxDecoration(
                image: new DecorationImage(
                  image: new NetworkImage(imageURL),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ],
        ),

      );
    }

    Widget _buildImageList(dynamic data) {
      double imageHeight = MediaQuery.of(context).size.height / 3;
      var backgroundColor1 = Color(0xFF444152);
      var backgroundColor2 = Color(0xFF6f6c7d);

      return /*Stack(
        children: <Widget>[
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: new BoxDecoration(
              gradient: new LinearGradient(
                begin: Alignment.centerLeft,
                end: new Alignment(10.0, 0.0), // 10% of the width, so there are ten blinds.
                colors: [backgroundColor1, backgroundColor2], // whitish to gray
                tileMode: TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
          ),*/

          ListView.builder(
              itemCount: data["brands"].length,
              itemBuilder: (context, index) {
                return Container(
                  height: imageHeight,
                  width: double.infinity,
                  decoration:new BoxDecoration(
                    image: new DecorationImage(
                      image: new NetworkImage(data["brands"][index]["image_url"]),
                      fit: BoxFit.cover,
                    ),
                  ),
                );
              }
          );

        //],
      //);
    }

    _buildSplashScreen() {
      var backgroundColor1 = Color(0xFF444152);
      var backgroundColor2 = Color(0xFF6f6c7d);

      return Stack(
        children: <Widget>[
          Container(
            height: double.infinity,
            width: double.infinity,
            color: Colors.black,
/*
            decoration: new BoxDecoration(
              gradient: new LinearGradient(
                begin: Alignment.centerLeft,
                end: new Alignment(10.0, 0.0), // 10% of the width, so there are ten blinds.
                colors: [backgroundColor1, backgroundColor2], // whitish to gray
                tileMode: TileMode.repeated, // repeats the gradient over the canvas
              ),
            ),
*/
          ),

          Center(
            child: GlowingProgressIndicator(
              duration: Duration(milliseconds: 400),
              child: Container(
                height: 100.0,
                width: 100.0,
                child: Image.asset('assets/images/logo_zb_10.png', color: Colors.grey)
              )
            ),
          ),


          Container(
            height: double.infinity,
            width: double.infinity,
          ),
        ],
      );
    }

    Widget _buildImageStaticView(List<dynamic> data) {
      double imageHeight = MediaQuery.of(context).size.height / 3;

      List<Widget> brands = List();
      for (int i = 0; i < data[0]["brands"].length; i++) {
        brands.add(
          Container(
            height: imageHeight,
            width: double.infinity,
/*
            decoration:new BoxDecoration(
              image: new DecorationImage(
                image: new NetworkImage(data[0]["brands"][i]["image_url"]),
                fit: BoxFit.cover,
              ),
            ),
*/
            child: InkWell(
              onTap: (){
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return ProductsListPage(
                          brandID: data[0]["brands"][i]["id"],
                          urlProducts: data[0]["brands"][i]["url"],
                          jsonFeatures: data,
                      );
                    },
                  ),
                );

              },
            ),
          )
        );
      }

      return Column(
        children: brands,
      );
    }

    // check timeout:
    //print("Globals.timerIsInactive = " + Globals.timerIsInactive.toString());
    //print("Globals.timerPopupIsAlreadyShowing = " + Globals.timerPopupIsAlreadyShowing.toString());

    return /*MaterialApp(
      //initialRoute: Constants.ROUTE_HOME_PAGE,
      //routes: Routes.routes,
      home:*/ DefaultTabController(
        length: 3,
        child: WillPopScope(
          onWillPop: _onWillPop,
          child: FutureBuilder<dynamic> (
              future: _getAllAPI(), //_getAPI(),
              builder: (context, snapshot) {
                return (snapshot.hasData)
                  ? Scaffold(
                    body: Stack(
                      children: <Widget>[

/*
                        Container(
                          height: double.infinity,
                          width: double.infinity,
                          color: Colors.black,
                        ),

                        Center(
                          child: Container(
                            height: 100.0,
                            width: 100.0,
                            child: Image.asset('assets/images/logo_zb_10.png')
                          )
                        ),
*/

                        _buildSplashScreen(),

                        Container(
                          height: double.infinity,
                          width: double.infinity,
                          decoration:new BoxDecoration(
                            image: new DecorationImage(
                              image: new NetworkImage(snapshot.data[0]["all_brands"]),
                              fit: BoxFit.fitHeight
                            ),
                          ),
                        ),


                        // 1-- all image taruh sini
                        // 2-- fungsi di bawah tanpa image
                        _buildImageStaticView(snapshot.data),
          /*
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  // ZB
                                  _buildImageSection(
                                      snapshot.data["brands"][0]["id"],
                                      snapshot.data["brands"][0]["image_url"],
                                      snapshot.data["brands"][0]["url"],
                                      snapshot.data["brands"][0]["url_brand"]),
                                  // ZB Lux
                                  _buildImageSection(
                                      snapshot.data["brands"][1]["id"],
                                      snapshot.data["brands"][1]["image_url"],
                                      snapshot.data["brands"][1]["url"],
                                      snapshot.data["brands"][1]["url_brand"]),
                                  // ZB Teen
                                  _buildImageSection(
                                      snapshot.data["brands"][2]["id"],
                                      snapshot.data["brands"][2]["image_url"],
                                      snapshot.data["brands"][2]["url"],
                                      snapshot.data["brands"][2]["url_brand"]),
                                ],
                              ),
          */
          /*
                          TabBarView(
                            children: [
                              _buildImageTab(
                                  snapshot.data["brands"][0]["name"],
                                  snapshot.data["brands"][0]["image_url"],
                                  snapshot.data["brands"][0]["url"]),
                              _buildImageTab(
                                  snapshot.data["brands"][1]["name"],
                                  snapshot.data["brands"][1]["image_url"],
                                  snapshot.data["brands"][1]["url"]),
                              _buildImageTab(
                                  snapshot.data["brands"][2]["name"],
                                  snapshot.data["brands"][2]["image_url"],
                                  snapshot.data["brands"][2]["url"]),
                            ],
                          ),
          */
                        new Positioned( //Place it at the top, and not use the entire screen
                          top: 0.0,
                          left: 0.0,
                          right: 0.0,
                          child: AppBar(
                            //title: new Text("ZB Store", style: TextStyle(color: Colors.white)),
                            backgroundColor: Colors.transparent,
                            elevation: 0.0,
                            actions: <Widget>[

                              // Using Stack to show Notification Badge
                              InkWell(
                                onTap: () {
                                  //Navigator.pushNamed(context, Constants.ROUTE_CART_LIST);
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) {
                                        return CartListPage();
                                      },
                                    ),
                                  );

                                },
                                child: Stack(
                                  children: <Widget>[
                                    new IconButton(
                                      icon: Icon(Icons.shopping_cart, color: Colors.white),
                                    ),

                                    FutureBuilder<int>(
                                        future: network.getNotificationBadge(),
                                        builder: (context, snapshot) {
                                          return (snapshot.hasData && snapshot.data != 0)
                                              ? new Positioned(
                                            right: 6,
                                            top: 5,
                                            child: new Container(
                                              padding: EdgeInsets.all(2),
                                              decoration: new BoxDecoration(
                                                color: Colors.red,
                                                borderRadius: BorderRadius.circular(6),
                                              ),
                                              constraints: BoxConstraints(
                                                minWidth: 14,
                                                minHeight: 14,
                                              ),
                                              child: Text(
                                                snapshot.data.toString(),
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 8,
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          )
                                          : new Container();
                                        }
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          iconTheme: new IconThemeData(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  drawer: HomeDrawer(jsonFeatures: snapshot.data),
                )
              : Container(
                  child: _buildSplashScreen()
              );
            }
          ),
        ),
      );
    //);
  }
}


/*
class HomeBody extends StatelessWidget {
  const HomeBody({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return FutureBuilder<dynamic> (
          future: getAPI(),
          builder: (context, snapshot) {
            return (snapshot.hasData) ?

            ListView(
              children: <Widget>[

                SizedBox(
                    height: 210.0,
                    width: MediaQuery.of(context).size.width,
                    child: new Carousel(
                      images: [
                        new NetworkImage(snapshot.data["slide"][0]["image_url"]),
                        new NetworkImage(snapshot.data["slide"][1]["image_url"]),
                        new NetworkImage(snapshot.data["slide"][2]["image_url"]),
                        new NetworkImage(snapshot.data["slide"][3]["image_url"]),
                      ],
                      showIndicator: false,
                    )
                ),

                // -----------
                //   Brand 1
                // -----------
                SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: Image.network(snapshot.data["brands"][0]["image_url"])
                ),

                // -----------
                //   Brand 2
                // -----------
                SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: Image.network(snapshot.data["brands"][1]["image_url"])
                ),

                // -----------
                //   Brand 3
                // -----------
                SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: Image.network(snapshot.data["brands"][2]["image_url"])
                ),

                // -----------
                //   Footer
                // -----------
              ],
            )

                :

            new Center(
              child: CircularProgressIndicator(),
            );

          }
      );
  }
}
*/
