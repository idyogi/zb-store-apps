import 'package:flutter/material.dart';
import 'package:zb_store/model/orders.dart';

class OrderDetailPage extends StatelessWidget {
  final Orders orders;

  OrderDetailPage({
    @required this.orders,
  });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 1,
        backgroundColor: Colors.white,
        title: new Text("Detil Order", style: TextStyle(color: Colors.black87)),
        iconTheme: new IconThemeData(color: Colors.black54),
      ),
      body: Padding(
        padding: EdgeInsets.all(5.0),
        child: ListView(
          children: <Widget>[

            Text("Order ID: " + orders.orderID),
            SizedBox(height: 3.0),

            Text("Barang: " + orders.cartSumary),
            SizedBox(height: 3.0),

            Text("Payment type: " + orders.paymentType),
            SizedBox(height: 3.0),

            (orders.vaNumber != null)
            ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("VA/Code Number: " + orders.vaNumber),
                SizedBox(height: 3.0),

              ],
            )
            : Container(),

            Text("Harga total: " + orders.grossAmount),
            SizedBox(height: 3.0),

            Divider(),

            Text("Kurir: " + orders.shippingCourier),
            SizedBox(height: 3.0),

            Text("Biaya Kurir: " + orders.shippingCost),
            SizedBox(height: 3.0),

            Text("Jenis Kurir: " + orders.shippingService),
            SizedBox(height: 3.0),

            Text("Nama Penerima: " + orders.shipFirstName),
            SizedBox(height: 3.0),

            Text("Telpon Penerima: " + orders.shipPhone),
            SizedBox(height: 3.0),

            Text("Alamat Penerima: "),
            SizedBox(height: 3.0),

            Text(orders.shipAddress),
            SizedBox(height: 3.0),

            Text(orders.shipSubdistrict + " - " + orders.shipCity),
            SizedBox(height: 3.0),

            Text(orders.shipProvince),
            SizedBox(height: 3.0),

            Divider(),

            Text("Tracking shipment: "),
          ],
        ),
      ),
    );
  }

}