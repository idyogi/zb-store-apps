import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:skeleton_text/skeleton_text.dart';
import 'package:zb_store/pages/cart_list_page.dart';
import 'package:zb_store/scoped_model/product_scoped_model.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/util/languages.dart';
import 'package:zb_store/widgets/products_list_item.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:zb_store/util/skeleton.dart';
import 'package:zb_store/util/network.dart';
import 'package:zb_store/widgets/home_drawer.dart';
import 'package:http/http.dart' as http;
import 'package:carousel_slider/carousel_slider.dart';
import 'product_single_page.dart';
import 'product_category_page.dart';

class Global {
  static int brandID;
  static String urlProducts = "";
  static dynamic jsonFeatures;
}

class ProductsListPage extends StatelessWidget {
  //final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final int brandID;
  final String urlProducts;
  final dynamic jsonFeatures;
  const ProductsListPage({Key key, @required this.brandID,
    @required this.urlProducts, @required this.jsonFeatures})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("brandID: $brandID");
    print("urlProducts: $urlProducts");
    print("urlFeatures: $jsonFeatures");
    Global.brandID = brandID;
    Global.urlProducts = urlProducts;
    Global.jsonFeatures = jsonFeatures;
    Globals.brandID = brandID;
    Network network = new Network();
    ProductsScopedModel productModel = ProductsScopedModel();
    productModel.parseProductsFromResponse(Global.urlProducts, 1);

    String brandName;
    if (brandID == 1) {
      brandName = 'assets/images/logo_zb_1.png';
    } else if (brandID == 2) {
      brandName = 'assets/images/logo_zb_2.png';
    } else if (brandID == 3) {
      brandName = 'assets/images/logo_zb_3.png';
    }

    //print("total cart = " + globals.totalCartItem.toString());
    return new ScopedModel<ProductsScopedModel>(
      model: productModel,
      child: Scaffold(
        //key: _scaffoldKey,
        appBar: AppBar(
          elevation: 0,
          centerTitle: true,
          backgroundColor: Colors.white,
          //title: new Text(title, style: TextStyle(color: Colors.black)),
          title: new Image.asset(brandName, width: 120.0, height: 40.0,),
          actions: <Widget>[
/*
            IconButton(
              icon: Icon(Icons.shopping_cart, color: Colors.black),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return CartListPage();
                    },
                  ),
                );
              },
            ),
*/
            // Using Stack to show Notification Badge
            InkWell(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return CartListPage();
                    },
                  ),
                );
                //Navigator.pushNamed(context, Constants.ROUTE_CART_LIST);

              },
              child: Stack(
                children: <Widget>[
                  new IconButton(
                    icon: Icon(Icons.shopping_cart, color: Colors.black),
                  ),

                  FutureBuilder<int>(
                    future: network.getNotificationBadge(),
                    builder: (context, snapshot) {
                      return (snapshot.hasData && snapshot.data != 0)
                        ? new Positioned(
                        right: 6,
                        top: 5,
                        child: new Container(
                          padding: EdgeInsets.all(2),
                          decoration: new BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(6),
                          ),
                          constraints: BoxConstraints(
                            minWidth: 14,
                            minHeight: 14,
                          ),
                          child: Text(
                            snapshot.data.toString(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 8,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )
                      : new Container();
                    }
                  ),
                ],
              ),
            ),

          ],
          iconTheme: new IconThemeData(color: Colors.black),
        ),
        body: ProductsListPageBody(),
        drawer: HomeDrawer(jsonFeatures: Global.jsonFeatures),

      ),
    );
  }
}

class ProductsListPageBody extends StatefulWidget {
  //final String url;
  //const ProductsListPageBody({Key key, @required this.url}) : super(key: key);
  @override
  ProductsListPageBodyState createState() {
    return new ProductsListPageBodyState();
  }
}

class ProductsListPageBodyState extends State<ProductsListPageBody> {
  int pageIndex = 1;
  int currentPage = 0;
  PageController controller;

  @override
  Widget build(BuildContext context) {

    return ScopedModelDescendant<ProductsScopedModel>(
      builder: (context, child, model) {
        return model.isLoading
          ? _buildSplashScreen()
          : _buildListView(context, model);
      },
    );
  }

  _buildSplashScreen() {

    return Stack(
      children: <Widget>[
        Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
        ),

        Center(
          child: GlowingProgressIndicator(
              duration: Duration(milliseconds: 400),
              child: Container(
                height: 70.0,
                width: 70.0,
                child: (Globals.brandID == 1)
                    ? Image.asset('assets/images/logo_zb_10.png', color: Colors.black54)
                    : (Globals.brandID == 2)
                    ? Image.asset('assets/images/logo_zb_20.png', color: Colors.black54)
                    : Image.asset('assets/images/logo_zb_30.png', color: Colors.black54),
              )
          ),
        ),

/*
          Container(
            height: double.infinity,
            width: double.infinity,
          ),
*/
      ],
    );
  }

  _buildAlertTimeout() {
    showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) =>
          CupertinoActionSheet(
            title: Text(AppText.POPUP_TIMEOUT_TITLE[Globals.langSelected]),
            message: Text(AppText.POPUP_TIMEOUT_MESSAGE[Globals.langSelected]),
            actions: <Widget>[

              CupertinoActionSheetAction(
                  child: Text(AppText.POPUP_YES[Globals.langSelected]),
                  onPressed: () {
                    //model.updateIsCut(cartItem, 0);
                    Navigator.pop(context);
                  }
              ),
              CupertinoActionSheetAction(
                  child: Text(AppText.POPUP_NO[Globals.langSelected]),
                  onPressed: () {
                    //model.updateIsCut(cartItem, 1);
                    Navigator.pop(context);
                    //model.setTimeoutPopup(false);
                  }
              ),
            ],
          ),
    );
  }

  _buildCircularProgressIndicator() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  _buildListView(BuildContext context, ProductsScopedModel model) {

    //model.setTimeoutPopup(true);
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
      child:  model.getProductsCount() == 0 // model.displayTimeoutPopup()
        //? _buildAlertTimeout()
        ? Center(child: Text("No products available.")) // nitip notif timeout
        : ListView.builder(
        physics: const AlwaysScrollableScrollPhysics(),
        itemCount: model.getProductsCount() + 1,
        itemBuilder: (context, index) {
          //print("ListView.builder index=$index");
          //print("ListView.builder model.getProductsCount()="+model.getProductsCount().toString());
          // debug
          //return Container();

          if (index == model.getProductsCount() && !model.produkHabis) {
            if (model.hasMoreProducts) {
              //print("ListView.builder hasMoreProducts index=$index");
              //print("ListView.builder hasMoreProducts pageIndex=$pageIndex");
              //print("ListView.builder hasMoreProducts call API ");
              pageIndex++;
              model.parseProductsFromResponse(Global.urlProducts, pageIndex);
              return Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Container(),
                //child: Center(child: CircularProgressIndicator()),
              );
            }
            return Container();
          }
          // debug
          //return Container();

          else if (index == 0) {
            //0th index would contain filter icons
            return _buildTopWidgets(Global.jsonFeatures); //"http://beta.devsandbox.me/api/brand/1"
          } else if (index % 2 == 0) {
            //2nd, 4th, 6th.. index would contain nothing since this would
            //be handled by the odd indexes where the row contains 2 items
            return Container();
          } else {
            // jika jumlah data ganjil tampilkan single produk
            if (index % 2 == 1 && model.getProductsCount() == index) {
              return ProductsListItem(
                product1: model.productsList[index - 1],
                product2: null,
              );
            }

            //1st, 3rd, 5th.. index would contain a row containing 2 products
            if (index > model.getProductsCount() - 1) {
              return Container();
            }

            return ProductsListItem(
              product1: model.productsList[index - 1],
              product2: model.productsList[index],
            );
          }

        },
      ),
    );
  }

  Future<dynamic> _getAPI(String url) async {

    var response = await http.get(
      url,
      headers: {},
    ).catchError((error) {
      print(error);
    },
    );

    //print("landing json: ${response.body}");
    return json.decode(response.body);
  }

  _buildTopWidgets(dynamic jsonBrand) {
    return /*FutureBuilder<dynamic> (
        future: _getAPI(jsonBrand),
        builder: (context, snapshot) {
          return (snapshot.hasData)
            ?*/ Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  // circle category
                  _buildCategoryWidgets(jsonBrand[Global.brandID]["category"]),
                  // slider product
                  _buildSliderWidgets(jsonBrand[Global.brandID]["slider"]),
                  // single products
                  //_buildSingleWidgets(snapshot.data["bestbuy"]),
                ],
              );
            //: Center(child: CircularProgressIndicator());
        //}
    //);
  }

  _buildSingleWidgets(dynamic json) {
    List<Widget> products = List();
    for (int i = 0; i < json.length; i++) {
      products.add(
        Padding(
          padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
          child: InkWell(
            onTap: () {
              //print("url: " + json[i]["url"].toString());
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return ProductSinglePage(productURL: json[i]["url"].toString());
                  },
                ),
              );
            },
            child: Image.network(json[i]["image"].toString())
          )
        )
      );
    }
    return Container(
      //margin: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 5.0),
      width: MediaQuery.of(context).size.width,
      //height: 100.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: products,
      ),
    );
  }

  _buildSliderWidgets(dynamic json) {

    List<String> imageSlider = List();
    for (int i = 0; i < json.length; i++) {
      imageSlider.add(json[i]["image"].toString() +
          "|" + json[i]["url"].toString() + "|" + json[i]["title"].toString()
          + "|" + json[i]["subtitle"].toString());
    }

    return Container(
        margin: const EdgeInsets.only(bottom: 2.0),
        width: MediaQuery.of(context).size.width,
        child: CarouselSlider(
        autoPlay: true,
        aspectRatio: 1/1,
        //height: double.infinity,
        viewportFraction: 1.0,
        autoPlayInterval: Duration(seconds: 3),
        pauseAutoPlayOnTouch: Duration(seconds: 10),
        items: imageSlider.map((item) {
          var itemSplit = item.split("|");
          return Builder(
            builder: (BuildContext context) {
              return Container(
                width: MediaQuery.of(context).size.width,
                //margin: EdgeInsets.symmetric(horizontal: 5.0),
                child: InkWell(
                  onTap: () {
                    //print("URL: ${itemSplit[1]}");
                    if (itemSplit[1] != "") {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return ProductSinglePage(productURL: itemSplit[1]);
                          },
                        ),
                      );
                    }


                  },
                  child: Stack(
                    children: <Widget>[
/*
                      Container(
                        height: MediaQuery.of(context).size.width,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.grey[200],
                        child: Image.network(itemSplit[0]),
                      ),
*/
                      SkeletonAnimation(
                        child: Container(
                          height: MediaQuery.of(context).size.width,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.grey[200],
                        ),
                      ),

                      //Image.network(itemSplit[0]),
                      Container(
                        child: Image.network(
                          itemSplit[0],
                          fit: BoxFit.cover,
                        ),
                        height: MediaQuery.of(context).size.width,
                        width: MediaQuery.of(context).size.width,
                      ),


                      Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(itemSplit[2], style: TextStyle(fontSize: 22.0, color: Colors.white, fontWeight: FontWeight.bold)),
                              Text(itemSplit[3], style: TextStyle(fontSize: 15.0, color: Colors.white, fontWeight: FontWeight.bold)),
                            ],
                          ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        }).toList(),
      )
/*
      PageView.builder(
          onPageChanged: (value) {
            setState(() {
              currentPage = value;
            });
          },
          scrollDirection: Axis.horizontal,
          controller: controller,
          itemCount: json.length,
          itemBuilder: (context, index) {

            return InkWell(
              onTap: () {
              },
              child: Image.network(json[index]["image"].toString()),
            );
          }
      ),
*/
    );

  }

  _buildCategoryWidgets(dynamic json) {

    return Container(
      margin: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 0.0),
      width: MediaQuery.of(context).size.width,
      height: 110.0,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: json.length,
        itemBuilder: (context, index) {
          return Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(width: 15.0),
              InkWell(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) {
                        return ProductsCategoryPage(
                            id: Global.brandID,
                            urlCategory: json[index]["url"].toString(),
                            name: json[index]["name"].toString(),
                        );
                      },
                    ),
                  );

                },
                child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[

                      Stack(
                        children: <Widget>[

                          SkeletonAnimation(
                            child: Container(
                              width: 80.0,
                              height: 80.0,
                              color: Colors.grey[200],
                            ),
                          ),
                          new Container(
                            width: 80.0,
                            height: 80.0,
                            decoration: new BoxDecoration(
                              //color: Colors.grey[200],
                              shape: BoxShape.rectangle,
                              image: new DecorationImage(
                                  fit: BoxFit.cover,
                                  image: new NetworkImage(json[index]["image"].toString())
                              )
                            )
                          ),

                        ],
                      ),

                      SizedBox(height: 5.0),
                      new Text(json[index]["name"].toString(), style: TextStyle(fontSize: 12.0)),
                    ],
                  ),
              ),
            ],
          );
        }
      ),
/*
      ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          _buildCategoryList(json),
        ],
      ),
*/
    );
  }

  _buildSliderWidgets2(Size screenSize, dynamic json) {
/*
    var scrollController = ScrollController();
    void scrollAfter(ScrollController scrollController, {int seconds}) {
      Future.delayed(Duration(seconds: seconds), () {
        var offset = screenSize.width;
        var scrollDuration = Duration(seconds: 2);
        scrollController.animateTo(offset,
            duration: scrollDuration, curve: Curves.ease);
      });
    }

    scrollAfter(scrollController, seconds: 2);

    controller: fixedExtentScrollController,
    physics: FixedExtentScrollPhysics(),
    itemExtent: screenSize.width,

    FixedExtentScrollController fixedExtentScrollController = new FixedExtentScrollController();
*/
    return Container(
      margin: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 0.0),
      width: screenSize.width,
      height: 360.0,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          //controller: scrollController,
          itemCount: json.length,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
              },
              child: Image.network(json[index]["image"].toString()),
            );
          }
      ),
    );

  }

  _buildCategoryList(dynamic json) {
    List<Widget> _categories = List();

    _buildCircleWidget(String title, String link) {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(width: 15.0),
          InkWell(
            onTap: () {
            },
            child: new Center(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                        width: 80.0,
                        height: 80.0,
                        decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                                fit: BoxFit.cover,
                                image: new NetworkImage(link)
                            )
                        )),
                    SizedBox(height: 5.0),
                    new Text(title, style: TextStyle(fontSize: 12.0)),
                  ],
                )),
          ),
        ],
      );
    }

    for(int i = 0; i < json.length; i++) {
      _categories.add(_buildCircleWidget(json[i]["name"].toString(), json[i]["image"].toString()));
    }

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: _categories
    );
  }

  _buildFilterWidgets(Size screenSize) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      width: screenSize.width,
      child: Card(
        elevation: 4.0,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              _buildFilterButton("SORT"),
              Container(
                color: Colors.black,
                width: 2.0,
                height: 24.0,
              ),
              _buildFilterButton("REFINE"),
            ],
          ),
        ),
      ),
    );
  }

  _buildFilterButton(String title) {
    return InkWell(
      onTap: () {
        print(title);
      },
      child: Row(
        children: <Widget>[
          Icon(
            Icons.arrow_drop_down,
            color: Colors.black,
          ),
          SizedBox(
            width: 2.0,
          ),
          Text(title),
        ],
      ),
    );
  }
}

