import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:zb_store/model/address.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:zb_store/scoped_model/address_scoped_model.dart';
import 'package:zb_store/util/languages.dart';
import 'address_add_page.dart';
import 'package:zb_store/util/globals.dart';
import 'address_edit_page.dart';

class AddressListPage extends StatefulWidget {
  @override
  AddressListPageState createState() {
    return new AddressListPageState();
  }
}

class AddressListPageState extends State<AddressListPage> {
  final AddressScopedModel addressModel = AddressScopedModel();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new ScopedModel<AddressScopedModel>(
      model: addressModel,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: 1,
          centerTitle: true,
          backgroundColor: Colors.white,
          title: new Text(
              AppText.ADDRESS_LIST[Globals.langSelected], style: TextStyle(color: Colors.black87)),
          iconTheme: new IconThemeData(color: Colors.black54),
        ),
        body: _addressListPageBody(),
        bottomNavigationBar: _buildBottomNavigationBar(context),
      ),
    );
  }

  _addressListPageBody() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
      child: ScopedModelDescendant<AddressScopedModel>(
        builder: (context, child, model) {
          return FutureBuilder<List<Address>>(
            future: model.parseAddress(),
            builder: (context, snapshot) {
              if (snapshot.hasError) print(snapshot.error);

              return snapshot.hasData
              //? PhotosList(photos: snapshot.data)
                  ? (snapshot.data.length > 0 ? _addressList(snapshot.data) : Center(child: Text(AppText.CHECKOUT_ADDRESS_EMPTY[Globals.langSelected])))
                  : _buildSplashScreen();
            },
          );
        }
      )
    );
  }

  _buildSplashScreen() {

    return Stack(
      children: <Widget>[
        Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
        ),

        Center(
          child: GlowingProgressIndicator(
              duration: Duration(milliseconds: 400),
              child: Container(
                height: 70.0,
                width: 70.0,
                child: (Globals.brandID == 1)
                    ? Image.asset('assets/images/logo_zb_10.png', color: Colors.black54)
                    : (Globals.brandID == 2)
                    ? Image.asset('assets/images/logo_zb_20.png', color: Colors.black54)
                    : Image.asset('assets/images/logo_zb_30.png', color: Colors.black54),
              )
          ),
        ),

/*
          Container(
            height: double.infinity,
            width: double.infinity,
          ),
*/
      ],
    );
  }


  _addressList(List<Address> address) {
    return new ListView.builder(
      itemCount: address.length,
      itemBuilder: (context, index) {
        //print("address.length:" + address.length.toString());
        //print("address:" + address.toString());
        return ScopedModelDescendant<AddressScopedModel>(
          builder: (context, child, model) {
            return InkWell(
                onTap: () {
                  // change checkout address

/*
                  _scaffoldKey.currentState.showSnackBar(
                      new SnackBar(
                        duration: new Duration(seconds: 2),
                        content: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            FutureBuilder<dynamic>(
                                future: model.updateCheckoutAddress(address[index].id.toString()),
                                builder: (context, snapshot) {
                                  return (snapshot.hasData)
                                      ? Text(snapshot.data['message'])
                                      : CircularProgressIndicator();
                                }
                            ),
                          ],
                        ),
                      )
                  );
*/

                  _scaffoldKey.currentState.showSnackBar(
                      new SnackBar(
                        duration: new Duration(minutes: 2),
                        content: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new CircularProgressIndicator(),
                            new Text("  Updating...")
                          ],
                        ),
                      )
                  );

                  model.updateCheckoutAddress(address[index].id.toString()).
                    then((response) {
                      _scaffoldKey.currentState.hideCurrentSnackBar();
                      Navigator.of(context).pop();
                  });

                },
                child: Card(
                    child: ListTile(
                        //leading: IconButton(icon: Icon(Icons.check_circle, color: Colors.grey)),
                        title: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Flexible(
                              child: Text(address[index].name),
                            ),
                            SizedBox(width: 2.0),
/*
                            (address[index].isDefault == 1)
                              ? IconButton(
                                  icon: Icon(Icons.check_circle, color: Colors.red, size: 17.0),
                                )
                               : Container(),
*/
                            Text((address[index].isDefault == 1) ? "*" : "", style: TextStyle(color: Colors.redAccent, fontSize: 25))
                          ],
                        ),
                        subtitle: Text(
                                address[index].address + "\n" +
                                "Desa: " + address[index].village + "\n" +
                                address[index].subdistrictName + " - " + address[index].cityName + "\n" +
                                address[index].provinceName  + "\n" +
                                "Kode Pos: " + address[index].postalCode + "\n" +
                                "Phone: " + address[index].phone
                        ),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            // edit
                            new IconButton(
                              icon: Icon(Icons.edit, color: Colors.grey),
                              onPressed: () {
                                // reset address
                                Globals.provinceID = "";
                                Globals.provinceName = "";
                                Globals.cityID = "";
                                Globals.cityName = "";
                                Globals.subdistrictID = "";
                                Globals.subdistrictName = "";
                                Globals.postalCode = "";

                                Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) {
                                        return AddressEditPage(address: address[index]);
                                      },
                                    )
                                );
                              },
                            ),
                            // delete
                            new IconButton(
                              icon: Icon(Icons.delete, color: Colors.grey),
                              onPressed: () {
                                _scaffoldKey.currentState.showSnackBar(
                                    new SnackBar(
                                      duration: new Duration(seconds: 2),
                                      content: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          FutureBuilder<dynamic>(
                                              future: model.removeAddress(address[index].id.toString()),
                                              builder: (context, snapshot) {
                                                return (snapshot.hasData)
                                                    ? Text(snapshot.data['message'])
                                                    : CircularProgressIndicator();
                                              }
                                          ),
                                        ],
                                      ),
                                    )
                                );
                                addressModel.removeFromAddressList(address[index].id);
                              },
                            ),
                          ],
                        )
                    )
                )
            );
          }
        );
      }
      );
    }
  }

_buildBottomNavigationBar(BuildContext context) {
  return Container(
    width: MediaQuery.of(context).size.width,
    height: 50.0,
    child: Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        // add address
        Flexible(
          flex: 1,
          child: RaisedButton(
            onPressed: () {
              // reset address
              Globals.provinceID = "";
              Globals.provinceName = "";
              Globals.cityID = "";
              Globals.cityName = "";
              Globals.subdistrictID = "";
              Globals.subdistrictName = "";
              Globals.village = "";
              Globals.postalCode = "";

              Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return AddressAddPage();
                    },
                  )
              );
              //Route route = MaterialPageRoute(builder: (context) => AddressAddPage());
              //Navigator.pushNamed(context, Constants.ROUTE_ADD_ADDRESS);
            },
            color: Colors.white,
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.add_circle,
                    color: Colors.black,
                  ),
                  SizedBox(
                    width: 4.0,
                  ),
                  Text(
                    AppText.CHECKOUT_ADDRESS_ADD[Globals.langSelected],
                    style: TextStyle(color: Colors.black),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    ),
  );
}


