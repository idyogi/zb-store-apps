import 'package:flutter/material.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/util/languages.dart';

class ProductSizePage extends StatelessWidget {
  final String imageURL;
  ProductSizePage({this.imageURL});

  @override
  Widget build(BuildContext context) {
    print("ProductSizePage imageURL: $imageURL");
    return Scaffold(
      //key: _scaffoldKey,
      appBar: AppBar(
        elevation: 1.0,
        leading: new IconButton(
          icon: new Icon(Icons.close, color: Colors.black54),
          onPressed: ()=>Navigator.of(context).pop(),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: new Text(
            AppText.PRODUCT_SIZE_DETAILS[Globals.langSelected], style: TextStyle(color: Colors.black87)),
        iconTheme: new IconThemeData(color: Colors.black54),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
        child: ListView(
          children: <Widget>[
            (imageURL != null )
            ? Image.network(imageURL)
            : Text("Tidak tersedia"),
          ],
        ),
      ),
      //bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

}