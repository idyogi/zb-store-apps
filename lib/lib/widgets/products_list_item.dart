import 'package:flutter/material.dart';
import 'package:zb_store/model/product.dart';
import 'package:zb_store/pages/product_detail_page.dart';
import 'package:zb_store/pages/product_single_page.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/util/languages.dart';
import 'package:zb_store/util/locale.dart';
import 'package:skeleton_text/skeleton_text.dart';

class ProductsListItem extends StatelessWidget {
  final Product product1;
  final Product product2;

  ProductsListItem({
    @required this.product1,
    @required this.product2,
  });

  Number number = new Number();

  @override
  Widget build(BuildContext context) {
    //print("ProductsListItem product1: " + product1.toString());
    //print("ProductsListItem product2: " + product2.toString());
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _buildProductItemCard(context, product1),
        product2 == null
        ? _buildProductItemCardDummy(context) //Container()
        : _buildProductItemCard(context, product2),
      ],
    );
  }

    _buildProductItemCardDummy(BuildContext context) {
      return Container(
        width: MediaQuery.of(context).size.width / 2,
        height: 280,
      );
    }

    _buildProductItemCard(BuildContext context, Product product) {
    return InkWell(
      onTap: () {
        //print("product id: " + product.productId.toString());
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) {
              //return ProductDetailPage(product: product);
              return ProductSinglePage(productURL: product.productUrl);
            },
          ),
        );
      },
      child: Container(
        width: MediaQuery.of(context).size.width / 2,
        child: Card(
          //elevation: 4.0,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Stack(
                children: <Widget>[

                  SkeletonAnimation(
                    child: Container(
                      height: 280.0,
                      width: MediaQuery.of(context).size.width / 2,
                      color: Colors.grey[200],
                    ),
                  ),

                  Container(
                    child: Image.network(
                      product.images[0],
                      fit: BoxFit.cover,
                    ),
                    height: 280.0,
                    width: MediaQuery.of(context).size.width / 2,
                  ),

                  (product.salePriceText != product.priceText)
                  ?Padding(
                    padding: EdgeInsets.only(left: 5.0, top: 5.0),
                    child: Container(
                      alignment: Alignment(-1.0, -1.0),
                      width: 40.0,
                      height: 16.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.redAccent
                      ),
                      child: Center(child: Text("SALE",
                          style: TextStyle(color: Colors.white,
                              fontSize: 10.0, fontWeight: FontWeight.bold))),
                    ),
                  )
                  : Container(),

                  (product.preOrder == 1)
                      ?Padding(
                    padding: EdgeInsets.only(left: 5.0, top: 5.0),
                    child: Container(
                      alignment: Alignment(-1.0, -1.0),
                      width: 40.0,
                      height: 16.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey
                      ),
                      child: Center(child: Text("PO",
                          style: TextStyle(color: Colors.white,
                              fontSize: 10.0, fontWeight: FontWeight.bold))),
                    ),
                  )
                      : Container(),

                ],
              ),

/*
              SizedBox(
                height: 2.0,
              ),
*/
              Padding(
                padding: const EdgeInsets.only(
                  left: 4.0,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      product.productName,
                      style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold/*, fontStyle: FontStyle.italic*/),
                    ),
                    SizedBox(
                      height: 2.0,
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          product.salePriceText,
                          style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.bold),
                        ),
                        SizedBox(width: 4.0),
                        (product.salePriceText != product.priceText)
                        ?Text(
                          product.priceText,
                          style: TextStyle(fontSize: 10.0, color: Colors.grey[600], decoration: TextDecoration.lineThrough),
                        )
                        :Container(),
                      ],
                    ),
                    SizedBox(width: 1.0),
                    Text(
                      /*AppText.PRODUCT_COLOR_AVAILABLE[Globals.langSelected]
                          +*/product.colorAvailable.toString() + AppText.PRODUCT_COLOR_LABEL[Globals.langSelected],
                      style: TextStyle(fontSize: 10.0, color: Colors.grey[600], fontWeight: FontWeight.normal, fontStyle: FontStyle.italic),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
