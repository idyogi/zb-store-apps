import 'package:flutter/material.dart';
import 'package:zb_store/model/cart_item.dart';
import 'package:zb_store/scoped_model/cart_scoped_model.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/util/languages.dart';
import 'package:zb_store/util/locale.dart';
import 'package:flutter/cupertino.dart';
import 'package:zb_store/pages/cart_edit_page.dart';


class ProductsListItem extends StatefulWidget {
  final CartItem product;
  //final int index;

  ProductsListItem({
    @required this.product,
    //@required this.index,
  });

  @override
  ProductsListItemState createState() {
    return new ProductsListItemState();
  }
}

class ProductsListItemState extends State<ProductsListItem> {
  Number number = new Number();
  TextEditingController _cutNote = TextEditingController();
  //List<TextEditingController> _controllersCutNote = new List();
  //final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  
  @override
  Widget build(BuildContext context) {
    return _buildProductItemCard(context, widget.product);
  }

  _buildProductItemCard(BuildContext context, CartItem product) {

    const double imageHeight = 150.0;
    const double imageWidth = 100.0;
    const double infoWidth = 145.0;
    //_controllersCutNote[widget.index].text = product.cutNotes;

    Widget updateQuantityItem(int qty) {
      return CupertinoActionSheetAction(
        child: Text(qty.toString()),
        onPressed: () {
          //model.setItemQuantity(product, qty);
          Navigator.pop(context);
        },
      );
    }

    Future<AlertDialog> buildCutNoteWidget() {
      _cutNote.text = product.cutNotes;
      return showDialog<AlertDialog>(
        context: this.context,
        builder: (BuildContext context) =>
        new AlertDialog(
          title: new Text('Cut Note'),
          content: TextField(
            maxLines: 8,
            controller: _cutNote,
          ),
          actions: <Widget>[
            new FlatButton(
              onPressed: () {
                //model.setCutNote(product, _cutNote.text);
                Navigator.of(context).pop();
              },
              child: new Text('Simpan'),
            ),
            new FlatButton(
              onPressed: () {
                // close dialog
                //_cutNote.text = "";
                //model.setCutNote(product, _cutNote.text);
                Navigator.of(context).pop();
              },
              child: new Text('Batal'),
            ),
          ],
        ),
      );

    }

    var stringSplit = product.productModel.trim().split(",");
    //print("product.productModel: "+product.productModel.toString());
    //print("stringSplit:"+stringSplit.toString());
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) {
              return CartEditPage(cartItem: product);
            },
          ),
        );
      },
      child: Card(
        margin: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 5.0),
        elevation: 0,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[

            Expanded(
              flex: 7,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[

                  Stack(
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 3.0, horizontal: 3.0),
                        child: Image.network(
                          product.productImage, fit: BoxFit.cover,
                        ),
                        height: imageHeight,
                        width: imageWidth,
                      ),

                      (product.preOrder == 1)
                          ?Padding(
                        padding: EdgeInsets.only(left: 6.0, top: 6.0),
                        child: Container(
                          alignment: Alignment(-1.0, -1.0),
                          width: 30.0,
                          height: 14.0,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.grey
                          ),
                          child: Center(child: Text("PO",
                              style: TextStyle(color: Colors.white,
                                  fontSize: 10.0, fontWeight: FontWeight.bold))),
                        ),
                      )
                          : Container(),

                    ],
                  ),

                  Container(
                    margin: const EdgeInsets.only(top: 5.0, left: 5.0),
                    width: infoWidth,
                    //height: imageHeight,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[

                        // product name
                        Text(
                          "${product.productName} (${stringSplit[1]})",
                          style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),
                        ),
                        SizedBox(
                          height: 2.0,
                        ),
                        Text(
                          "${AppText.PRODUCT_COLOR[Globals.langSelected]}: ${stringSplit[0]}",
                          style: TextStyle(fontSize: 14.0, color: Colors.grey[600], fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 2.0,
                        ),


/*
                        (product.isCut == 1)
                            ? Text(
                          "(sudah termasuk biaya potong: ${product.priceCutText})",
                          style: TextStyle(fontSize: 10.0, color: Colors.grey[500]),
                        )
                            : Container(),
*/

                        SizedBox(
                          height: 4.0,
                        ),

                        Text(
                          "${AppText.PRODUCT_ITEM_QUANTITY[Globals.langSelected]}: ${product.itemQty}",
                          style: TextStyle(fontSize: 12.0, color: Colors.grey[800]),
                        ),

                        SizedBox(
                          height: 1.0,
                        ),

                        (product.isCut == 1)
                        ? Text(
                            "${AppText.PRODUCT_CUT_QUANTITY[Globals.langSelected]}: ${product.itemQty}",
                            style: TextStyle(fontSize: 12.0, color: Colors.grey[800]),
                          )
                        : Container(),
/*
                        : Text(
                          "${AppText.PRODUCT_CUT_QUANTITY[Globals.langSelected]}: 0",
                          style: TextStyle(fontSize: 12.0, color: Colors.grey[800]),
                        ),
*/

/*

                        Container(
                          //width: infoWidth,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                            ],
                          ),
                        ),

                      SizedBox(
                        height: 2.0,
                      ),
*/

/*
                        Container(
                          //width: infoWidth,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              (product.isCut == 1)
                                  ? Text(
                                "(dipotong)",
                                style: TextStyle(fontSize: 10.0, color: Colors.grey[500]),
                              )
                                  : Text(
                                "(tidak dipotong)",
                                style: TextStyle(fontSize: 10.0, color: Colors.grey[500]),
                              ),
                            ],
                          ),
                        ),
*/

                        SizedBox(
                          height: 5.0,
                        ),

                        // price
                        Text(
                          product.priceBeforeCutText,
                          style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold),
                        ),

                        (product.isCut == 1)
                        ?Column(
                          children: <Widget>[
                            SizedBox(
                              height: 1.0,
                            ),

                            // price
                            Text(
                              "(+${AppText.PRODUCT_CUT_FEE[Globals.langSelected]} ${product.priceCutText})",
                              style: TextStyle(fontSize: 10.0, color: Colors.grey[800]),
                            ),

                          ],
                        )
                        : Container(),

/*
                        (product.isCut == 1)
                            ? Container(
                          width: infoWidth,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "Catatan Potong:",
                                style: TextStyle(fontSize: 13.0, color: Colors.grey[800]),
                              ),

                            ],
                          ),
                        )
                            : Container(),

                        (product.isCut == 1)
                            ? Container(
                            width: infoWidth,
                            child: _buildCutList(product.cutNotes, product.itemQty)
                        )
                            : Container(),
*/
                      ],
                    ),
                  ),
                ],
              )
            ),

            Expanded(
              flex: 1,
              child: ScopedModelDescendant<CartScopedModel>(
                  builder: (context,child,model) {
                    //this.model = model;
                    return Container(
                      padding: EdgeInsets.only(right: 5.0),
                      height: imageHeight,
                      child: Column(
                        //mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[

                          (product.brandID == 1)
                          ? Image.asset("assets/images/logo_zb_1.png", width: 60.0, height: 40.0, color: Colors.black54)
                          : (product.brandID == 2)
                            ? Image.asset("assets/images/logo_zb_2.png", width: 60.0, height: 40.0, color: Colors.black54)
                            : Image.asset("assets/images/logo_zb_3.png", width: 60.0, height: 40.0, color: Colors.black54),

                          new IconButton(
                              icon: Icon(Icons.mode_edit, color: Colors.grey),
                              onPressed: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) {
                                      return CartEditPage(cartItem: product);
                                    },
                                  ),
                                );
                              }
                          ),

                          new IconButton(
                              icon: Icon(Icons.delete, color: Colors.grey),
                              onPressed: () {
                                return showDialog(
                                  context: context,
                                  builder: (context) => new AlertDialog(
                                    title: new Text('Are you sure?'),
                                    content: new Text('Remove ${product.productName} from cart?'),
                                    actions: <Widget>[
                                      new FlatButton(
                                        onPressed: () => Navigator.of(context).pop(false),
                                        child: new Text('No'),
                                      ),
                                      new FlatButton(
                                        onPressed: () {
                                          // remove from model
                                          model.removeCartItem(product.removeUrl);
                                          // remove from database
                                          print("product.id:" + product.id.toString());
                                          model.removeFromCartList(product.id);
                                          // close dialog
                                          Navigator.of(context).pop(true);
                                        },
                                        child: new Text('Yes'),
                                      ),
                                    ],
                                  ),
                                );
                              }
                          ),

                    ],
                  ),
                );
              }
          ),
        ),


          ],
        ),
      )
    );
  }

  _buildCutList(String cutNote, int cutQty) {
    List<Widget> item = List();
    String appendCutNotes = "";

    var cutNoteSplit = cutNote.split("|");

    for (int i=1; i < cutQty+1; i++) {
/*
      item.add(
        Text(
          "${cutNoteSplit[i-1]} cm, ",
          style: TextStyle(color: Colors.grey[500], fontSize: 10)
        )
      );
*/

      appendCutNotes = appendCutNotes + "${cutNoteSplit[i-1]}, ";
    }

    String removeLastComma = appendCutNotes.substring(0, appendCutNotes.length-2);

    return Padding(
      padding: const EdgeInsets.only(bottom: 6.0),
      child: /*Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[*/
          Text(
              "Potong jadi: $removeLastComma cm",
              style: TextStyle(color: Colors.grey[500], fontSize: 10)
          ),
/*
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: item
          ),
        ],
      ),
*/
    );
  }

  _buildFilterButton(String title) {
    return InkWell(
      onTap: () {
        print(title);
      },
      child: Row(
        children: <Widget>[
/*
          Icon(
            Icons.arrow_drop_down,
            color: Colors.black,
          ),
*/
          SizedBox(
            width: 2.0,
          ),
          Text(title),
        ],
      ),
    );
  }
}
