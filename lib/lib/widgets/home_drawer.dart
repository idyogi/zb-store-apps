import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zb_store/pages/address_list_page.dart';
import 'package:zb_store/pages/home_address.dart';
import 'package:zb_store/pages/home_lang_page.dart';
import 'package:zb_store/pages/home_login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zb_store/pages/product_category_page.dart';
import 'package:zb_store/pages/product_fave_page.dart';
import 'package:zb_store/pages/order_page.dart';
import 'package:zb_store/pages/product_list_page.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/pages/product_search_page.dart';
import 'package:zb_store/util/languages.dart';

class HomeDrawer extends StatefulWidget {
  final List<dynamic> jsonFeatures;
  HomeDrawer({this.jsonFeatures});

  @override
  HomeDrawerState createState() {
    return new HomeDrawerState(jsonFeatures);
  }
}

class HomeDrawerState extends State<HomeDrawer> {
  final List<dynamic> jsonFeatures;
  HomeDrawerState(this.jsonFeatures);

  bool alreadyLogin = false;
  bool zbMenuIsExpanded;
  bool zbLuxIsExpanded;
  bool zbMiniIsExpanded;

  @override
  void initState() {
    super.initState();
    zbMenuIsExpanded = false;
    zbLuxIsExpanded = false;
    zbMiniIsExpanded = false;
  }

  @override
  Widget build(BuildContext context) {

    Future<bool> _getSavedLoginStatus() async{
      bool isGoogleLoggedIn = false;
      bool isFacebookLoggedIn = false;
      final prefs = await SharedPreferences.getInstance();

      isGoogleLoggedIn = prefs.getBool(Constants.IS_GOOGLE_LOGIN) ?? false;
      isFacebookLoggedIn = prefs.getBool(Constants.IS_FACEBOOK_LOGIN) ?? false;
      return isFacebookLoggedIn || isGoogleLoggedIn;
    }

    void _removeSavedPrefs() async {
      final prefs = await SharedPreferences.getInstance();
      // remove google saved prefs
      prefs.setBool(Constants.IS_GOOGLE_LOGIN, false);
      prefs.setString(Constants.SAVED_GOOGLE_ID, "");
      prefs.setString(Constants.SAVED_GOOGLE_NAME, "");
      prefs.setString(Constants.GOOGLE_PIC_URL, "");
      prefs.setString(Constants.SAVED_GOOGLE_EMAIL, "");
      prefs.setString(Constants.SAVED_GOOGLE_TOKEN, "");
      // remove facebook saved prefs
      prefs.setBool(Constants.IS_FACEBOOK_LOGIN, false);
      prefs.setString(Constants.SAVED_FACEBOOK_ID, "");
      prefs.setString(Constants.SAVED_FACEBOOK_NAME, "");
      prefs.setString(Constants.FACEBOOK_PIC_URL, "");
      prefs.setString(Constants.SAVED_FACEBOOK_EMAIL, "");
      prefs.setString(Constants.SAVED_FACEBOOK_TOKEN, "");

      prefs.setString(Constants.SAVED_USER_MEMBERSHIP, "");
    }

    Future<Map> _getSavedLoginData() async{
      final prefs = await SharedPreferences.getInstance();
      Map savedUser = new Map();
      bool _isGoogleLoggedIn = prefs.getBool(Constants.IS_GOOGLE_LOGIN) ?? false;
      bool _isFacebookLoggedIn = prefs.getBool(Constants.IS_FACEBOOK_LOGIN) ?? false;
      // initialize
      savedUser['id'] = "";
      savedUser['name'] = "";
      savedUser['email'] = "";
      savedUser['token'] = "";
      savedUser['member'] = "";

      if (_isGoogleLoggedIn) {
        savedUser['provider'] = "Google";
        savedUser['id'] = prefs.getString(Constants.SAVED_GOOGLE_ID) ?? "";
        savedUser['name'] = prefs.getString(Constants.SAVED_GOOGLE_NAME) ?? "";
        savedUser['email'] = prefs.getString(Constants.SAVED_GOOGLE_EMAIL) ?? "";
        savedUser['pic'] = prefs.getString(Constants.GOOGLE_PIC_URL) ?? "";
        savedUser['token'] = prefs.getString(Constants.SAVED_GOOGLE_TOKEN) ?? "";
        savedUser['member'] = prefs.getString(Constants.SAVED_USER_MEMBERSHIP) ?? "";
      } else if (_isFacebookLoggedIn) {
        savedUser['provider'] = "Facebook";
        savedUser['id'] = prefs.getString(Constants.SAVED_FACEBOOK_ID) ?? "";
        savedUser['name'] = prefs.getString(Constants.SAVED_FACEBOOK_NAME) ?? "";
        savedUser['email'] = prefs.getString(Constants.SAVED_FACEBOOK_EMAIL) ?? "";
        savedUser['pic'] = prefs.getString(Constants.FACEBOOK_PIC_URL) ?? "";
        savedUser['token'] = prefs.getString(Constants.SAVED_FACEBOOK_TOKEN) ?? "";
        savedUser['member'] = prefs.getString(Constants.SAVED_USER_MEMBERSHIP) ?? "";
      } else {
        _removeSavedPrefs();
      }
      return savedUser;
    }

    _buildCategoryList(int brandID, List<dynamic> categories) {
      List<Widget> categoryItem = new List();
      for (int i = 0; i < categories.length; i++) {
        categoryItem.add(
          Column(
            children: <Widget>[
              InkWell(
                onTap: () {
/*
                  Route route = MaterialPageRoute(builder: (context) => ProductsCategoryPage(
                    id: brandID,
                    urlCategory: categories[i]["url"],
                    name: categories[i]["name"],
                  ));
                  Navigator.pushReplacement(context, route);
*/
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) {
                        return ProductsCategoryPage(
                          id: brandID,
                          urlCategory: categories[i]["url"],
                          name: categories[i]["name"],
                        );
                      },
                    ),
                  );
                },
                child: Text(categories[i]["name"], style: TextStyle(color: Colors.grey[500])),
              ),
              SizedBox(height: 10.0),
            ],
          )
        );
      }
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: categoryItem,
      );
    }

/*
    _goToSearchPage(String search) {
      //Navigator.pushReplacement(context, route);
      return Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) {
            return ProductSearchPage(search: search);
          },
        ),
      );
    }
*/

    return Drawer(
      child: FutureBuilder<Map>(
        future: _getSavedLoginData(),
        builder: (context, snapshot) {
          print("snapshot: " + snapshot.data.toString());
          print("snapshot hasData: " + snapshot.hasData.toString());
          return (snapshot.hasData)
          ? ListView(
              children: <Widget>[
                // header
                //Stack(
                  //children: <Widget>[
                    UserAccountsDrawerHeader(
                      //margin: EdgeInsets.only(top: 18.0),
                      accountName: Text(snapshot.data['name'] != null ? snapshot.data['name'] : "", style: TextStyle(color: Colors.black)),
                      accountEmail: Text(snapshot.data['email'] != null ? snapshot.data['email'] : "", style: TextStyle(color: Colors.black)),
                      decoration: BoxDecoration(color: Colors.white),
                      currentAccountPicture:
                      (snapshot.data['pic'] != "" && snapshot.data['pic'] != null)
                          ? CircleAvatar (
                          backgroundImage: NetworkImage(snapshot.data['pic']),
                          backgroundColor: Colors.grey[100]
                      )
                          : CircleAvatar (
                          child: Image.asset("assets/images/logo_zb_1.png"),
                          backgroundColor: Colors.grey[100]
                      ),
                      otherAccountsPictures: <Widget>[
                        (snapshot.data['member'] == "privilege")
                        ? Image.asset("assets/images/crown.png", width: 100.0)
                        : Container(),
                      ],
                    ),

/*
                    Padding(
                      padding: EdgeInsets.only(left: 75.0, top: 0),
                      child: Container(
                        width: 40.0,
                        child: Image.asset("assets/images/crown.png"),
                      ),
                    ),
*/
                  //],
                //),

                // search
                Padding(
                  padding: EdgeInsets.only(top: 8.0, left: 15.0, right: 28.0, bottom: 6.0),
                  child: /*InkWell(
                    onTap: () {

                    },
                    child:*/ Container(
                      height: 35,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16.0),
                          color: Colors.grey[400]),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(left: 10.0, top: 4.0, right: 5.0),
                            child: Icon(Icons.search, color: Colors.white, size: 25.0),
                          ),

                          Flexible(
                            child: TextField(
                              //controller: null,
                              onSubmitted: (search) {
                                Navigator.of(context).pop();
                                Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) {
                                        return ProductSearchPage(search: search);
                                      },
                                    )
                                );
                              },
                              maxLines: 1,
                              //maxLength: 30,
                              cursorColor: Colors.white,
                              style: TextStyle(fontSize: 16, color: Colors.white),
                              decoration: InputDecoration.collapsed(
                                filled: false,
                                hintText: "Search",
                                hintStyle: TextStyle(color: Colors.white),
                              ),
                            ),
                          )
                          //Text("Search", style: TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.normal)),
                        ],
                      ),
                    ),
                  //),
                ),

                ListTile(
                  leading: Image.asset("assets/images/logo_zb_1.png", width: 60.0, height: 40.0, color: Colors.grey),
                  title: Text("ZB"),
                  //trailing: Icon(Icons.arrow_forward),
                  onTap: () {
                    //Navigator.of(context).pop();
                    Route route = MaterialPageRoute(builder: (context) => ProductsListPage(
                      brandID: jsonFeatures[0]["brands"][0]["id"],
                      urlProducts: jsonFeatures[0]["brands"][0]["url"],
                      jsonFeatures: jsonFeatures,
                    ));
                    Navigator.pushReplacement(context, route);
      /*
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) {
                          return ProductsListPage(
                              brandID: 1,
                              urlProducts: "http://beta.devsandbox.me/api/products?brand=1",
                              urlFeatures: "http://beta.devsandbox.me/api/brand/1",
                          );
                        },
                      ),
                    );
      */
                  },
                  trailing: new IconButton(
                    icon: zbMenuIsExpanded ? new Icon(Icons.arrow_drop_up, size: 40) : new Icon(Icons.arrow_drop_down, size: 40),
                    onPressed: () {
                      setState(() {
                        zbMenuIsExpanded ? zbMenuIsExpanded = false : zbMenuIsExpanded = true;
                      });
                    },
                  ),
                ), // menu

                // ZB expandable category
                zbMenuIsExpanded
                ? Container(
                    margin: EdgeInsets.only(left: 20.0),
                    child: _buildCategoryList(1, jsonFeatures[1]["category"]),
                    )
                : Container(),

                ListTile(
                  leading: Image.asset("assets/images/logo_zb_2.png", width: 60.0, height: 40.0, color: Colors.grey),
                  title: Text("ZB Lux"),
                  //trailing: Icon(Icons.arrow_forward),
                  onTap: () {
                    //Navigator.of(context).pop();
                    Route route = MaterialPageRoute(builder: (context) => ProductsListPage(
                      brandID: jsonFeatures[0]["brands"][1]["id"],
                      urlProducts: jsonFeatures[0]["brands"][1]["url"],
                      jsonFeatures: jsonFeatures,
                    ));
                    Navigator.pushReplacement(context, route);
      /*
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) {
                          return ProductsListPage(
                              brandID: 2,
                              urlProducts: "http://beta.devsandbox.me/api/products?brand=2",
                              urlFeatures: "http://beta.devsandbox.me/api/brand/2",
                          );
                        },
                      ),
                    );
      */
                  },
                  trailing: new IconButton(
                    icon: zbLuxIsExpanded ? new Icon(Icons.arrow_drop_up, size: 40) : new Icon(Icons.arrow_drop_down, size: 40),
                    onPressed: () {
                      setState(() {
                        zbLuxIsExpanded ? zbLuxIsExpanded = false : zbLuxIsExpanded = true;
                      });
                    },
                  ),
                ), // menu

                // ZB lux expandable category
                zbLuxIsExpanded
                    ? Container(
                  margin: EdgeInsets.only(left: 20.0),
                  child: _buildCategoryList(2, jsonFeatures[2]["category"]),
                )
                : Container(),


                ListTile(
                  leading: Image.asset("assets/images/logo_zb_3.png", width: 60.0, height: 40.0, color: Colors.grey),
                  title: Text("ZB Teens"),
                  //trailing: Icon(Icons.arrow_forward),
                  onTap: () {
                    //Navigator.of(context).pop();
                    Route route = MaterialPageRoute(builder: (context) => ProductsListPage(
                      brandID: jsonFeatures[0]["brands"][2]["id"],
                      urlProducts: jsonFeatures[0]["brands"][2]["url"],
                      jsonFeatures: jsonFeatures,
                    ));
                    Navigator.pushReplacement(context, route);

      /*
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) {
                          return ProductsListPage(
                              brandID: 3,
                              urlProducts: "http://beta.devsandbox.me/api/products?brand=3",
                              urlFeatures: "http://beta.devsandbox.me/api/brand/3",
                          );
                        },
                      ),
                    );
      */
                  },
                  trailing: new IconButton(
                    icon: zbMiniIsExpanded ? new Icon(Icons.arrow_drop_up, size: 40) : new Icon(Icons.arrow_drop_down, size: 40),
                    onPressed: () {
                      setState(() {
                        zbMiniIsExpanded ? zbMiniIsExpanded = false : zbMiniIsExpanded = true;
                      });
                    },
                  ),
                ), // menu

                // ZB lux expandable category
                zbMiniIsExpanded
                    ? Container(
                  margin: EdgeInsets.only(left: 20.0),
                  child: _buildCategoryList(3, jsonFeatures[3]["category"]),
                )
                    : Container(),

                Divider(),
                (snapshot.data['name'] != "")
                ?ListTile(
                  leading: Icon(Icons.format_list_bulleted),
                  title: Text(AppText.HOME_ORDER[Globals.langSelected]),
                  //trailing: Icon(Icons.arrow_forward),
                  onTap: () {
                    Navigator.of(context).pop();
                    //Route route = MaterialPageRoute(builder: (context) => OrderPage());
                    //Navigator.pushReplacement(context, route);
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) {
                          return OrderPage();
                        },
                      ),
                    );
                  },
                ) // menu
                : Container(),
                /*ListTile(
                  leading: Icon(Icons.format_list_bulleted, color: Colors.grey[300]),
                  title: Text("Pesanan Saya", style: TextStyle(color: Colors.grey[300])),
                  //trailing: Icon(Icons.arrow_forward),
                  onTap: null,
                ),*/ // menu

                (snapshot.data['name'] != "")
                ? ListTile(
                    leading: Icon(Icons.favorite_border),
                    title: Text(AppText.HOME_FAVE[Globals.langSelected]),
                    //trailing: Icon(Icons.arrow_forward),
                    onTap: () {
                      Navigator.of(context).pop();
                      //Route route = MaterialPageRoute(builder: (context) => ProductFavePage());
                      //Navigator.pushReplacement(context, route);
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return ProductFavePage();
                          },
                        ),
                      );
                    },
                )
                : Container(),
/*
                : ListTile(
                    leading: Icon(Icons.favorite_border, color: Colors.grey[300]),
                    title: Text("Favorite Saya", style: TextStyle(color: Colors.grey[300])),
                    //trailing: Icon(Icons.arrow_forward),
                    onTap: null,
                ),
*/
      /*
                        (snapshot.data['name'] != "")
                ?ListTile(
                    leading: Icon(Icons.card_travel),
                    title: Text("Keranjang Saya"),
                    //trailing: Icon(Icons.arrow_forward),
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.pushNamed(context, Constants.ROUTE_CART_LIST);
                    }
                ) // menu
                :ListTile(
                    leading: Icon(Icons.card_travel, color: Colors.grey[300]),
                    title: Text("Keranjang Saya", style: TextStyle(color: Colors.grey[300])),
                    //trailing: Icon(Icons.arrow_forward),
                    onTap:null,
                ), // menu
      */
                (snapshot.data['name'] != "") ? Divider() : Container(),
      /*
                FutureBuilder<bool>(
                    future: _getSavedLoginStatus(),
                    builder: (context, snapshot) {
                      return (snapshot.hasData && snapshot.data == true)
      */

                // pilih bahasa
                ListTile(
                  leading: Icon(Icons.language),
                  title: Text(AppText.HOME_LANG[Globals.langSelected]),
                  //trailing: Icon(Icons.arrow_forward),
                  onTap: () {
                    Navigator.of(context).pop();
                    //Route route = MaterialPageRoute(builder: (context) => LoginPage());
                    //Navigator.pushReplacement(context, route);
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) {
                          return HomeLangPage();
                        },
                      ),
                    );
                  },
                ),

                (snapshot.data['name'] != "")
                    ? ListTile(
                  leading: Icon(Icons.local_post_office),
                  title: Text(AppText.HOME_ADDRESS[Globals.langSelected]),
                  //trailing: Icon(Icons.arrow_forward),
                  onTap: () {
                    Navigator.of(context).pop();
                    //Route route = MaterialPageRoute(builder: (context) => ProductFavePage());
                    //Navigator.pushReplacement(context, route);
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) {
                          return AddressPage();
                        },
                      ),
                    );
                  },
                )
                : Container(),


                (snapshot.data['name'] != "")
                ? Container()
/*
                ? ListTile(
                    leading: Icon(Icons.person, color: Colors.grey[300]),
                    title: Text("Login", style: TextStyle(color: Colors.grey[300])),
                    //trailing: Icon(Icons.arrow_forward, color: Colors.grey[300]),
                    onTap: null,
                  )
*/
                : ListTile(
                    leading: Icon(Icons.person),
                    title: Text("Login"),
                    //trailing: Icon(Icons.arrow_forward),
                    onTap: () {
                      Navigator.of(context).pop();
                      //Route route = MaterialPageRoute(builder: (context) => LoginPage());
                      //Navigator.pushReplacement(context, route);
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return LoginPage();
                          },
                        ),
                      );
                    },
                  ),

                (snapshot.data['name'] != "")
                ? ListTile(
                    leading: Icon(Icons.exit_to_app),
                    title: Text("Logout"),
                    //trailing: Icon(Icons.arrow_forward),
                    onTap: () {
                      _removeSavedPrefs();
                      Navigator.of(context).pop();
                      //Route route = MaterialPageRoute(builder: (context) => LoginPage());
                      //Navigator.pushReplacement(context, route);
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return LoginPage();
                          },
                        ),
                      );
                    },
                )
                : Container(),
/*
                : ListTile(
                  leading: Icon(Icons.exit_to_app, color: Colors.grey[300]),
                  title: Text("Logout", style: TextStyle(color: Colors.grey[300])),
                  //trailing: Icon(Icons.arrow_forward),
                  onTap: null,
                ),
*/

              ],
            )
          : new Container();
        }
      ),
    );
  }
}
