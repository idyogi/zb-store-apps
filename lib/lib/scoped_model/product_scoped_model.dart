import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:zb_store/model/product.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:zb_store/util/globals.dart';
import 'package:shared_preferences/shared_preferences.dart';


class ProductsScopedModel extends Model {
  List<Product> _productsList = [];
  List<Product> _cartList = [];

  bool _isLoading = true;
  bool _hasModeProducts = true;
  bool _produkHabis = false;
  int currentProductCount;

  String token = "";
  String userName = "";
  String userEmail = "";


  List<Product> get productsList => _productsList;
  List<Product> get cartList => _cartList;

  bool get isLoading => _isLoading;
  bool get produkHabis => _produkHabis;
  bool get hasMoreProducts => _hasModeProducts;

  bool _showTimeoutPopup = false;


/*
  void setPotongJadi(int value) {
    _potongJadi.add(value);
    print("SET potong jadi[$value]="+_potongJadi[value].toString());
  }

  int getPotongJadi(int value) {
    print("GET potong jadi[$value]="+_potongJadi[value-1].toString());
    return _potongJadi[value-1];
  }
*/

  void addToProductsList(Product product) {
    _productsList.add(product);
  }

  void addToCartList(Product product) {
    _cartList.add(product);
  }

  void setSelectedSize(Product product, String text) {
    product.selectedSize = text;
    product.cutQuantity = 0;
    product.cutNote = "";
    //print("setSelectedSize called");
    notifyListeners();
  }

  void setSelectedColor(Product product, String text) {
    product.selectedColor = text;
    product.cutQuantity = 0;
    product.cutNote = "";
    notifyListeners();
  }

  void setItemQuantity(Product product, int qty) {
    product.itemQuantity = qty;
    // reset cut data
    product.cutQuantity = 0;
    product.cutNote = "";
    notifyListeners();
  }

  void setCutQuantity(Product product, int qty) {
    product.cutQuantity = qty;
    notifyListeners();
  }

  int getCutQuantity(Product product) {
    return product.cutQuantity == null ? 0 : product.cutQuantity;
  }

  int getStockQuantity(Product product) {
    return product.stockQuantity;
  }

  void setStockQuantity(Product product, int qty) {
    product.stockQuantity = qty;
    notifyListeners();
  }

  void setSkuPrice(Product product, String price) {
    product.skuPriceText = price;
    notifyListeners();
  }

  void setSkuSalePrice(Product product, String price) {
    product.skuSalePriceText = price;
    notifyListeners();
  }

  String getSkuPrice(Product product) {
    return product.skuPriceText;
  }

  String getSkuSalePrice(Product product) {
    return product.skuSalePriceText;
  }

/*
  void setDefaultItemQuantity(Product product) {
    if (product.stockQuantity == 0) {
      product.itemQuantity = 0;
    } else if (product.stockQuantity > 0) {
      product.itemQuantity = 1;
    }
  }
*/

  void setCutNote(Product product, String note) {
    product.cutNote = note;
    notifyListeners();
  }

  int getProductsCount() {
    //print("Cek all stock: "+_productsList.length.toString());
    return _productsList.length;
  }

  void setTimeoutPopup(bool value) {
    _showTimeoutPopup = value;
    notifyListeners();
  }

  bool displayTimeoutPopup() {
    return _showTimeoutPopup;
  }

  Future getSavedToken() async {
    final prefs = await SharedPreferences.getInstance();
    final alreadyLogin = prefs.getBool(Constants.IS_GOOGLE_LOGIN) ?? false;
    if (alreadyLogin) {
      token = prefs.getString(Constants.SAVED_GOOGLE_TOKEN) ?? "";
      userName = prefs.getString(Constants.SAVED_GOOGLE_NAME) ?? "";
      userEmail = prefs.getString(Constants.SAVED_GOOGLE_EMAIL) ?? "";
    } else {
      token = prefs.getString(Constants.SAVED_FACEBOOK_TOKEN) ?? "";
      userName = prefs.getString(Constants.SAVED_FACEBOOK_NAME) ?? "";
      userEmail = prefs.getString(Constants.SAVED_FACEBOOK_EMAIL) ?? "";
    }
    // bypass
    //token = '\$2y\$10\$JcIt0Lpn/W1z2ugl8h6Yt.cjriDjgoFIfqhHnAe0qL9uC5N0hauYG';
  }

  bool getFavorite(Product product) {
    bool value;
    product.isFavorite == 1 ? value = true : value = false;
    return value;
  }

  void setFavorite(Product product, bool value) {
    value == true ? product.isFavorite = 1 : product.isFavorite = 0;
    notifyListeners();
  }

  Future<dynamic> addFave(Product product, int productID) async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.post(
        Constants.API_ADD_FAVE,
        headers: { "X-Authorization":token},
        body: {
          "id" : productID.toString(),
        }
    ).catchError((error) {
      print(error);
    },
    );
    //setFavorite(product, true);
    //product.isFavorite = 1;
    //notifyListeners();

    var response = json.decode(jsonResponse.body);
    print("addFave response: "+response.toString());
    return response;
  }

  removeFromFaveList(int productID) {
    _productsList.removeWhere((product) => product.productId == productID);
    notifyListeners();
  }

  Future<dynamic> removeFave(Product product, int productID) async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.post(
      Constants.API_REMOVE_FAVE + productID.toString(),
      headers: { "X-Authorization":token},
/*
        body: {
          "id" : productID.toString(),
        }
*/
    ).catchError((error) {
      print(error);
    },
    );
    //removeFromFaveList(productID);

    //setFavorite(product, false);
    //product.isFavorite = 0;
    //notifyListeners();

    var response = json.decode(jsonResponse.body);
    print("addFave response: "+response.toString());
    return response;
  }

  Future<dynamic> _getFavorite(url, pageIndex) async {
    // get token from shared prefs
    await getSavedToken();
    // get cart
    var response = await http.get(
      url+ "?page=$pageIndex",
      headers: {
        "X-Authorization":token,
      },
    ).catchError(
          (error) {
        return false;
      },
    );
    if (json.decode(response.body)['meta']['current_page'] ==
        json.decode(response.body)['meta']['last_page']) {
      _produkHabis = true;
    }
    return json.decode(response.body)['data'];
  }

  Future parseFavoriteFromResponse(String url, int pageIndex) async {
    if (!_produkHabis) {
      if (pageIndex == 1) {
        _isLoading = true;
      }

      notifyListeners();

      currentProductCount = 0;

      var dataFromResponse = await _getFavorite(url, pageIndex);

      dataFromResponse.forEach(
            (newProduct) {
          currentProductCount++;

          List<String> imagesOfProductList = [];

          newProduct["images"].forEach((newImage) {
            imagesOfProductList.add(newImage.toString());
          },
          );

          Product product = new Product(
            productId: newProduct["id"],
            productName: newProduct["name"],
            description: newProduct["description"],
            regularPrice: newProduct["price"],
            salePrice: newProduct["price_sale"],
            priceText: newProduct["price_text"],
            salePriceText: newProduct["price_sale_text"],
            colorAvailable: newProduct["color_available"],
            //skuPriceText: newProduct["price_text"],
            productUrl: newProduct["product_url"],
            preOrder: newProduct["preorder"],
            isFavorite: newProduct["is_favorite"],

            discount: ((((newProduct["price"] -
                newProduct["price_sale"]) /
                (newProduct["price"])) *
                100))
                .round(),

            images: imagesOfProductList,
            //categories: categoriesOfProductList,
          );
          //print("productURL: " + product.productUrl);
          product.itemQuantity = 1;
          product.cutQuantity = 0;
          product.stockQuantity = 0;

          addToProductsList(product);
        },
      );

      if (pageIndex == 1) _isLoading = false;
/*
      if (currentProductCount < 6) {
        _hasModeProducts = false;
      }
*/

      notifyListeners();
    }
  }

  Future<dynamic> getProductsDetails(String url) async {

    var response = await http.get(url).catchError((error) {
      return false;
    },
    );
    //var dataFromResponse = json.decode(response.body)['data'];
    return json.decode(response.body);
  }


  Future parseProductsFromResponse(String url, int pageIndex) async {
    await getSavedToken();

    //print("parseProductsFromResponse: produkHabis="+_produkHabis.toString());
    if (!_produkHabis) {

      if (pageIndex == 1) {
        _isLoading = true;
      }

      //*****BIKIN KACAUUUUUUU!!!!!****
      //notifyListeners();
      //******************************

      currentProductCount = 0;
      print("parseProductsFromResponse: produk blm habis-> call:"+url + "&page=$pageIndex START");
      //var response = await _getProductsByCategory(url, pageIndex);
      var response = await http.get(
        url + "&page=$pageIndex",
        headers: {
          "X-Authorization":token,
        },
      ).catchError((error) {
        //print ("error token");
        //return false;
       },
      );

      print("parseProductsFromResponse: call:"+url + "&page=$pageIndex FINISHED");
      if (json.decode(response.body)['meta']['current_page'] >=
          json.decode(response.body)['meta']['last_page']) {
        _produkHabis = true;
      }

      var dataFromResponse = json.decode(response.body)['data'];
      dataFromResponse.forEach((newProduct) {
          currentProductCount++;

          List<String> imagesOfProductList = [];

          newProduct["images"].forEach((newImage) {
              imagesOfProductList.add(newImage.toString());
            },
          );

          Product product = new Product(
            productId: newProduct["id"],
            productName: newProduct["name"],
            description: newProduct["description"],
            regularPrice: newProduct["price"],
            salePrice: newProduct["price_sale"],
            //skuPriceText: newProduct["price_text"],
            productUrl: newProduct["product_url"],
            isFavorite: newProduct["is_favorite"],
            preOrder: newProduct["preorder"],
            colorAvailable: newProduct["color_available"],
            priceText: newProduct["price_text"],
            salePriceText: newProduct["price_sale_text"],
            ifSale: (newProduct["price_text"] != newProduct["price_sale_text"]),

/*
          discount: ((((int.parse(newProduct["price"]) -
                          int.parse(newProduct["price_sale"])) /
                      (int.parse(newProduct["price"]))) *
                  100))
              .round(),
*/

            // int
            discount: ((((newProduct["price"] -
                newProduct["price_sale"]) /
                (newProduct["price"])) *
                100))
                .round(),

            images: imagesOfProductList,
            //matching: newProduct["matching"],
            //categories: categoriesOfProductList,
          );

        product.itemQuantity = 1;
        product.cutQuantity = 0;
        product.stockQuantity = 0;

        addToProductsList(product);
        },
      );

      if (pageIndex == 1) _isLoading = false;
      if (currentProductCount < 10) {
        _hasModeProducts = false;
      }

      notifyListeners();
    }
  }

  setImageByColorSize(Product product, String color, String size) {
    List<String> imageSet = [];
    product.matching["$color,$size"]["images"].forEach((item) {
      if (item != null) imageSet.add(item.toString());
    });

    if (imageSet.length > 0) {
      //product.images = [];
      product.images = new List();
      product.images = imageSet;
      //print("product_scouped_model->setImageByColorSize images = " + product.images.toString());
      //notifyListeners();
    }
  }

  setImageByJSON(Product product, dynamic matching, String colorSize) {
    List<String> imageSet = [];
    //print("product.matching:" + product.matching.toString());
    matching[colorSize]["images"].forEach((item) {
      if (item != null) imageSet.add(item.toString()); // : imageSet.add("http://beta.devsandbox.me/images/hijab/e2.jpg");
    });
    if (imageSet.length > 0) {
      product.images = [];
      product.images = imageSet;
      //print("images = " + product.images.toString());
      notifyListeners();
    }
  }

  List<String> getImageSet(Product product) {
    return product.images;
  }

  Future<Product> getProductsByURL(url) async {

    var response = await http.get(
      url,
      headers: {},
    ).catchError((error) {
      return false;
    },
    );
    //return json.decode(response.body);

    var dataFromResponse = json.decode(response.body)['data'];

    //print("dataFromResponse: " + dataFromResponse.toString());

    //dataFromResponse.forEach((newProduct) {

    List<String> imagesOfProductList = [];
    dataFromResponse["images"].forEach((newImage) {
      imagesOfProductList.add(newImage.toString().replaceAll(" ", "%20"));
    });

    List<String> sizeOfProductList = [];
    dataFromResponse["variations"]["Size"].forEach((item) {
      sizeOfProductList.add(item.toString());
    });

    List<String> colorOfProductList = [];
    dataFromResponse["variations"]["Color"].forEach((item) {
      colorOfProductList.add(item.toString());
    });

    var color = dataFromResponse["variations"]["Color"][0];
    var size = dataFromResponse["variations"]["Size"][0];
    var colorSize = "$color,$size";

    Product product = new Product(
      productId: dataFromResponse["id"],
      productName: dataFromResponse["name"],
      description: dataFromResponse["description"],
      regularPrice: dataFromResponse["price"],
      salePrice: dataFromResponse["price_sale"],
      productUrl: dataFromResponse["product_url"],
      colorAvailable: dataFromResponse["color_available"],
      isFavorite: dataFromResponse["is_favorite"],
      preOrder: dataFromResponse["preorder"],
      discount: ((((dataFromResponse["price"] -
          dataFromResponse["price_sale"]) /
          (dataFromResponse["price"])) *
          100))
          .round(),
      images: imagesOfProductList,
      size: sizeOfProductList,
      color: colorOfProductList,
      selectedColor: color,
      selectedSize: size,
      skuId: dataFromResponse["matching"][colorSize]["sku_id"],
      skuPriceText: dataFromResponse["matching"][colorSize]["price_text"],
      skuSalePriceText: dataFromResponse["matching"][colorSize]["price_sale_text"],
      ifSaleSku: (dataFromResponse["matching"][colorSize]["price_text"] !=
          dataFromResponse["matching"][colorSize]["price_sale_text"]),
      matching: dataFromResponse["matching"],
    );
    product.itemQuantity = 1;
    product.cutQuantity = 0;
    product.stockQuantity = 0;
    //});
    addToProductsList(product);

    //print("return: " + json.decode(response.body).toString());
    //print("images: " + product.images.toString());

    //return json.decode(response.body);
    return product;
  }

  Future<dynamic> changeLang(String lang) async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.get(
        Constants.API_LANG + lang,
        headers: { "X-Authorization":token},
    ).catchError((error) {
        print(error);
      },
    );
    var response = json.decode(jsonResponse.body);
    //print("addFave response: "+response.toString());
    return response;
  }

}
