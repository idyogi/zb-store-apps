import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:zb_store/model/cart_item.dart';
import 'package:zb_store/model/payment.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/model/product.dart';
import 'package:zb_store/model/orders.dart';

class CartScopedModel extends Model {
  List<CartItem> _cartList = [];
  //List<Payment> _transferList = [];
  bool _isLoading = true;
  bool _hasModeProducts = true;
  bool _produkHabis = false;
  //bool _isUpdated = false;
  int currentProductCount;
  int totalCartItem = 0;
  String totalCartMoney = "";
  String token = "";
  String userName = "";
  String userEmail = "";
  List<Product> _productsList = [];
  List<Product> get productsList => _productsList;
  List<CartItem> get cartList => _cartList;
  bool get produkHabis => _produkHabis;


  //List<Payment> get transferList => _transferList;

  bool get isLoading => _isLoading;

  bool get hasMoreProducts => _hasModeProducts;

/*
  void addToAddressList(Address address) {
    _addressList.add(address);
  }
*/

  void addToCartList(CartItem item) {
    _cartList.add(item);
  }

  Future getSavedToken() async {
    final prefs = await SharedPreferences.getInstance();
    final alreadyLogin = prefs.getBool(Constants.IS_GOOGLE_LOGIN) ?? false;
    if (alreadyLogin) {
      token = prefs.getString(Constants.SAVED_GOOGLE_TOKEN);
      userName = prefs.getString(Constants.SAVED_GOOGLE_NAME);
      userEmail = prefs.getString(Constants.SAVED_GOOGLE_EMAIL);
    } else {
      token = prefs.getString(Constants.SAVED_FACEBOOK_TOKEN);
      userName = prefs.getString(Constants.SAVED_FACEBOOK_NAME);
      userEmail = prefs.getString(Constants.SAVED_FACEBOOK_EMAIL);
    }
    // bypass
    //token = '\$2y\$10\$JcIt0Lpn/W1z2ugl8h6Yt.cjriDjgoFIfqhHnAe0qL9uC5N0hauYG';
  }

  int getCartCount() {
    return _cartList.length;
  }

  int getProductCount() {
    return _productsList.length;
  }

  int getTotalCartItem() {
    return totalCartItem;
  }

  String getTotalCartMoney() {
    return totalCartMoney;
  }

  String getUserName() {
    return userName;
  }


//  void sumCartTotalQty() {
  int getCartTotalQty() {
    int total = 0;
    for (int i=0; i < _cartList.length; i++) {
      total = total + _cartList[i].price;
    }
    //_cartTotalQty = total;
    return total;
  }

//  void sumCartTotalPurchase() {
  double getCartTotalPurchase() {
    double total = 0;
    for (int i=0; i < _cartList.length; i++) {
      total = total + _cartList[i].salePrice * _cartList[i].price;
    }
    //_cartTotalPurchase = total;
    return total;
  }


  Future<dynamic> getCartQuantity() async {
    // get token from shared prefs
    await getSavedToken();
    // get cart
    var response = await http.get(
      Constants.API_GET_CART,
      headers: {
        "X-Authorization":token,
      },
    ).catchError(
          (error) {
        return false;
      },
    );
    return json.decode(response.body)['total_text'];
  }

  void addCartItem(CartItem item) {
    if ((item.price <= item.stockQuantity - 1) && (item.stockQuantity != 0)) {
      item.price++;
      notifyListeners();
    }
  }

  void subtractCartItem(CartItem item) {
    if (item.price > 1) {
      item.price--;
      notifyListeners();
    }
  }

  void updateItemQuantity(CartItem item, int qty) async {
    //_isUpdated = true;
    item.itemQty = qty;
    notifyListeners();
  }

  void updateIsCut(CartItem item, int value) async {
    //_isUpdated = true;
    item.isCut = value;
    //if (value == 0) item.cutNotes = "";
    notifyListeners();
  }

  void updateCutNote(CartItem item, String note) {
    //_isUpdated = true;
    item.cutNotes = note;
    notifyListeners();
  }

  Future<dynamic> checkoutGet() async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.get(
      Constants.API_GET_CHECKOUT,
      headers: {
        "X-Authorization":token,
      },
    ).catchError(
          (error) {
        print(error);
      },
    );
    var response = json.decode(jsonResponse.body);
    // debug

/*
    print("checkoutGet response: "+response.toString());
    print("checkoutGet username: "+userName);
    print("checkoutGet address: "+response['address'].toString());
    print("checkoutGet item_count: "+response['item_count'].toString());
    print("checkoutGet total_text: "+response['total_text']);
*/
    return response;
  }

  void removeFromCartList(int productId) {
    _cartList.removeWhere((CartItem) => CartItem.id == productId);
    notifyListeners();
  }

  Future<dynamic> removeCartItem(String removeUrl) async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.post(
        removeUrl,
        headers: {
          "X-Authorization":token,
        },
    ).catchError(
          (error) {
        print(error);
      },
    );
    var response = json.decode(jsonResponse.body);
    print("remove cart response: "+response.toString());
    // update total item and money
    totalCartItem = response['item_count'];
    totalCartMoney = response['total_text'];
    notifyListeners();
    return response;
  }

  Future<dynamic> updateCart(String updateUrl, int qty, int isCut, String cutNotes) async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.post(
        updateUrl,
        headers: { "X-Authorization":token},
        body: {
          "qty" : qty.toString(),
          "is_cut" : isCut.toString(),
          "cut_notes" : cutNotes,
        }
    ).catchError((error) {
        print(error);
      },
    );
    var response = json.decode(jsonResponse.body);
    print("post token = " + token);
    print("post qty = " + qty.toString());
    print("post is_cut = " + isCut.toString());
    print("post cut_notes = " + cutNotes);
    print("post updateUrl = " + updateUrl);
    print("edit cart response: " + response.toString());
    return response;
  }

  Future<dynamic> addCart(int productID, int skuID, int qty, int cut, String cutNotes) async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.post(
      Constants.API_ADD_CART,
      headers: { "X-Authorization":token},
      body: {
        "product_id" : productID.toString(),
        "sku_id" : skuID.toString(),
        "qty" : qty.toString(),
        "cut" : cut.toString(),
        "cut_notes" : cutNotes,
      }
    ).catchError((error) {
        print(error);
      },
    );
    var response = json.decode(jsonResponse.body);
    //globals.totalCartItem = response['item_count'];
    //totalCartItem = response['item_count'];
    //totalCartMoney = response['total_text'];
    //notifyListeners();
    print("\n\nsku id: "+skuID.toString());
    print("addCart token: "+token);
    print("addCart response: "+json.decode(jsonResponse.body).toString());
    print("addCart response message: "+response.toString());
    print("addCart total item: "+totalCartItem.toString());
    //print("addCart total money: "+totalCartMoney);
    return response;
  }

  Future<dynamic> checkoutCommit(int dropship, String paymentID, String dropshipName, String dropshipPhone) async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.post(
        Constants.API_COMMIT_CHECKOUT,
        headers: {"X-Authorization":token},
        body: {
          "payment_id" : paymentID,
          "dropshipper" : dropship.toString(),
          "dropship_name" : dropshipName,
          "dropship_phone" : dropshipPhone,
        }
    ).catchError((error) {
      print(error);
    },
    );
    print("url: "+ Constants.API_COMMIT_CHECKOUT);
    print("token: "+ token);
    print("payment_id: "+ paymentID.toString());
    print("dropshipper: "+ dropship.toString());
    print("dropshipName: "+ dropshipName);
    print("dropshipPhone: "+ dropshipPhone);
    var response = json.decode(jsonResponse.body);
    print("checkoutCommit response: " + response.toString());
    return response;
  }

  Future<dynamic> uploadTransfer(String orderID, String imageBase64) async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.post(
        Constants.API_UPLOAD_TRANSFER,
        headers: {"X-Authorization":token},
        body: {
          "order_id" : orderID,
          "image" : imageBase64,
        }
    ).catchError((error) {
      print(error);
    },
    );
    var response = json.decode(jsonResponse.body);
    print("uploadTransfer response: " + response.toString());
    return response;
  }

  List<Payment> parsePayment(dynamic dataFromResponse) {
    List<Payment> listPayment = new List();

    //var dataFromResponse = await checkoutGet();
    dataFromResponse.forEach(
      (newPayment) {

        Payment payment = new Payment(
          id: newPayment["id"],
          name: newPayment["name"],
          image: newPayment["image"],
        );

        listPayment.add(payment);
      }
    );
    //print("listPayment: " + listPayment.toString());
    return listPayment;
  }

  Future<dynamic> _getProductsByCategory(String url) async {
    // get token from shared prefs
    await getSavedToken();
    // get cart
    print("cart model api url: $url");
    var response = await http.get(
      url,
      headers: {
        "X-Authorization":token,
      },
    ).catchError(
          (error) {
        return false;
      },
    );
    //print("token: $token");
    //print("json.decode(response.body): "+json.decode(response.body).toString());

    if (json.decode(response.body) != null) {
      totalCartItem = json.decode(response.body)['item_count'];
      totalCartMoney = json.decode(response.body)['total_text'];
      notifyListeners();
    }
    return json.decode(response.body)['data'];
  }

  Future parseCartFromResponse(String url) async {
    //if (pageIndex == 1) {
      _isLoading = true;
    //}

    //notifyListeners();

    currentProductCount = 0;

    var dataFromResponse = await _getProductsByCategory(url);

      //print("cart scoped dataFromResponse:$dataFromResponse");

      if (dataFromResponse != null) {

      dataFromResponse.forEach(
            (newProduct) {
          currentProductCount++;


          //parse new product's details
          CartItem product = new CartItem(
            id: newProduct["id_cart"],
            isCut: newProduct["is_cut"],
            preOrder: newProduct["preorder"],
            cutOption: newProduct["cut_option"],
            brandID: newProduct["brand_id"],
            productName: newProduct["product_name"],
            price: newProduct["price"],
            notes: newProduct["notes"],
            priceText: newProduct["price_text"],
            itemQty: newProduct["qty"],
            productImage: newProduct["product_image"],
            productModel: newProduct["product_model"],
            cutNotes: newProduct["cut_notes"],
            updateUrl: newProduct["update_url"],
            removeUrl: newProduct["remove_url"],
            height: newProduct["height"],
            cutRange: newProduct["cutRange"],
            priceCutText: newProduct["price_cut_text"],
            priceBeforeCutText: newProduct["price_before_cut_text"],

          );

          addToCartList(product);
          product.ifItemAvailable = true;
        },
      );

      //if (pageIndex == 1)
      _isLoading = false;

      notifyListeners();

    } else {
      //_cartIsEmpty = true;
      _cartList = [];
      _isLoading = false;
     // _cartList.length = 0;
    }
  }

  void addToProductsList(Product product) {
    _productsList.add(product);
  }


  Future<dynamic> getOrders() async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.get(
      Constants.API_GET_ORDERS,
      headers: {
        "X-Authorization":token,
      },
    ).catchError(
          (error) {
        print(error);
      },
    );
    var response = json.decode(jsonResponse.body);
    // debug
    //print("getOrders response: "+response.toString());
    return response;
  }

  Future<List<Orders>> getOrdersCategory(String url) async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var response = await http.get(
      url,
      headers: {
        "X-Authorization":token,
      },
    ).catchError(
          (error) {
        print(error);
      },
    );

    List<Orders> _orderList = new List();
    var dataFromResponse = json.decode(response.body)['data'];
    //print("dataFromResponse $url: "+dataFromResponse.toString());

    dataFromResponse.forEach(
          (newOrder) {

            List<CartItem> itemList = [];
            String cartSummary = "";
            newOrder["items"].forEach(
              (newItem) {
                  itemList.add(
                    new CartItem(
                      productName: newItem["product_name"],
                      productImage: newItem["product_image"],
                      productModel: newItem["product_model"],
                      itemQty: newItem["qty"],
                      isCut: newItem["is_cut"],
                      cutNotes: newItem["cut_notes"],
                  ),
                );
                cartSummary = cartSummary + newItem["product_name"] + ", ";
              },

            );

            Orders address = new Orders(
              id: newOrder["id"],
              orderID: newOrder["order_id"],
              status: newOrder["transaction_status"],
              paymentType: newOrder["payment_type"],
              grossAmount: newOrder["gross_amount_text"],
              subAmount: newOrder["sub_amount_text"],
              shippingCourier: newOrder["shipping_courier"],
              shippingService: newOrder["shipping_service"],
              shippingCost: newOrder["shipping_cost"],
              shipFirstName: newOrder["ship_first_name"],
              shipLastName: newOrder["ship_last_name"],
              shipEmail: newOrder["ship_email"],
              shipProvince: newOrder["ship_province"],
              shipCity: newOrder["ship_city"],
              shipSubdistrict: newOrder["ship_subdistrict"],
              shipAddress: newOrder["ship_address"],
              shipPhone: newOrder["ship_phone"],
              paymentMethod: newOrder["payment_method"],
              vaNumber: newOrder["va_number"],
              dropshipper: newOrder["dropshipper"],
              dropshipperName: newOrder["dropship_name"],
              dropshipperPhone: newOrder["dropship_phone"],
              link: newOrder["link"],
              //paidAt: newOrder["paid_at"]["date"],
              //settlementAt: newOrder["settlement_at"]["date"],
              //deliveredAt: newOrder["delivered_at"]["date"],
              items: itemList,
              cartSumary: cartSummary,
            );
            _orderList.add(address);
      },
    );
    //print("_orderList length: "+_orderList.length.toString());
    //print("_orderList: "+_orderList.toString());
    return _orderList;
  }

  Future<dynamic> applyVoucher(String code) async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.post(
        Constants.API_VOUCHER_CHECKOUT,
        headers: { "X-Authorization":token},
        body: {
          "code" : code,
        }
    ).catchError((error) {
        print(error);
      },
    );
    var response = json.decode(jsonResponse.body);
    return response;
  }


}
