import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:zb_store/model/address.dart';
import 'package:zb_store/model/zone.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zb_store/util/globals.dart';
import 'package:zb_store/model/shipment.dart';

class AddressScopedModel extends Model {
  String token = "";
  String userName = "";
  String userEmail = "";
  List<Address> _addressList = [];
  List<Address> get addressList => _addressList;

  void addToAddressList(Address address) {
    _addressList.add(address);
  }

  Future getSavedToken() async {
    final prefs = await SharedPreferences.getInstance();
    final alreadyLogin = prefs.getBool(Constants.IS_GOOGLE_LOGIN) ?? false;
    if (alreadyLogin) {
      token = prefs.getString(Constants.SAVED_GOOGLE_TOKEN);
      userName = prefs.getString(Constants.SAVED_GOOGLE_NAME);
      userEmail = prefs.getString(Constants.SAVED_GOOGLE_EMAIL);
    } else {
      token = prefs.getString(Constants.SAVED_FACEBOOK_TOKEN);
      userName = prefs.getString(Constants.SAVED_FACEBOOK_NAME);
      userEmail = prefs.getString(Constants.SAVED_FACEBOOK_EMAIL);
    }
  }

  Future<dynamic> addAddress(
      String name,
      String provinceID,
      String cityID,
      String subdistrictId,
      String village,
      String postalCode,
      String address,
      String phone,
      String isDefault
      ) async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.post(
        Constants.API_ADD_ADDRESS,
        headers: { "X-Authorization":token},
        body: {
          "name" : name,
          "province_id" : provinceID,
          "city_id" : cityID,
          "subdistrict_id" : subdistrictId,
          "village" : village,
          "postal_code" : postalCode,
          "address" : address,
          "phone" : phone,
          "is_default" : isDefault,
          "alias" : "Rumahnya " + name,
        }
    ).catchError((error) {
        print(error);
      },
    );
    var response = json.decode(jsonResponse.body);
/*
    print("post qty = " + qty.toString());
    print("post is_cut = " + isCut.toString());
    print("post cut_notes = " + cutNotes);
    print("edit cart response: " + response.toString());
*/
    return response;
  }

  Future<dynamic> updateAddress(String id, String name, String provinceID, String cityID,
      String subdistrictId, String address, String postalCode, String phone, String village, String isDefault) async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.post(
        Constants.API_EDIT_ADDRESS,
        headers: { "X-Authorization":token},
        body: {
          "id" : id,
          "name" : name,
          "province_id" : provinceID,
          "city_id" : cityID,
          "subdistrict_id" : subdistrictId,
          "address" : address,
          "postal_code" : postalCode,
          "phone" : phone,
          "village" : village,
          "is_default" : isDefault,
          "alias" : "Rumahnya " + name,
        }
    ).catchError((error) {
      print(error);
    },
    );
    var response = json.decode(jsonResponse.body);
    return response;
  }

  void removeFromAddressList(int addressID) {
    _addressList.removeWhere((address) => address.id == addressID);
    notifyListeners();
  }

  Future<dynamic> removeAddress(String addressID) async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.post(
      Constants.API_REMOVE_ADDRESS,
      headers: {
        "X-Authorization":token,
      },
      body: {
        "id" : addressID,
      }
    ).catchError(
          (error) {
        print(error);
      },
    );
    var response = json.decode(jsonResponse.body);
    //print("remove cart response: " + response.toString());
    notifyListeners();
    return response;
  }


  Future<dynamic> updateCheckoutAddress(String addressID) async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.post(
        Constants.API_UPDATE_ADDRESS_CHECKOUT,
        headers: { "X-Authorization":token},
        body: {
          "id" : addressID,
        }
    ).catchError((error) {
      print(error);
    },
    );
    var response = json.decode(jsonResponse.body);
    return response;
  }

  Future<dynamic> fetchAddress() async {
    await getSavedToken();

    final response = await http.get(
      Constants.API_GET_ADDRESS,
      headers: {
        "X-Authorization":token,
      },
    );
    //print("fetchAddress response: " + json.decode(response.body)['data'].toString());
    // Use the compute function to run parsePhotos in a separate isolate
    return json.decode(response.body)['data'];
  }

  Future<List<Address>> parseAddress() async {
    _addressList = new List();

    var dataFromResponse = await fetchAddress();
    dataFromResponse.forEach(
          (newAddress) {

        Address address = new Address(
          id: newAddress["id"],
          name: newAddress["name"],
          address: newAddress["address"],
          provinceID: newAddress["province_id"],
          provinceName: newAddress["province"],
          cityID: newAddress["city_id"],
          cityName: newAddress["city"],
          subdistrictID: newAddress["subdistrict_id"],
          subdistrictName: newAddress["subdistrict"],
          postalCode: newAddress["postal_code"] == null ? "-" : newAddress["postal_code"],
          village: newAddress["village"] == null ? "-" : newAddress["village"],
          phone: newAddress["phone"],
          isDefault: newAddress["default"],
        );
        addToAddressList(address);
      },
    );
    //print("addressList length: "+addressList.length.toString());
    return _addressList;
  }

  Future<dynamic> fetchAddressAPI(String url, String param) async {

    final response = await http.get(url + param);
    //print("fetchAddressAPI url: " + url + param);
    //print("fetchAddressAPI response: " + json.decode(response.body).toString());
    // Use the compute function to run parsePhotos in a separate isolate
    return json.decode(response.body);
  }

  Future<List<Zone>> parseProvince(String url, String param) async {
    List<Zone> provinceList = new List();

    var dataFromResponse = await fetchAddressAPI(url, param);
    dataFromResponse.forEach(
          (newProvince) {

        Zone province = new Zone(
          id: newProvince["province_id"],
          name: newProvince["province"],
          postalCode: newProvince["province_id"], // dummy
        );
        provinceList.add(province);
      },
    );
    return provinceList;
  }

  Future<List<Zone>> parseCity(String url, String param) async {
    List<Zone> cityList = new List();

    var dataFromResponse = await fetchAddressAPI(url, param);
    dataFromResponse.forEach(
          (newCity) {

        Zone city = new Zone(
          id: newCity["city_id"],
          name: newCity["city_name"],
          postalCode: newCity["postal_code"],
        );
        cityList.add(city);
      },
    );
    return cityList;
  }

  Future<List<Zone>> parseSubdistrict(String url, String param) async {
    List<Zone> subdistrictList = new List();

    var dataFromResponse = await fetchAddressAPI(url, param);
    dataFromResponse.forEach(
          (newSubdistrict) {

        Zone subdistrict = new Zone(
          id: newSubdistrict["subdistrict_id"],
          name: newSubdistrict["subdistrict_name"],
          postalCode: newSubdistrict["subdistrict_id"], // dummy
        );
        subdistrictList.add(subdistrict);
      },
    );
    return subdistrictList;
  }

  Future<List<Shipment>> getShipmentService() async {
    List<Shipment> shipmentList = new List();

    await getSavedToken();

    final response = await http.get(
        Constants.API_SHIPMENT_GET,
        headers: {
          "X-Authorization":token,
        },
    );
    //print("getShipmentService response" + response.toString());

    var dataFromResponse = json.decode(response.body)["data"];
    dataFromResponse.forEach(
          (newShipment) {

        Shipment shipment = new Shipment(
          service: newShipment["service"],
          etd: newShipment["etd"],
          price: newShipment["price"].toString(),
          name: newShipment["name"],
          code: newShipment["code"],
        );
        shipmentList.add(shipment);
      },
    );
    return shipmentList;
  }

  Future<dynamic> updateShipmentService(String service) async {
    // get token from shared prefs
    await getSavedToken();
    // api
    var jsonResponse = await http.post(
        Constants.API_SHIPMENT_UPDATE,
        headers: { "X-Authorization":token},
        body: {
          "service" : service,
        }
    ).catchError((error) {
      print(error);
    },
    );
    var response = json.decode(jsonResponse.body);
    print("updateShipmentService response:"+response.toString());
    return response;
  }

}
