class Payment {
  final String id;
  final String name;
  final String image;

  Payment({this.id, this.name, this.image});

  factory Payment.fromJson(Map<String, dynamic> json) {
    return Payment(
      id: json['id'] as String,
      name: json['name'] as String,
      image: json['image'] as String,
    );
  }
}