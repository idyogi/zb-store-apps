import 'package:zb_store/model/any_image.dart';
import 'package:zb_store/model/category.dart';

class Product {
  String lineItemId;
  int productId;
  String productName;
  //List<Category> categories;
  List<String> images;
  //List<Map<String, List<String>>> imageColor;
  List<String> size;
  List<String> color;
  String cutNote;
  String selectedColor;
  String selectedSize;
  String priceText;
  String salePriceText;
  int skuId;
  int regularPrice;
  int salePrice;
  int discount;
  String skuPriceText;
  String skuSalePriceText;
  //bool ifItemAvailable;
  bool ifAddedToCart;
  bool ifSale;
  bool ifSaleSku;
  int isFavorite;
  String description;
  String productUrl;
  int stockQuantity;
  int cutQuantity;
  int cutOption;
  int itemQuantity;
  int colorAvailable;
  String imageSize;
  dynamic matching;
  int preOrder;
  String preOrderDate;

  Product(
      {this.lineItemId,
      this.productId,
      this.productName,
      //this.categories,
      this.images,
      //this.imageColor,
      this.size,
      this.color,
      this.cutNote,
      this.selectedColor,
      this.selectedSize,
      this.priceText,
      this.salePriceText,
      this.skuId,
      this.regularPrice,
      this.salePrice,
      this.discount,
      this.skuPriceText,
      this.skuSalePriceText,
      //this.ifItemAvailable,
      this.ifAddedToCart,
      this.ifSale,
      this.ifSaleSku,
      this.isFavorite,
      this.description,
      this.productUrl,
      this.stockQuantity,
      this.cutQuantity,
      this.cutOption,
      this.itemQuantity,
      this.colorAvailable,
      this.imageSize,
      this.matching,
      this.preOrder,
      this.preOrderDate
      });

  @override
  toString() => "productId: $productId , productName: $productName";

}
