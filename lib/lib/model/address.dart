class Address {
  final int id;
  final String name;
  final String address;
  final int provinceID;
  final String provinceName;
  final int cityID;
  final String cityName;
  final int subdistrictID;
  final String subdistrictName;
  final String village;
  final String postalCode;
  final String phone;
  final int isDefault;

  Address({
    this.id,
    this.name,
    this.address,
    this.provinceID,
    this.provinceName,
    this.cityID,
    this.cityName,
    this.subdistrictID,
    this.subdistrictName,
    this.village,
    this.postalCode,
    this.phone,
    this.isDefault
  });

}