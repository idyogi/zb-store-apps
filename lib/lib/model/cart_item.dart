
class CartItem {
  int id;
  //int cutQty;
  int itemQty;
  int price;
  int isCut;
  int height;
  int cutRange;
  int brandID;
  String productName;
  String cutNotes;
  String productImage;
  String productModel;
  String priceText;
  String priceCutText;
  String priceBeforeCutText;
  String notes;
  String updateUrl;
  String removeUrl;
  int preOrder;
  int cutOption;

  // unused
  List<String> images;
  int regularPrice;
  int salePrice;
  int discount;
  bool ifItemAvailable;
  bool ifAddedToCart;
  String description;
  int stockQuantity;

  CartItem(
      {
        this.id,
        this.brandID,
        this.itemQty,
        this.price,
        this.isCut,
        this.height,
        this.cutRange,
        this.productName,
        this.cutNotes,
        this.productImage,
        this.productModel,
        this.priceText,
        this.priceCutText,
        this.priceBeforeCutText,
        this.notes,
        this.updateUrl,
        this.removeUrl,
        this.preOrder,
        this.cutOption,

        // unused
        this.images,
        this.regularPrice,
        this.salePrice,
        this.discount,
        this.ifItemAvailable,
        this.ifAddedToCart,
        this.description,
        this.stockQuantity
      });

  @override
  toString() => "productId: $id, productName: $productName";

}
