import 'cart_item.dart';

class Orders {
  final int id;
  final String orderID;
  final String status;
  final String paymentType;
  final String grossAmount;
  final String subAmount;
  final String shippingCourier;
  final String shippingService;
  final String shippingCost;
  final String shipFirstName;
  final String shipLastName;
  final String shipEmail;
  final String shipProvince;
  final String shipCity;
  final String shipSubdistrict;
  final String shipAddress;
  final String shipPhone;
  final String paymentMethod;
  final String vaNumber;
  final int dropshipper;
  final String dropshipperName;
  final String dropshipperPhone;
  final String paidAt;
  final String settlementAt;
  final String deliveredAt;
  final List<CartItem> items;
  final String cartSumary;
  final String link;

  Orders({
    this.id,
    this.orderID,
    this.status,
    this.paymentType,
    this.grossAmount,
    this.subAmount,
    this.shippingCourier,
    this.shippingService,
    this.shippingCost,
    this.shipFirstName,
    this.shipLastName,
    this.shipEmail,
    this.shipProvince,
    this.shipCity,
    this.shipSubdistrict,
    this.shipAddress,
    this.shipPhone,
    this.paymentMethod,
    this.vaNumber,
    this.dropshipper,
    this.dropshipperName,
    this.dropshipperPhone,
    this.paidAt,
    this.settlementAt,
    this.deliveredAt,
    this.items,
    this.cartSumary,
    this.link,
  });

}