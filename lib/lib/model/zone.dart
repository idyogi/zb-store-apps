class Zone {
  final String id;
  final String name;
  final String postalCode;

  Zone({this.id, this.name, this.postalCode});

}