class Response {
  final String status;
  final String message;
  final int stock;

  Response({this.status, this.message, this.stock});

  factory Response.fromJson(Map<String, dynamic> json) {
    return Response(
      status: json['status'],
      message: json['message'],
      stock: json['stock'],
    );
  }
}