class Shipment {
  final String service;
  final String etd;
  final String price;
  final String name;
  final String code;

  Shipment({this.service, this.etd, this.price, this.name, this.code});

}